trigger BidAfterInsertAfterUpdate on Bid__c (After Insert,After update) {
  
    Set<Id> setVendorIds = new Set<Id>(); 
    Set<Id> setBidReqIds = new Set<Id>();  
    Set<Id> setUserIds = new Set<id>(); 
    Set<id> setSpecId = new Set<Id>();
    Set<Id> setBidIds = new Set<Id>();
    Map<Id,List<Id>> mapVendorUserId = new Map<Id,List<Id>>();   
    
    /*if(trigger.isInsert){
        BidTriggerUtility.triggerShare(Trigger.New);        
    }*/
  
    //if trigger is update
    if(trigger.isUpdate){
        List<Bid__c> bidList = Trigger.new;
        for(Bid__c bid : bidList) {
            setBidIds.add(bid.Id);
        }
        BidTriggerUtility.triggerUnshare(setBidIds);
        //BidTriggerUtility.triggerUnshare(Trigger.New,Trigger.OldMap);    
    }
   
}
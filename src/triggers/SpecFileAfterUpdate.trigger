trigger SpecFileAfterUpdate on Spec_File__c (after update, after insert) {
    
    
    List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();
    
    for(Spec_File__c objSpec:Trigger.New){        
        
        //if attached file is replaced create FILE CHANGE event.
        //if(Trigger.oldMap.get(objSpec.id) == null || Trigger.oldMap.get(objSpec.id).Attachment_Id__c!=objSpec.Attachment_Id__c){
        if (String.isNotBlank(objSpec.Attachment_Id__c) && (trigger.isInsert || objSpec.Attachment_Id__c != Trigger.oldMap.get(objSpec.id).Attachment_Id__c)) {

            Spec_Event__c objSpecEvent = new Spec_Event__c();
            objSpecEvent.Specification__c=objSpec.Specification__c;
            objSpecEvent.Spec_File__c = objSpec.Id;
            // Spec File is created initially with blank Attachment Id field value and then geting updated in SpecFileAttachmentController
            //objSpecEvent.Event_Type__c = Trigger.isUpdate ? 'File Change' : 'File Create';
            objSpecEvent.Event_Type__c = trigger.isInsert || String.isBlank(trigger.oldMap.get(objSpec.Id).Attachment_Id__c) ? 'File Create' : 'File Change';
            objSpecEvent.User__c=UserInfo.getUserID();
            lstSpecEvent.add(objSpecEvent);
            system.debug('lstSpecEvent <><> :' +lstSpecEvent);
        }
    }

    if(lstSpecEvent.size()>0){
        insert lstSpecEvent; 
    }       
}
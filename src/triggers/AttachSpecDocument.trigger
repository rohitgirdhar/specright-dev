trigger AttachSpecDocument on specright__Specification__c (after insert, after update) {
    List<Document__c> docList = new List<Document__c>();
    for(Specification__c currentSpec : Trigger.new){
        String specRTName = Schema.SObjectType.Specification__c.getRecordTypeInfosById().get(currentSpec.recordtypeid).getname();
        if(Trigger.isInsert){
            for(Spec_File__c file : [Select Id, Standard_File_Type__c, RecordType.Name From Spec_File__c Where RecordType.Name = 'Document' AND Standard_File_Type__c includes (:specRTName)]){
                Document__c docNew = new Document__c();
                docNew.File__c = file.Id;
                docNew.Specification__c = currentSpec.Id;
                docList.add(docNew);
            }
        }
        else if(Trigger.isUpdate){
            List<Id> existFileIds = new List<Id>();
            for(Document__c existDoc :[Select Id, File__c, Specification__c From Document__c Where Specification__c =: currentSpec.Id]){
                existFileIds.add(existDoc.File__c);
            }
            for(Spec_File__c file : [Select Id, Standard_File_Type__c, RecordType.Name From Spec_File__c Where RecordType.Name = 'Document' AND Standard_File_Type__c includes (:specRTName) AND Id NOT in: existFileIds]){
                Document__c docNew = new Document__c();
                docNew.File__c = file.Id;
                docNew.Specification__c = currentSpec.Id;
                docList.add(docNew);
            }
        }
    }
    if(!docList.isEmpty()){
        insert docList;
    }
}
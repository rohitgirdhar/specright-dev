trigger ExchangeSPHistoryTrack on Bid__c (after insert, after update) {
    /*
    List<History__c> historyList = new List<History__c>();
    
    Set<Id> exPartnerSpecIds = new Set<Id>();
    for(Bid__c objRecord : Trigger.New){
        exPartnerSpecIds.add(objRecord.Id);
    }
    if(trigger.isInsert){
        logHistoryInsert(exPartnerSpecIds);
    }
    else if(trigger.isUpdate){
        logHistoryUpdate(exPartnerSpecIds);
    }
    
    @Future
    public static void logHistoryInsert(Set<Id> bidIds){
        for(Bid__c objRecord : [Select Id From Bid__c Where Id in :bidIds]){
            History__c historyObj = new History__c();
            historyObj.Exchange_Spec_Partner__c = objRecord.Id;
            historyObj.Date__c = System.now();
            historyObj.User__c = UserInfo.getUserId();
            historyObj.Action__c = 'Exchange - Spec/Partner Created.';
            historyList.add(historyObj);
        }
        
        if(historyList.size() > 0) {
            insert historyList;
        }
        
    }
    
    @Future
    public static void logHistoryUpdate(Set<Id> bidIds){
        for(Bid__c objRecord : [Select Id From Bid__c Where Id in :bidIds]){
            Bid__c newObj = trigger.new[0]; 
            Bid__c oldObj = trigger.old[0]; 
            Bid__c obj = new Bid__c(); 
            // This takes all available fields from the required object. 
            Schema.SObjectType objType = obj.getSObjectType(); 
            Map<String, Schema.SObjectField> M = Schema.SObjectType.Bid__c.fields.getMap();
            for (String str : M.keyset()) {
                try {
                    if(!(str.equalsIgnoreCase('lastmodifieddate') || str.equalsIgnoreCase('systemmodstamp'))){
                        //Skip System Fields
                        History__c historyObj = new History__c();
                        if(newObj.get(str) != oldObj.get(str)){
                            historyObj.Exchange_Spec_Partner__c = objRecord.Id;
                            historyObj.Date__c = System.now();
                            historyObj.User__c = UserInfo.getUserId();
                            String fieldName = str.replace('specright__', '');
                            fieldName = fieldName.replace('__c', '');
                            fieldName = fieldName.replaceAll('_+', ' ');
                            fieldName = fieldName.capitalize();
                            String actionValue = fieldName + ' changed from ' + oldObj.get(str) + ' to ' + newObj.get(str) + '.';
                            if(actionValue.length() > 150) {
                                historyObj.Action__c = fieldName + ' changed';
                            }
                            else{
                                historyObj.Action__c = actionValue;
                            }
                            historyList.add(historyObj);
                        }
                    }
                } 
                catch (Exception e) { 
                    System.debug('Error: ' + e); 
                } 
            }
        }
        
        if(historyList.size() > 0) {
            insert historyList;
        }
    }
	*/
}
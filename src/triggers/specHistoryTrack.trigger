trigger specHistoryTrack on Specification__c (after insert, after update) {
    List<History__c> historyList = new List<History__c>();
    for(Specification__c objSpec : Trigger.New){
        if(trigger.isInsert) {
            History__c historyObj = new History__c();
            historyObj.Specification__c = objSpec.Id;
            historyObj.Date__c = System.now();
            historyObj.User__c = UserInfo.getUserId();
            historyObj.Action__c = 'Specification Created.';
            historyList.add(historyObj);
        }
        else if(trigger.isUpdate) {
            //Specification__c newSpec = trigger.new[0]; 
            //Specification__c oldSpec = trigger.old[0]; 
            for(Specification__c newSpec: trigger.new){
                for(Specification__c oldSpec: trigger.old){
                    Specification__c specObject = new Specification__c(); 
                    // This takes all available fields from the required object. 
                    Schema.SObjectType objType = specObject.getSObjectType(); 
                    Map<String, Schema.SObjectField> M = Schema.SObjectType.Specification__c.fields.getMap();
                    for (String str : M.keyset()) {
                        try {
                            if(!(str.equalsIgnoreCase('lastmodifieddate') || str.equalsIgnoreCase('systemmodstamp'))){
                                //Skip System Fields
                                History__c historyObj = new History__c();
                                if(newSpec.get(str) != oldSpec.get(str)){
                                    historyObj.Specification__c = objSpec.Id;
                                    historyObj.Date__c = System.now();
                                    historyObj.User__c = UserInfo.getUserId();
                                    String fieldName = str.replace('specright__', '');
                                    fieldName = fieldName.replace('__c', '');
                                    fieldName = fieldName.replaceAll('_+', ' ');
                                    fieldName = fieldName.capitalize();
                                    String actionValue = fieldName + ' changed from ' + oldSpec.get(str) + ' to ' + newSpec.get(str) + '.';
                                    if(actionValue.length() > 150) {
                                        historyObj.Action__c = fieldName + ' changed';
                                    }
                                    else{
                                        historyObj.Action__c = actionValue;
                                    }
                                    historyList.add(historyObj);
                                }
                            }
                        } 
                        catch (Exception e) { 
                            System.debug('Error: ' + e); 
                        } 
                    }
                }
            }
            
        }
    }
    
    if(historyList.size() > 0) {
        insert historyList;
    }
}
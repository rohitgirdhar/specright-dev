trigger SpecAuditAfterInsertCreateSpecEvent on Spec_Audit__c (After Insert) {

    List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();

    for(Spec_Audit__c objTemp:Trigger.new){
        
        Spec_Event__c objSpecEvent = new Spec_Event__c();
        objSpecEvent.Specification__c=objTemp.Specification__c;
        objSpecEvent.Event_Type__c='Audit';
        objSpecEvent.User__c=UserInfo.getUserID();
        lstSpecEvent.add(objSpecEvent);     
    } 
    if(lstSpecEvent.size()>0){
        insert lstSpecEvent;
    }
}
trigger SpecFileAfterUpdateEdit on Spec_File__c (after update) {
    List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();
    
    for(Spec_File__c objSpec:Trigger.New){        
        if(Trigger.oldMap.get(objSpec.Id).Spec_File_Name__c != Trigger.newMap.get(objSpec.Id).Spec_File_Name__c ||
           Trigger.oldMap.get(objSpec.Id).File_Type__c != Trigger.newMap.get(objSpec.Id).File_Type__c){
               Spec_Event__c objSpecEvent = new Spec_Event__c();
               objSpecEvent.Specification__c=objSpec.Specification__c;
               objSpecEvent.Spec_File__c = objSpec.Id;
               objSpecEvent.Event_Type__c = 'Edit';
               objSpecEvent.User__c=UserInfo.getUserID();
               lstSpecEvent.add(objSpecEvent); 
           }
    }
    
    if(lstSpecEvent.size()>0){
        insert lstSpecEvent; 
    }
}
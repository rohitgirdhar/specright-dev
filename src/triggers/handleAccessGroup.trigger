trigger handleAccessGroup on BidGroup__c (before update, after update) {
	if(trigger.isBefore)
		AccessGroupHandler.setAccessEnd(Trigger.newMap);
	if(trigger.isAfter)
		AccessGroupHandler.updateRecordtype(Trigger.new);
}
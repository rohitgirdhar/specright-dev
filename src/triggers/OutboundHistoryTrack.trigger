trigger OutboundHistoryTrack on specright__Outbound_Finished_Product__c (after insert, after update) {
	List<History__c> historyList = new List<History__c>();
    for(Outbound_Finished_Product__c objRecord : Trigger.New){
        if(trigger.isInsert) {
            History__c historyObj = new History__c();
            historyObj.Outbound_Finished_Product__c = objRecord.Id;
            historyObj.Date__c = System.now();
            historyObj.User__c = UserInfo.getUserId();
            historyObj.Action__c = 'Outbound Material Created.';
            historyList.add(historyObj);
        }
        else if(trigger.isUpdate) {
            //Outbound_Finished_Product__c newObj = trigger.new[0]; 
            //Outbound_Finished_Product__c oldObj = trigger.old[0]; 
            for(Outbound_Finished_Product__c newObj: trigger.new){
                for(Outbound_Finished_Product__c oldObj: trigger.old){
                    Outbound_Finished_Product__c obj = new Outbound_Finished_Product__c(); 
                    // This takes all available fields from the required object. 
                    Schema.SObjectType objType = obj.getSObjectType(); 
                    Map<String, Schema.SObjectField> M = Schema.SObjectType.Outbound_Finished_Product__c.fields.getMap();
                    for (String str : M.keyset()) {
                        try {
                            if(!(str.equalsIgnoreCase('lastmodifieddate') || str.equalsIgnoreCase('systemmodstamp'))){
                                //Skip System Fields
                                History__c historyObj = new History__c();
                                if(newObj.get(str) != oldObj.get(str)){
                                    historyObj.Outbound_Finished_Product__c = objRecord.Id;
                                    historyObj.Date__c = System.now();
                                    historyObj.User__c = UserInfo.getUserId();
                                    String fieldName = str.replace('specright__', '');
                                    fieldName = fieldName.replace('__c', '');
                                    fieldName = fieldName.replaceAll('_+', ' ');
                                    fieldName = fieldName.capitalize();
                                    String actionValue = fieldName + ' changed from ' + oldObj.get(str) + ' to ' + newObj.get(str) + '.';
                                    if(actionValue.length() > 150) {
                                        historyObj.Action__c = fieldName + ' changed';
                                    }
                                    else{
                                        historyObj.Action__c = actionValue;
                                    }
                                    historyList.add(historyObj);
                                }
                            }
                        } 
                        catch (Exception e) { 
                            System.debug('Error: ' + e); 
                        } 
                    }
                }
            }
            
        }
    }
    
    if(historyList.size() > 0) {
        insert historyList;
    }
}
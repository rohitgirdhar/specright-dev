trigger SpecificationAfterUpdate on Specification__c (After update) {
    Set<Id> setSpecIds = new Set<Id>();
    Set<Id> setVendorIds = new Set<Id>(); 
    List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();
    List<Specification__Share> lstSharedSpecification= new List<Specification__Share>();
    Map<Id,List<Id>> mapVendorUserId = new Map<Id,List<Id>>();      
    List<Bid__c> lstBids = new List<Bid__c>();  
    
    for(Specification__c objSpec : Trigger.New){
        if(System.Trigger.oldMap.get(objSpec.Id).OwnerId!=objSpec.OwnerId){
            setSpecIds.add(objSpec.id);
        }
        
        // To create SpecEvent on every edit
        Spec_Event__c objSpecEvent = new Spec_Event__c();
        objSpecEvent.Specification__c=objSpec.id;
        objSpecEvent.Event_Type__c='Edit';
        objSpecEvent.User__c=UserInfo.getUserID();
        lstSpecEvent.add(objSpecEvent);
                
         //Check that Status field values is changed to Obsolete and add create a SpecEvent 'Obsolete'
        if(Trigger.OldMap.get(objSpec.id).Status__c!=objSpec.Status__c && objSpec.Status__c=='Obsolete'){  
            Spec_Event__c objSpecEvent1 = new Spec_Event__c();
            objSpecEvent1.Specification__c=objSpec.id;
            objSpecEvent1.Event_Type__c='Obsolete';
            objSpecEvent1.User__c=UserInfo.getUserID();
            lstSpecEvent.add(objSpecEvent1);
        }                
    }
        
    lstBids = [select id,Supplier__c,specification__c from Bid__c where Bid_Request__c in (select id from Bid_Request__c where specification__c in :setSpecIds and Bid_Status__c !='Awarded')];    

    //Get all vendor ids
    for(Bid__c objBid:lstBids){
        if(objBid.Supplier__c!=null){
            setVendorIds.add(objBid.Supplier__c);  
        }
    }
    
    //Construct Map of VendorId - UserIds
    for(User u: [select id,contact.AccountId from user where contact.AccountId in :setVendorIds]){        
        //if vendor id key exists thn add user id to the list
        if(mapVendorUserId.keyset().contains(u.contact.AccountId)){
            mapVendorUserId.get(u.contact.AccountId).add(u.id);
        }
        //else put new key with new list consisting current user id
        else {
            mapVendorUserId.put(u.contact.AccountId,new List<Id>{u.id});
        }
    }  
    
    //Get all vendor ids
    for(Bid__c objBid:lstBids){
        if( objBid.Supplier__c != NULL && mapVendorUserId.keySet().contains(objBid.Supplier__c) ) {
                
            for(Id userId:mapVendorUserId.get(objBid.Supplier__c)){
                Specification__Share objSpec= new Specification__Share();
                objSpec.AccessLevel = 'Read'; //Read
                objSpec.parentID = objBid.Specification__c; // Set the ID of record being shared.
                objSpec.RowCause = 'Manual';
                // Assign user id to grant read access to this particular Specification record.                          
                objSpec.UserOrGroupId = userId;                           
                lstSharedSpecification.add(objSpec);                                
            }
        }
    }
    
    
    //insert share records 
    if( lstSharedSpecification != null && lstSharedSpecification.size()>0 ) {        
        insert lstSharedSpecification;        
    }
    
    //insert spec events
    if(lstSpecEvent.size()>0){
    insert lstSpecEvent;    
    }
}
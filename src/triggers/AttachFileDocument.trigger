trigger AttachFileDocument on specright__Spec_File__c (after insert, after update) {
    List<Document__c> newDocList = new List<Document__c>();
    Id SpecFileDocRTId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Document').getRecordTypeId();
    for(Spec_File__c newFile : Trigger.new){
        if(Trigger.isInsert){
            if(newFile.RecordTypeId == SpecFileDocRTId){
                List<String> documentType = newFile.Standard_File_Type__c.split(';');
                for(Specification__c spec : [Select Id, RecordType.Name From Specification__c Where RecordType.Name in: documentType]){
                    Document__c newDoc = new Document__c();
                    newDoc.File__c = newFile.Id;
                    newDoc.Specification__c = spec.Id;
                    newDocList.add(newDoc);
                }
            }            
        }
        else if(Trigger.isUpdate){
            if(newFile.RecordTypeId == SpecFileDocRTId){
                Set<Id> existSpecIds = new Set<Id>();
                List<String> documentType = newFile.Standard_File_Type__c.split(';');
                for(Document__c existDoc : [Select Id, Specification__c, File__c From Document__c Where File__c =: newFile.Id]){
                    existSpecIds.add(existDoc.Specification__c);
                    System.debug('Spec Id: ' + existDoc.Specification__c);
                }
                for(Specification__c spec : [Select Id, RecordType.Name From Specification__c Where RecordType.Name in: documentType AND Id NOT in: existSpecIds]){
                    Document__c newDoc = new Document__c();
                    newDoc.File__c = newFile.Id;
                    newDoc.Specification__c = spec.Id;
                    newDocList.add(newDoc);
                }
            }
        }
    }
    
    if(!newDocList.isEmpty()){
        insert newDocList;
    }
}
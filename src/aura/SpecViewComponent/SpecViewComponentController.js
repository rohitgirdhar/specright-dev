({
    doInit: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //console.log(' recordId ' + recordId);
        var getExtensionObject = component.get("c.getExtensionObject");
        component.set("v.minimumTabs", 3);
        getExtensionObject.setParams({
            specId: recordId
        });

        getExtensionObject.setCallback(this, function(response) {
            var state = response.getState() ;

            if (state === "SUCCESS") {
                var extensionObject = response.getReturnValue();
                //debugger;
                //console.log(' extensionObject ', extensionObject);

                if (extensionObject != undefined && extensionObject.Id != null) {
	                component.set("v.extensionTabLabel", extensionObject.label.toUpperCase());
	                component.set("v.extensionId", extensionObject.Id);
	                component.set("v.hasExtension", true);
	                //console.log('the extension value', component.get("v.hasExtension"));

	                var getReleatedListsMetadata = component.get("c.getReleatedListsMetadata");
	                //if (extensionObject != undefined && extensionObject.Id != null) {

                    getReleatedListsMetadata.setParams({
                        commodityId: extensionObject.Id
                    });

                    getReleatedListsMetadata.setCallback(this, function(response) {
                        var state = response.getState();
                        //console.log('the response data', response.getReturnValue());
                        if (state === "SUCCESS") {       
                            var relatedLists = response.getReturnValue();
                            
                            //Set flat related lists (1 per tab)
                            var flatRelatedLists = relatedLists.slice(0, 3).map(function(elt) {	//component.get("v.minimumTabs")
                                elt.label = elt.label.toUpperCase();
                            });;
                            component.set("v.flatRelatedLists", relatedLists.slice(0, 3));
                            
                            //Set other related lists (all in one tab)
                            if(component.get("v.showRelatedListTab")) {                    
                            	component.set("v.otherRelatedLists", relatedLists.slice(3));                        
                            	//console.log('data',component.get("v.otherRelatedLists"));
                            }
                        }
                        else if (response.getState() === "ERROR") {
                            //$A.log("Errors", response.getError());
                            debugger;
                            console.log(response.getError());
                        } 
                    });

                    $A.enqueueAction(getReleatedListsMetadata);
	            	//}
	            }
            }
            else if (response.getState() === "ERROR") {
                //$A.log("Errors", response.getError());
                console.log('the error ', response.getError());
            }else {
                console.log('the error in else', response.getError());
            }
        });
        
        $A.enqueueAction(getExtensionObject);        
    }
})
/**
 * Clean up batch for Community Access
 * @author - Wisdomedge Inc.
 */

global with sharing class AccessShareBatch implements Database.batchable<sObject>, Schedulable {
	global String query;
	global Date dt =Date.Today();
	global AccessShareBatch() {
		Date dt =Date.Today();
		query = 'Select Id, Specification__c from Bid__c where Access_End__c < :dt';
	}
	global Database.QueryLocator start(Database.BatchableContext BC){
      	return Database.getQueryLocator(query);
   	}
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<Bid__c> lstbids = new List<Bid__c>();
		Set<Id> lstparentids = new Set<Id>();
		Set<Id> lstspecids = new Set<Id>();
		List<Spec_File__Share> lstSpecFileShareDelete = new List<Spec_File__Share>();
		List<Specification__Share> lstspecshare = new List<Specification__Share>();
		List<Bid__Share> lstbidshare = new List<Bid__Share>();
		for(sObject s : scope){
			Bid__c bid = (Bid__c)s;
			lstparentids.add(bid.Id);
			lstspecids.add(bid.Specification__c);
			lstbids.add(bid); 
		}

		
		if(lstparentids.size() > 0 && lstspecids.size() > 0) {
			lstbidshare = [Select Id from Bid__Share where ParentId in :lstparentids];
			system.debug(lstbidshare);
			for(Bid__c bid :[Select Id, Access_End__c, Specification__c from Bid__c where Specification__c in :lstspecids and Id not in:lstparentids]) {
				if(bid.Access_End__c > Date.Today())
					lstspecids.remove(bid.Specification__c);	
			}
			if(lstspecids.size() > 0) {
				lstspecshare = [Select Id from Specification__Share where ParentId in :lstspecids];
				system.debug(lstspecshare);

				for(Spec_File__Share objShare: [select id,parentId,UserOrGroupId from Spec_File__Share where parentId in :lstspecids]){
					lstSpecFileShareDelete.add(objShare);
				}
			}
		}
		//Delete Specification share records
		if(lstspecshare.size() > 0) {
			Database.Delete(lstspecshare, false);
			DataBase.emptyRecycleBin(lstspecshare);
		}

		//Delete bid share records
		if(lstbidshare.size() > 0) {
			Database.Delete(lstbidshare, false);
			DataBase.emptyRecycleBin(lstbidshare);
		}

		//Delete spec file share records
		if(lstSpecFileShareDelete.size() > 0) {
			Database.Delete(lstSpecFileShareDelete, false);
			DataBase.emptyRecycleBin(lstSpecFileShareDelete);
		}

		
    }
    global void execute(SchedulableContext sc) {
    	List<AsyncApexJob> lstCleanupJob = [Select id from AsyncApexJob where ApexClass.name='Daily Shares Deletion Job' 
				and status in ('Holding','Queued', 'Processing','Preparing')];
		if(lstCleanupJob.size() == 0) {
    		Database.executeBatch(new AccessShareBatch(), 50);
    	}
    }

   	global void finish(Database.BatchableContext BC){
   	}

   	/**
   	 * method to schedule the deletion share batch to run every midnight 23:59
   	 * this can be called from both post install script as well as the authorization 
   	 * completion for existing orgs.
   	 */
   	global static void scheduleDeleteShareBatch() {
   		//check if the cleanup job is already scheduled, if not then schedule it
        List<CronTrigger> lstJobs = 
            [Select id from CronTrigger 
            where CronJobDetail.name=:'Daily Shares Deletion Job' and state in ('WAITING','ACQUIRED','EXECUTING')];
        if(lstJobs.size() == 0) {
            String scheduleString  =  '0 59 23 * * ?';
            AccessShareBatch scheduler = new  AccessShareBatch();
            System.schedule('Daily Shares Deletion Job', scheduleString, scheduler); 
        }
   	}	
}
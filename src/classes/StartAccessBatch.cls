global class StartAccessBatch implements Database.batchable<sObject>, Schedulable {
	global String query;
	global StartAccessBatch() {
		//query = 'Select Id, Bid_Start_Date__c from specright__Bid__c where Bid_Start_Date__c =:Date.today()';
	}
	global Database.QueryLocator start(Database.BatchableContext BC){
      	return Database.getQueryLocator(query);
   	}
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
   		/*List<Bid__c> lstBid = new List<Bid__c>();
   		for(SObject obj:scope){
   			lstBid.add((Bid__c)obj);
   		}
   		Set<Id> setVendorIds = new Set<Id>(); 
        Set<Id> setBidReqIds = new Set<Id>();  
        Set<Id> setUserIds = new Set<id>(); 
        Set<id> setSpecId = new Set<Id>();
        Map<Id,List<Id>> mapVendorUserId = new Map<Id,List<Id>>(); 
        //Map<Id,List<Id>> mapVendorContactId = new Map<Id,List<Id>>(); 
        //Map<Id,Id> mapContactIdUserId = new Map<Id,Id>(); 
        Map<Id,List<Id>> mapSpecUserId = new Map<Id,List<Id>>();  
        List<spec_file__c> lstSpecFile = new List<spec_file__c>();
        Map<id,EmailStaging__c> mapEmailStagToInsert = new Map<Id,EmailStaging__c>();
        
        for(Bid__c objBid : lstBid) { 
            if(objBid.Bid_Status__c!='Awarded'){  
                if(objBid.Supplier__c!=null){
                    setVendorIds.add(objBid.Supplier__c);  
                }
                if(objBid.Bid_Request__c!=null){
                    setBidReqIds.add(objBid.Bid_Request__c);
                } 
                setSpecId.add(objBid.specification__c); 
            }
        }  
        System.debug('SSSSV:'+setVendorIds);
        //CRUD & FLS Enforecement on Contact
        if(!Schema.sObjectType.Contact.isAccessible()){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
        }
        
        //construct map vendor & contact ids
        /*for(Contact objCon:[select id,AccountId from contact where email!=null and Accountid in: setVendorIds]){
            //if vendor id key exists thn add contact id to the list
            if(mapVendorContactId.keyset().contains(objCon.AccountId)){
                mapVendorContactId.get(objCon.AccountId).add(objCon.id);
            }
            //else put new key with new list consisting current contact id
            else {
                mapVendorContactId.put(objCon.AccountId,new List<Id>{objCon.id});
            }
        }   */   
        
        /*
        // Check CRUD & FLS Enforcement for Specification
        String [] SpecificationFields = new String [] {'Location__c','Spec_Name__c','Name'};
        Map<String,Schema.SObjectField> mapSpec = Schema.SObjectType.Specification__c.fields.getMap();
        for (String fieldToCheck : SpecificationFields){
             if(!mapSpec.get(fieldToCheck).getDescribe().isAccessible()) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
             }
         }
        //get spec file id with respect to spec id
        lstSpecFile = [select id,specification__c from spec_file__c where specification__c in :setSpecId and File_Type__c in ('Level 1','Level 2','Level 3')];            
        
        //CRUD & FLS Enforecement on User
        if(!Schema.sObjectType.User.isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        //get userids of respective vendors
        for(User u: [select id,contactid,contact.AccountId from user where IsActive = true and contact.AccountId in :setVendorIds]){
            setUserIds.add(u.id);
            //if vendor id key exists thn add user id to the list
            if(mapVendorUserId.keyset().contains(u.contact.AccountId)){
                mapVendorUserId.get(u.contact.AccountId).add(u.id);
            }
            //else put new key with new list consisting current user id
            else {
                mapVendorUserId.put(u.contact.AccountId,new List<Id>{u.id});
            }
            
            //update map of contactid - user id
            //mapContactIdUserId.put(u.contactid,u.id);
        }                                       
        
        List<Specification__Share> lstSharedSpecification = new List<Specification__Share>();
        List<Bid__Share> lstSharedBid = new List<Bid__Share>();
        List<spec_file__Share> lstSharedSpecFile = new List<spec_file__Share>();
        
        //CRUD & FLS Enforecement on User
        if(!Schema.sObjectType.EmailTemplate.isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        //Get Email Template id
        List<EmailTemplate> lstTemp=[Select id from emailtemplate where name ='BidInviteSupplierTemplate'];
        String templateid='';
        if(lstTemp!=null && lstTemp.size()>0){
            templateid=lstTemp[0].id;
        }
        
        for(Bid__c objBid : lstBid ) {
            if( objBid.Bid_Status__c!='Awarded' && objBid.Supplier__c != NULL && mapVendorUserId.keySet().contains(objBid.Supplier__c) 
                    && objBid.Bid_Start_Date__c == Date.today()) {
                         
                for(Id userId:mapVendorUserId.get(objBid.Supplier__c)){
                    //Create share records for Bid
                    Bid__Share objBidShare= new Bid__Share();
                    objBidShare.AccessLevel = 'Edit';
                    objBidShare.parentID = objBid.id; // Set the ID of record being shared.
                    objBidShare.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular bid record.                          
                    objBidShare.UserOrGroupId = userId;                           
                    lstSharedBid.add(objBidShare);  
                    
                    //Create share records for specification
                    Specification__Share objSpec= new Specification__Share();
                    objSpec.AccessLevel = 'Read';
                    objSpec.parentID = objBid.Specification__c; // Set the ID of record being shared.
                    objSpec.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular Specification record.                          
                    objSpec.UserOrGroupId = userId;                           
                    lstSharedSpecification.add(objSpec);     
                    
                    //if specification id key exists thn add user id to the list
                    if(mapSpecUserId.keyset().contains(objBid.Specification__c)){
                        mapSpecUserId.get(objBid.Specification__c).add(userId);
                    }
                    //else put new key with new list consisting current user id
                    else {
                        mapSpecUserId.put(objBid.Specification__c,new List<Id>{userId});
                    }  
                    
                    //create email staging
                    EmailStaging__c objEmailStag = new EmailStaging__c(User__c=userId,type__c='Supplier Invitation');   
                    mapEmailStagToInsert.put(userId,objEmailStag);                          
                }                                
            }                        
        }  
        
        System.debug('*****'+mapEmailStagToInsert);
        //insert email staging        
        if(mapEmailStagToInsert.size()>0){
            insert mapEmailStagToInsert.values(); 
        }
        
        //create share records for spec files
        for(spec_file__c objSpecFile : lstSpecFile){
            if(mapSpecUserId.keyset().contains(objSpecFile.Specification__c)){
                for(Id userId: mapSpecUserId.get(objSpecFile.Specification__c)){
                    spec_file__Share objShare= new spec_file__Share();
                    objShare.AccessLevel = 'Read';
                    objShare.parentID = objSpecFile.Id; // Set the ID of record being shared.
                    objShare.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular file.                          
                    objShare.UserOrGroupId = userId;                           
                    lstSharedSpecFile.add(objShare); 
                }
            }
        }      
        
        
        //CRUD & FLS for Specification_Share
        if(!Schema.sObjectType.Specification__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        //insert specification share records
        if( lstSharedSpecification != null && lstSharedSpecification.size()>0 ) {        
            insert lstSharedSpecification;        
        }
        
        //CRUD & FLS for Spec_File__Share
        if(!Schema.sObjectType.spec_file__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        
        //insert spec file share records
        if( lstSharedSpecFile != null && lstSharedSpecFile.size()>0 ) {        
            insert lstSharedSpecFile;        
        }
        
        
        //CRUD & FLS for Bid__Share
        if(!Schema.sObjectType.Bid__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        System.debug('****'+lstSharedBid);
        //insert bid share records
        if( lstSharedBid!= null && lstSharedBid.size()>0 ) {        
            insert lstSharedBid;        
        }*/
   	}
   	global void execute(SchedulableContext sc) {
    	//Id BatchId = Database.executeBatch(new StartAccessBatch(), 200);
    }

   	global void finish(Database.BatchableContext BC){
   	}	
}
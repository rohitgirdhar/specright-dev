@isTest(SeeAlldata = true)
private class BidSubmitControllerTest {
  static testMethod void BidSubmitControllerTestMethod() {
      RecordType RT = [select id,Name from RecordType where SObjectType = 'specright__Bid__c' and Name = 'Bid' Limit 1];
      List<Bid__c > lstBidS = new List<Bid__c >();
      Bid__c bid = new Bid__c();
      bid.Bid_Status__c = 'Responded';
      bid.RecordTypeId = RT.id;
      lstBidS.add(bid);
      insert lstBidS;
      List<Bid__c > lstBid = [Select id from Bid__c];
      ApexPages.StandardSetController controller =  new ApexPages.StandardSetController(lstBid);
      controller.setSelected(lstBidS);
      BidSubmitController bidCtlr = new BidSubmitController(controller);
      
      bidCtlr.updateStatus();
       List<Bid__c > listBid = [Select id , Bid_Status__c from Bid__c where id =: lstBidS[0].id];
       system.debug('<<>>'+listBid);
       system.assertequals(listBid[0].Bid_Status__c,'Submitted');
      
    }

    static testMethod void BidSubmitControllerTestMethodForPushAudit() {

      RecordType RT = [select id,Name from RecordType where SObjectType = 'specright__Bid__c' and Name = 'Push Audit' Limit 1];
      List<Bid__c > lstBidS = new List<Bid__c >();
      Bid__c bid = new Bid__c();
      bid.Bid_Status__c = 'Waiting for Response'; 
      bid.RecordTypeId = RT.id;
      lstBidS.add(bid);
      insert lstBidS;
      List<Bid__c > lstBid = [Select id from Bid__c];
      ApexPages.StandardSetController controller =  new ApexPages.StandardSetController(lstBid);
      controller.setSelected(lstBidS);
      BidSubmitController bidCtlr = new BidSubmitController(controller);
     
    //  system.assertequals(!listBid[0].Bid_Status__c,'Submitted');
      bidCtlr.updateStatus();
      List<Bid__c > listBid = [Select id , Bid_Status__c from Bid__c where id =: lstBidS[0].id];
      system.debug('<<>>'+listBid);
      system.assertNotEquals(listBid[0].Bid_Status__c,'Submitted');
   }  
    static testMethod void BidSubmitControllerTestMethodForPushAudit1() {

      RecordType RT = [select id,Name from RecordType where SObjectType = 'specright__Bid__c' and Name = 'Push Audit' Limit 1];
      Bid__c bid = new Bid__c();
      bid.Bid_Status__c = 'Waiting for Response'; 
      bid.RecordTypeId = RT.id;
      insert bid;
      
      List<Bid__c > lstBid = [Select id from Bid__c];
      ApexPages.StandardSetController controller =  new ApexPages.StandardSetController(lstBid);
      //controller.setSelected(lstBidS);
      BidSubmitController bidCtlr = new BidSubmitController(controller);
      List<Bid__c> listbids = [Select Id,Bid_Status__c from Bid__c where id =: bid.id];
      System.assert(listbids.size()>0);
      ApexPages.Message[] pageMessages = ApexPages.getMessages();
      System.assertNotEquals(0, pageMessages.size());
   }
}
global with sharing class SpecEventBatch implements Database.Batchable<sObject> {

    global SpecEventBatch (){}

   global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //CRUD & FLS Checking 
        if (!Schema.sObjectType.RecentlyViewed.isAccessible() ) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        String query = 'SELECT Id,LastViewedDate FROM RecentlyViewed WHERE Type=\'specright__Specification__c\' ORDER BY LastViewedDate DESC ';
        return Database.getQueryLocator(query);
    }
  
    global void execute(Database.BatchableContext BC, List<RecentlyViewed> scope){
    
        List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();
		Boolean isSpecEventCreateable1= Schema.sObjectType.Spec_Event__c .isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.Specification__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.Event_Type__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.User__c.isCreateable();       
        if(!isSpecEventCreateable1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
            return;
        }
        //Check if  record in scope is from last hour.......?????
        for(RecentlyViewed obj : scope){
            // Check that record is created in RecentlyViewed Table in the last hour only 
            DateTime timeNow=Datetime.now().addHours(-1);   
        
            if(obj.LastViewedDate > timeNow){
               // To create SpecEvent - View
               Spec_Event__c objSpecEvent = new Spec_Event__c();
               objSpecEvent.Specification__c=obj.id;
               objSpecEvent.Event_Type__c='View';
               objSpecEvent.User__c=UserInfo.getUserID();
               lstSpecEvent.add(objSpecEvent);
            }
         }
    
        //CRUD & FLS Checking
         Boolean isSpecEventCreateable= Schema.sObjectType.Spec_Event__c .isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.Specification__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.Event_Type__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.User__c.isCreateable();       
         if(!isSpecEventCreateable){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
             return;
         }           
         if(lstSpecEvent.size()>0){
            insert lstSpecEvent;
         } 
   }
  
    global void finish(Database.BatchableContext BC){}
}
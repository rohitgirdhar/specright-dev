public with sharing class AVPSSpecificationNewButtonController {
    ApexPages.StandardController std;
    public String specId;
    public String sObjectName = 'specright__Specification__c';
    public List<Coatings__c> newCoatingsList = new List<Coatings__c>();
    public List<Layer_Material__c> newLayerMaterialList = new List<Layer_Material__c>();
    public List<Embellishments__c> newEmbellishmentsList = new List<Embellishments__c>();
    public List<PMS_Instructions__c> newPMSInstructionsList = new List<PMS_Instructions__c>();
    public Specification__c specSupplier{get; set;}
    //String apvRecordTypeAppend = ' - APV Supplier';
    //String apvRecordTypeName = '';
    //String oldRecordTypeName = '';
    
    public AVPSSpecificationNewButtonController(ApexPages.StandardController std){
        specId=ApexPages.currentPage().getParameters().get('specId');
        specSupplier = new Specification__c();
        this.std = std;
    }
    
    public PageReference cancel()
    {
        Pagereference pg=new pagereference('/' + specId);
        pg.setRedirect(true);
        return pg;
    }
    
    public pageReference ToApproveRecord(){
        if(specSupplier.Supplier__c == null) {
            return null;
        }
        String clonedSpecId = '';
        //Clone the Record with Master Spec
        Specification__c newSpec = cloneOldSpec();
        if(!Schema.sObjectType.Specification__c.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access.'));
        }
        else{
            insert newSpec;
        }
        cloneFlexiableRelatedSpec(specId, newSpec.Id);
        clonedSpecId = newSpec.Id;
        //APV Record Type Exist, then clone the record and update record type
        /*
        if(recordTypeExist()){
            Specification__c newSpec = cloneOldSpec();
            clonedSpecId = newSpec.Id;
        }
        //APV Record Does not Exist, create record type then clone record and update reocrd type
        else{
            clonedSpecId = needNewRecordType();
        }
        */
        Pagereference pg=new pagereference('/'+clonedSpecId + '/e?retURL=%2F'+clonedSpecId);
        //Pagereference pg=new pagereference('/'+ specId + '/e?clone=1');//&CF00No000000CNn8F='+ specId);
        pg.setRedirect(true);
        return pg;
    }
    /*
    public Id needNewRecordType() {
        //Create new Record Type
            MetadataService.RecordType apvRecordType = CreateFieldService.createNewRecordType(sObjectName, apvRecordTypeName, '');
            CreateFieldService.saveNewRecordType(apvRecordType);
            //Create new Layout
            CreateFieldService.saveCreateLayout(CreateFieldService.createLayout(sObjectName, apvRecordTypeName, null));
            //Copy Record Layout to APV Layout
            CreateFieldService.copyLayoutSection(sObjectName, oldRecordTypeName + ' Layout', apvRecordTypeName + ' Layout');
            //Add Field to APV Layout
            String[] fieldsToAdd = new String[]{'Master_Specification__c', 'Supplier__c'};
            CreateFieldService.saveAddFieldToLayout(CreateFieldService.addFieldToLayout(sObjectName, apvRecordTypeName+' Layout', fieldsToAdd, true));
            //Assign RecordType to Layout
            List<Profile> profilesList = [Select Id, Name
                                  From Profile
                                  Where Name like '%Specright -%'];
            for(Profile profile : profilesList){
                CreateFieldService.saveFieldLevelSecurity(
                    CreateFieldService.updateRecordTypePermission(profile.Name, apvRecordType, apvRecordTypeName));
            }
            Specification__c newSpec = cloneOldSpec();
            return newSpec.Id;
    }
    */
    /*
    public Boolean recordTypeExist() {
        Specification__c spec = [Select s.RecordType.Name, s.Id 
                                            From Specification__c s
                                            Where s.Id =: specId];
        
        Schema.DescribeSObjectResult R = Specification__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        Set<String> recordTypeName = new Set<String>();
        for(Schema.RecordTypeInfo RTInfo : RT)
        {
            System.debug('Record Type Name ' + RTInfo.getName());
            recordTypeName.add(RTInfo.getName());
        }
        if(recordTypeName.contains(spec.RecordType.Name + apvRecordTypeAppend)){
            apvRecordTypeName = spec.RecordType.Name + apvRecordTypeAppend;
            return true;
        }
        else{
            oldRecordTypeName = spec.RecordType.Name;
            apvRecordTypeName = spec.RecordType.Name + apvRecordTypeAppend;
            return false;
        }
    }
    */
        
    public static string getCreatableFieldsSOQL(String objectName, String whereClause, String fieldFilterOn){
        
        String selects = '';
        
        if (whereClause == null || whereClause == ''){ return null; }
        
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    System.debug('Name: ' + fd.getName());
                    selectFields.add(fd.getName());
                }
            }
        }
        
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            }
            
        }
        whereClause =  fieldFilterOn + '=\'' + whereClause + '\'';
        return 'SELECT ' + String.escapeSingleQuotes(selects) + ' FROM ' + String.escapeSingleQuotes(objectName) + ' WHERE ' + String.escapeSingleQuotes(whereClause);
        
    }
  
    /*
    public id getRecordTypeId(String name) {
        String recordTypeName = name;
        Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Specification__c.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtInfo = rtMapByName.get(recordTypeName);
        return rtInfo.getRecordTypeId();
    }
    */
    
    public Specification__c cloneOldSpec(){
        Boolean isSpecificationCreateable = Schema.sObjectType.Specification__c.isCreateable() && 
            				Schema.sObjectType.Specification__c.fields.Name.isCreateable() &&
            				Schema.sObjectType.Specification__c.fields.Master_Specification__c.isCreateable() &&
                            Schema.sObjectType.Specification__c.fields.Supplier__c.isCreateable() &&
                            Schema.sObjectType.Specification__c.fields.Status__c.isCreateable();
        if(!isSpecificationCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            return null;
        }
        String allFieldQuery = getCreatableFieldsSOQL(sObjectName, specId, 'Id');
        Specification__c oldSpec = (Specification__c)Database.query(allFieldQuery);
        Specification__c clonedSpec = oldSpec.clone(false, true);
        //clonedSpec.RecordTypeId = getRecordTypeId(apvRecordTypeName);
        Account specSupName = [Select id, Name
                               From Account
                               Where id =: specSupplier.Supplier__c limit 1];
        clonedSpec.Name = clonedSPec.Name + ' - ' + specSupName.Name;
        clonedSpec.Master_Specification__c = specId;
        clonedSpec.Supplier__c = specSupplier.Supplier__c;
        clonedSpec.Status__c = 'In Process';
        return clonedSpec;
    }
    
    public void cloneFlexiableRelatedSpec(String oldSpecId, Id newSpecId){
        String allCoatingFieldQuery = getCreatableFieldsSOQL('specright__Coatings__c', oldSpecId, 'Specification__c');
        List<Coatings__c> oldCoatingsList = Database.query(allCoatingFieldQuery);
        if(!oldCoatingsList.isEmpty()){
            for(Coatings__c oldCoatings : oldCoatingsList){
                Boolean isCoatingsCreateable = Schema.sObjectType.Coatings__c.isCreateable() && 
            				Schema.sObjectType.Coatings__c.fields.Specification__c.isCreateable();
                if(!isCoatingsCreateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            		return;
                }
                Coatings__c newCoatings = oldCoatings.clone(false, true);
                newCoatings.Specification__c = newSpecId;
                newCoatingsList.add(newCoatings);
            }
            if(!Schema.sObjectType.Coatings__c.isCreateable()){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access.'));
            }
            else{
                insert newCoatingsList;
            }
        }
        String allLayerMaterialFieldQuery = getCreatableFieldsSOQL('specright__Layer_Material__c', oldSpecId, 'Specification__c');
		List<Layer_Material__c> oldLayerMaterialList = Database.query(allLayerMaterialFieldQuery);
        if(!oldLayerMaterialList.isEmpty()){
            for(Layer_Material__c oldLM : oldLayerMaterialList){
                Boolean isLayerMaterialCreateable = Schema.sObjectType.Layer_Material__c.isCreateable() && 
            				Schema.sObjectType.Layer_Material__c.fields.Specification__c.isCreateable();
                if(!isLayerMaterialCreateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            		return;
                }
                Layer_Material__c newLM = oldLM.clone(false, true);
                newLM.Specification__c = newSpecId;
                newLayerMaterialList.add(newLM);
            }
            if(!Schema.sObjectType.Layer_Material__c.isCreateable()){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access.'));
            }
            else{
                insert newLayerMaterialList;
            }
        }
        String allEmbellishmentsFieldQuery = getCreatableFieldsSOQL('specright__Embellishments__c', oldSpecId, 'Specification__c');
        List<Embellishments__c> oldEmbellList = Database.query(allEmbellishmentsFieldQuery);
        if(!oldEmbellList.isEmpty()){
            for(Embellishments__c oldEmbell : oldEmbellList){
                Boolean isEmbellishmentsCreateable = Schema.sObjectType.Embellishments__c.isCreateable() && 
            				Schema.sObjectType.Embellishments__c.fields.Specification__c.isCreateable();
                if(!isEmbellishmentsCreateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            		return;
                }
                Embellishments__c newEmbell = oldEmbell.clone(false, true);
                newEmbell.Specification__c = newSpecId;
                newEmbellishmentsList.add(newEmbell);
            }
            if(!Schema.sObjectType.Embellishments__c.isCreateable()){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access.'));
            }
            else{
                insert newEmbellishmentsList;
            }
        }
        String allPMSFieldQuery = getCreatableFieldsSOQL('specright__PMS_Instructions__c', oldSpecId, 'Specification__c');
        List<PMS_Instructions__c> oldPMSList = Database.query(allPMSFieldQuery);
        if(!oldPMSList.isEmpty()){
            for(PMS_Instructions__c oldPMS : oldPMSList){
                Boolean isPMSInstructionsCreateable = Schema.sObjectType.PMS_Instructions__c.isCreateable() && 
            				Schema.sObjectType.PMS_Instructions__c.fields.Specification__c.isCreateable();
                if(!isPMSInstructionsCreateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            		return;
                }
                PMS_Instructions__c newPMS = oldPMS.clone(false, true);
                newPMS.Specification__c = newSpecId;
                newPMSInstructionsList.add(newPMS);
            }
            if(!Schema.sObjectType.PMS_Instructions__c.isCreateable()){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access.'));
            }
            else{
                insert newPMSInstructionsList;
            }
        }
    }
}
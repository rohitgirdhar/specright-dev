public with sharing class CustomInOutboundDirCtlr {
    ApexPages.standardController std;
    private Specification__c parentSpec;
    public SObjectType parentSobType;
    public SObjectType childSobType;
    public SObjectType outboundChildSobType;
    public CustomInOutboundDirCtlr(ApexPages.StandardController std) {
        this.parentSobType = Specification__c.SObjectType;
        this.childSobType  = Inbound_Material__c.SObjectType;
        this.outboundChildSobType = Outbound_Finished_Product__c.SObjectType;
        
        this.std = std;
    }
    
    public Specification__c getParentSpec(Id specId) {
        return [Select Id, Name
                From Specification__c
                Where Id =: specId limit 1];
    }
    
    public pageReference DirectToPage() {
        PageReference p = new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        Id specId = getParentSObjectId(m);
        parentSpec = getParentSpec(specId);
        id recordTypeId = getChildRecordTypeId(specId, childSobType);
        if(recordTypeId != null)
        {
            m.put('Name', parentSpec.Name);
            m.put('RecordType', recordTypeId);
            m.put('nooverride', '1');
            return p;
        }
        else{
            return null;
        }
        
    }
    
    public pageReference DirectToPageOutbound() {
        PageReference p = new PageReference('/' + outboundChildSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        Id specId = getParentSObjectId(m);
        List<Spec_Family__c> specFamilyRecord = [Select id, Name
                                                 From Spec_Family__c
                                                 Where id =: specId limit 1];
        if(!specFamilyRecord.isEmpty()){
            id recordTypeId = Schema.SObjectType.Outbound_Finished_Product__c.getRecordTypeInfosByName().get('Spec BOM').getRecordTypeId();
            System.debug('RTID: ' + recordTypeId);
            m.put('Name', specFamilyRecord.get(0).Name);
            m.put('RecordType', recordTypeId);
            m.put('nooverride', '1');
            return p;
        }
        else{
            parentSpec = getParentSpec(specId);
            id recordTypeId = getChildRecordTypeId(specId, outboundChildSobType);
            if(recordTypeId != null)
            {
                m.put('Name', parentSpec.Name);
                m.put('RecordType', recordTypeId);
                m.put('nooverride', '1');
                return p;
            }
            else{
                return null;
            }  
        }
              
    }
    
    private Id getChildRecordTypeId(Id parentId, SObjectType childType) {
        SObject parent = Database.query('Select RecordType.DeveloperName From ' + String.valueOf(parentSobType)
                + ' where Id = :parentId');
        String parentDeveloperName = (String) parent.getSobject('RecordType').get('DeveloperName');
        String childTypeUnmanged = String.valueOf(childType);
        List<RecordType> recordTypeIdList = [Select Id
                                   From RecordType 
                                   Where SObjectType =: String.valueOf(childType)
                                   and DeveloperName =: parentDeveloperName];
        if(!recordTypeIdList.isEmpty()){
            return recordTypeIdList.get(0).Id;
        }
        else{
            return null;
        }
    }
    
    private Id getParentSObjectId(Map<String, String> m) {
        for (String key : m.keySet()) {
            if (key.endsWith('_lkid')) {
                return m.get(key);
            }
        }
        return null;
    }
    
    public Set<String> blackListRecordType() {
        Set<String> blackList = new Set<String>();
        blackList.add('Master');
        blackList.add('Corrugated');
        blackList.add('Flexible');
        blackList.add('Spec BOM');
        return blackList;
    }
    
    public String SelectedRecordType {get; set;}  
    public List<SelectOption> getOptionsrecordTypeInbound(){
        List<SelectOption> recrodTypeOptions = new List<Selectoption>();
        
        Schema.DescribeSObjectResult R = Inbound_Material__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        for(Schema.RecordTypeInfo obj : RT){
            if(!blackListRecordType().contains(obj.getName())){
                recrodTypeOptions.add(new selectOption(obj.getRecordTypeId(), obj.getName()));
            }
        }            
        return recrodTypeOptions;
    }
    
    public List<SelectOption> getOptionsrecordTypeOutbound(){
        
        List<SelectOption> recrodTypeOptions = new List<Selectoption>();
        
        Schema.DescribeSObjectResult R = Outbound_Finished_Product__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        for(Schema.RecordTypeInfo obj : RT){
            if(!blackListRecordType().contains(obj.getName())){
                recrodTypeOptions.add(new selectOption(obj.getRecordTypeId(), obj.getName()));
            }
        }            
        
        return recrodTypeOptions;
    }
    
    public pageReference ToEditPage(){
        PageReference p = new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        //Id specId = getParentSObjectId(m);
        //parentSpec = getParentSpec(specId);
        m.putAll(ApexPages.currentPage().getParameters());
        m.put('Name', parentSpec.Name);
        m.put('RecordType', SelectedRecordType);
        m.put('nooverride', '1');
        return p;
    }
    
    public pageReference ToEditPageOutbound(){
        PageReference p = new PageReference('/' + outboundChildSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        //Id specId = getParentSObjectId(m);
        //parentSpec = getParentSpec(specId);
        m.putAll(ApexPages.currentPage().getParameters());
        m.put('Name', parentSpec.Name);
        m.put('RecordType', SelectedRecordType);
        m.put('nooverride', '1');
        return p;
    }
    
    public pageReference BackToSpec(){
        return std.cancel();
    }
}
@isTest
public with sharing class SpecEventBatchTest {
    
    public static testMethod void validatespeceventbatchController(){
     	SpecEventBatch objSpecEventBatch = new SpecEventBatch();
         Specification__c objSpecification = new Specification__c();
         objSpecification.Name = 'Test internalPart';
         system.assertEquals('Test internalPart',objSpecification.Name);  
         objSpecification.Prior_Part_Numbers__c = 'Test priorPart';
         insert objSpecification;
        Database.executeBatch(objSpecEventBatch);

    }
	   

}
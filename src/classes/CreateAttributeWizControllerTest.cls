@IsTest
public class CreateAttributeWizControllerTest {
    
    private class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			if(request instanceof MetadataService.retrieve_element)
				response.put('response_x', new MetadataService.retrieveResponse_element());
			else if(request instanceof MetadataService.checkDeployStatus_element)
				response.put('response_x', new MetadataService.checkDeployStatusResponse_element());
			else if(request instanceof MetadataService.listMetadata_element)
				response.put('response_x', new MetadataService.listMetadataResponse_element());
			else if(request instanceof MetadataService.checkRetrieveStatus_element)
				response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			else if(request instanceof MetadataService.describeMetadata_element)
				response.put('response_x', new MetadataService.describeMetadataResponse_element());
			else if(request instanceof MetadataService.deploy_element)
				response.put('response_x', new MetadataService.deployResponse_element());
            else if(request instanceof MetadataService.updateMetadata_element)
                response.put('response_x', new MetadataService.updateMetadataResponse_element());
            else if(request instanceof MetadataService.renameMetadata_element)
                response.put('response_x', new MetadataService.renameMetadataResponse_element());
            else if(request instanceof  MetadataService.cancelDeploy_element)
                response.put('response_x', new MetadataService.cancelDeployResponse_element());
            else if(request instanceof  MetadataService.deleteMetadata_element)
                response.put('response_x', new MetadataService.deleteMetadataResponse_element());
            else if(request instanceof  MetadataService.upsertMetadata_element)
                response.put('response_x', new MetadataService.upsertMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element)
                response.put('response_x', new MetadataService.createMetadataResponse_element());
            else if(request instanceof  MetadataService.deployRecentValidation_element)
                response.put('response_x', new MetadataService.deployRecentValidationResponse_element());
            else if(request instanceof MetadataService.describeValueType_element)
                response.put('response_x', new MetadataService.describeValueTypeResponse_element());
            else if(request instanceof MetadataService.checkRetrieveStatus_element)
                response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			return;
		}
	}
    
	@IsTest
    private static void testErrorStatus(){
        CreateAttributeWizController controller = new CreateAttributeWizController();
        controller.setFieldLengthError();
        controller.setFieldCheckBoxDefaultValueError();
        controller.setfieldDisplayFormatError();
        controller.setFieldNameError();
        controller.setfieldPicklistSortedError();
        controller.setFieldPicklistValuesError();
        controller.setFieldscaleError();
        controller.setFieldPrecisionError();
        controller.setFieldstartingNumberError();
        System.assertEquals(true, controller.fieldLengthError);
        System.assertEquals(true, controller.fieldCheckboxDefaultValueError);
        System.assertEquals(true, controller.fieldDisplayFormatError);
        System.assertEquals(true, controller.fieldNameError);
        controller.resetErrorMessage();
        System.assertEquals(false, controller.fieldLengthError);
        System.assertEquals(false, controller.fieldCheckboxDefaultValueError);
        System.assertEquals(false, controller.fieldDisplayFormatError);
        System.assertEquals(false, controller.fieldNameError);
    }
    
    @IsTest
    private static void pageReferenceTest() {
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        CreateAttributeWizController controller = new CreateAttributeWizController();
        controller.fieldType = 'Text';
        controller.fieldName = 'abc';
        controller.fieldCheckboxDefaultValue = 'true';
        controller.fieldDisplayFormat = 'A-{0000}';
        controller.fieldLength = 1;
        controller.fieldPicklistSorted = false;
        controller.fieldPicklistValues = 'abcd';
        controller.fieldPrecision = 1;
        controller.fieldscale = 1;
        controller.fieldstartingNumber = 1;
        controller.fieldVisibleLines = 1;
        pagereference pg = controller.goToAttributeDetail();
        System.assertEquals(true, controller.showAttributeDetail);
        pg = controller.backToSelectType();
        System.assertEquals(true, controller.showSelectType);
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Number';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'AutoNumber';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Checkbox';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldCheckboxDefaultValue = null;
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Currency';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Date';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Percent';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'Picklist';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'MultiselectPicklist';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'TextArea';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        controller.fieldType = 'LongTextArea';
        pg = controller.goToProfilePermissions();
        System.assertEquals(true, controller.showProfilePermission);
        
        String[] profiles = controller.getProfiles();
        profiles.add('ProfileName');
        controller.setProfiles(profiles);
        
        pg = controller.goToSelectRecordType();
        System.assertEquals(true, controller.showRecordType);
        pg = controller.backToSelectProfilePermission();
        System.assertEquals(true, controller.showProfilePermission);
        pg = controller.cancel();
        System.assertEquals(true, pg.getRedirect());
        
        String[] recordType = controller.getExistingRecordTypes();
        controller.setExistingRecordTypes(recordType);
        controller.newRecordTypeName = 'RecordTypeName';
        controller.newRecordTypeDescription = 'Some Description';
        pg = controller.goToConfirmation();
        System.assertEquals(true, pg.getRedirect());
        
    }
    
    @IsTest
    private static void selectOptionTest() {
        CreateAttributeWizController controller = new CreateAttributeWizController();
        List<SelectOption> checkbox = controller.getcheckboxDefaultValue();
        System.assertEquals(2, checkbox.size());
        
        List<SelectOption> typeOption = controller.gettypeOption();
        System.assertEquals(11, typeOption.size());
        
        List<SelectOption> selectProfile = controller.getprofileList();
        System.assertNotEquals(0, selectProfile.size());
        
        List<SelectOption> selectRecordType = controller.getexistingRecordTypeList();
        System.assertNotEquals(0, selectRecordType.size());
    }
}
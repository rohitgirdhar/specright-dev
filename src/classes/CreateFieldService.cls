public class CreateFieldService {
    private static MetadataService.CustomField defaultAttributeField (string fieldType, string objectName, string fieldLable){
        string newFieldName = convertToSalesforceAPIStyle(fieldLable);
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = objectName + '.' + newFieldName;
        customField.label = fieldLable;
        customField.type_x = fieldType;
        customField.trackHistory = false;
        return customField;
    }
    //Text Attribute
    public static MetadataService.CustomField createTextField (string fieldType, string objectName, string fieldLable, Integer length) {
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        customField.length = length;
        //saveField(customField);
        return customField;
    }
    
    public static MetadataService.CustomField createAutoNumberField(string fieldType, string objectName, string fieldLable, integer fieldstartingNumber, string fieldDisplayFormat){
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        customField.startingNumber = fieldstartingNumber;
        customField.displayFormat = fieldDisplayFormat;
        //saveField(customField);
        return customField;
    }
    
    //Number, Currency, Percent Attribute
    public static MetadataService.CustomField createNumberField(string fieldType, string objectName, string fieldLable, integer fieldPrecision, integer fieldscale){
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        customField.precision = fieldPrecision+fieldscale;
        customField.scale = fieldscale;
        //saveField(customField);
        return customField;
    }
    
    //Checkbox Attribute
    public static MetadataService.CustomField createCheckboxField(string fieldType, string objectName, string fieldLable, string defaultValue) {
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        customField.defaultValue = defaultValue;
        return customField;
    }
    
    //Date, TextArea Attribute
    public static MetadataService.CustomField createDateField(string fieldType, string objectName, string fieldLable){
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        //saveField(customField);
        return customField;
    }
    
    //Long Text Area Attribute
    public static MetadataService.CustomField createLongTextAreaField (string fieldType, string objectName, string fieldLable, integer length, integer visibleLines) {
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        customField.length = length;
        customField.visibleLines = visibleLines;
        return customField;
    }
    
    //Picklist, Multi-Picklist
    public static MetadataService.CustomField createPicklistField (string fieldType, string objectName, string fieldLable, string fieldPicklistValues, boolean fieldPicklistSorted, integer fieldvisibleLine){
        MetadataService.CustomField customField = defaultAttributeField(fieldType, objectName, fieldLable);
        MetadataService.Picklist pt = new MetadataService.Picklist();
        pt.picklistValues = new List<MetadataService.PicklistValue>{};
        pt.sorted= fieldPicklistSorted;
        List<String> pickListValues = fieldPicklistValues.split('\n');
        for(String value : pickListValues){
            MetadataService.PicklistValue pValue = new MetadataService.PicklistValue();
            pValue.fullName = value;
            pValue.default_x = false;
            pt.picklistValues.add(pValue);
        }
        if(fieldType.equals('MultiselectPicklist')){
            customField.visibleLines = fieldvisibleLine;
        }
        customField.picklist = pt;
        
        //saveField(customField);
        return customField;
    }
    
    public static void saveField (MetadataService.CustomField customField) {
        MetadataService.MetadataPort service = createService();
        service.createMetadata(new MetadataService.Metadata[] {customField});
        /* For Testing Only
        List<MetadataService.SaveResult> results =
            service.createMetadata(
                new MetadataService.Metadata[] { customField });
        handleSaveResults(results[0]);
        */
    }
    
    //Update FieldLevel Security
    public static MetadataService.Profile updateFieldLevelSecurity(String profileName, Boolean customProfile, MetadataService.CustomField field) {
        
        MetadataService.Profile oneProfile = new MetadataService.Profile();
        oneProfile.fullName = profileName;
        oneProfile.custom = customProfile;
        /*
        MetadataService.MetadataPort service = createService();
        MetadataService.Profile oneProfile = 
             (MetadataService.Profile) service.readMetadata('Profile', new String[] { profileName }).getRecords()[0];
        */
        MetadataService.ProfileFieldLevelSecurity fieldSec = new MetadataService.ProfileFieldLevelSecurity();
        fieldSec.field = field.fullName;
        fieldSec.editable = true;
        oneProfile.fieldPermissions /*.add(fieldSec); */= new MetadataService.ProfileFieldLevelSecurity[] {fieldSec};
        return oneProfile;
    }
    
    public static MetadataService.Profile updateRecordTypePermission(String profileName, MetadataService.RecordType recordTypeObject, String recordTypeName){
        MetadataService.MetadataPort service = createService();
        
        MetadataService.Profile oneProfile = new MetadataService.Profile();
        oneProfile.fullName = profileName;
        oneProfile.custom = true;
        
        MetadataService.ProfileRecordTypeVisibility recordTypeVisibility = new MetadataService.ProfileRecordTypeVisibility();
        recordTypeVisibility.default_x = false;
        recordTypeVisibility.recordType = recordTypeObject.fullName;
        recordTypeVisibility.visible = true;
        oneProfile.recordTypeVisibilities = new MetadataService.ProfileRecordTypeVisibility[] {recordTypeVisibility};
        MetadataService.ProfileLayoutAssignment layoutAssignment = new MetadataService.ProfileLayoutAssignment();
        layoutAssignment.layout = 'Specification__c-' + recordTypeName + ' Layout';
        string apiRecordTypeName = recordTypeName.replaceall('[^a-zA-Z0-9]', '_');
        apiRecordTypeName = apiRecordTypeName.replaceall('_+', '_');
        layoutAssignment.recordType = 'Specification__c.' + apiRecordTypeName;
        oneProfile.layoutAssignments = new MetadataService.ProfileLayoutAssignment[] {layoutAssignment};
        return oneProfile;
    }
    
    public static void saveFieldLevelSecurity(MetadataService.Profile profile) {
        MetadataService.MetadataPort service = createService();
        service.updateMetadata(new MetadataService.Metadata[] {profile});
        /*
        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { profile });
        handleSaveResults(results[0]);
        */
    }
    
    //Create new Record Type
    public static MetadataService.RecordType createNewRecordType(String objectName, String newRecordTypeName, String description){
        MetadataService.RecordType recordType = new MetadataService.RecordType();
        string apiRecordTypeName = newRecordTypeName.replaceall('[^a-zA-Z0-9]', '_');
        apiRecordTypeName = apiRecordTypeName.replaceall('_+', '_');
        recordType.active = true;
        recordType.fullName = objectName + '.' + apiRecordTypeName;

        recordType.label = newRecordTypeName;
        recordType.businessProcess = null;
        recordType.description = description;
        return recordType;
    }

    public static void saveNewRecordType(MetadataService.RecordType newRecordType){
        MetadataService.MetadataPort service = createService();
        service.createMetadata(new MetadataService.Metadata[] {newRecordType});
        /*List<MetadataService.SaveResult> results =
            service.createMetadata(
                new MetadataService.Metadata[] { newRecordType });
        handleSaveResults(results[0]);*/
    }


    //Add Field To exsiting Record Type
    public static MetadataService.Layout addFieldToLayout(String objectName, String layoutName, MetadataService.CustomField field)
    {
        MetadataService.MetadataPort service = createService();
        // Read the Layout
        //'-specright__'
        String layoutAPI = objectName + '-' + layoutName;
        
        MetadataService.Layout layout =
            (MetadataService.Layout) service.readMetadata('Layout',
                new String[] { layoutAPI }).getRecords()[0];
        // Add Layout section, layout, item and field
        if(layout.layoutSections==null)
            layout.layoutSections = new List<MetadataService.LayoutSection>();
        MetadataService.LayoutSection newLayoutSection = layout.layoutSections[0];
        MetadataService.LayoutColumn newLayoutColumn = newLayoutSection.layoutColumns[0];
        MetadataService.LayoutItem newLayoutItem = new MetadataService.LayoutItem();
        
        newLayoutItem.field = field.label.replaceall('\\s+', '_')+ '__c';//field.fullName;
        newLayoutColumn.layoutItems.add(newLayoutItem);
        newLayoutSection.layoutColumns[0] = newLayoutColumn; //= new List<MetadataService.LayoutColumn> { newLayoutColumn };
        layout.layoutSections[0] = newLayoutSection;
        
        //layout.relatedLists = new List<MetadataService.RelatedListItem>();
        
        return layout;        
    }
    
    public static MetadataService.Layout addFieldToLayout(String objectName, String layoutName, String[] fieldsToAdd, Boolean required)
    {
        MetadataService.MetadataPort service = createService();
        // Read the Layout
        //-specright__
        String layoutAPI = objectName + '-' + layoutName;
        
        MetadataService.Layout layout =
            (MetadataService.Layout) service.readMetadata('Layout',
                new String[] { layoutAPI }).getRecords()[0];
        // Add Layout section, layout, item and field
        if(layout.layoutSections==null)
            layout.layoutSections = new List<MetadataService.LayoutSection>();
        for(String fieldAPI : fieldsToAdd){
            MetadataService.LayoutSection newLayoutSection = layout.layoutSections[0];
            MetadataService.LayoutColumn newLayoutColumn = newLayoutSection.layoutColumns[1];
            MetadataService.LayoutItem newLayoutItem = new MetadataService.LayoutItem();
            if(required){
                newLayoutItem.behavior = 'Required';
            }
            newLayoutItem.field = fieldAPI;//field.fullName;
            newLayoutColumn.layoutItems.add(newLayoutItem);
            newLayoutSection.layoutColumns[1] = newLayoutColumn; //= new List<MetadataService.LayoutColumn> { newLayoutColumn };
            layout.layoutSections[0] = newLayoutSection;
        }
        
        
        //layout.relatedLists = new List<MetadataService.RelatedListItem>();
        
        return layout;        
    }
    
    public static void saveAddFieldToLayout(MetadataService.Layout layout){
        MetadataService.MetadataPort service = createService();
        service.updateMetadata(new MetadataService.Metadata[] { layout });
        /* Test only
        handleSaveResults(
            service.updateMetadata(
                new MetadataService.Metadata[] { layout })[0]);
        */
    }
        
    //Create new PageLayout    
    public static MetadataService.Layout createLayout(String objectName, String recordTypeName, MetadataService.CustomField field)
    {
        MetadataService.Layout layout = new MetadataService.Layout();
        //'-specright__'
        layout.fullName = objectName + '-' + recordTypeName + ' Layout';//'Test__c-My Layout';
        layout.layoutSections = new List<MetadataService.LayoutSection>();
        MetadataService.LayoutSection layoutSection = new MetadataService.LayoutSection();
        layoutSection.editHeading = true;
        layoutSection.label = 'Information';
        layoutSection.style = 'TwoColumnsTopToBottom';
        layoutSection.layoutColumns = new List<MetadataService.LayoutColumn>();
        MetadataService.LayoutColumn layoutColumn = new MetadataService.LayoutColumn();
        layoutColumn.layoutItems = new List<MetadataService.LayoutItem>();
        
        MetadataService.LayoutItem nativeNameItem = new MetadataService.LayoutItem();
        nativeNameItem.behavior = 'Required';
        nativeNameItem.field = 'Name';
        layoutColumn.layoutItems.add(nativeNameItem);
        
        if(field != null){
            MetadataService.LayoutItem layoutCustomItem1 = new MetadataService.LayoutItem();
            //'specright__'
            layoutCustomItem1.field = '' + field.label.replaceall('[^a-zA-Z0-9]', '_')+ '__c';
            layoutColumn.layoutItems.add(layoutCustomItem1);
        }
        
        layoutSection.layoutColumns.add(layoutColumn);
        layout.layoutSections.add(layoutSection);
        return layout;

    }
    
    public static void saveCreateLayout(MetadataService.Layout layout){
        MetadataService.MetadataPort service = createService();
        service.createMetadata(new MetadataService.Metadata[] {layout});
        /*
        List<MetadataService.SaveResult> results =
            service.createMetadata(
                new MetadataService.Metadata[] { layout });
        handleSaveResults(results[0]);
        */
    }
    
    /*
    public static void copyLayoutSection(String objectName, String sourceLayoutName, String targetLayoutName)
    {
        //'-specright__'
        String sourceLayoutNameAPI = objectName + '-' + sourceLayoutName;
        
        System.debug('Source API Name: ' + sourceLayoutNameAPI);
        
        //'-specright__'
        String targetLayoutNameAPI = objectName + '-' + targetLayoutName;
        MetadataService.MetadataPort service = createService();
        // Read the source Layout
        MetadataService.Layout sourceLayout =
            (MetadataService.Layout) service.readMetadata('Layout',
                new String[] { sourceLayoutNameAPI }).getRecords()[0];
        System.debug('SourceLayout: ' + sourceLayout);
        // Read the target Layout
        MetadataService.Layout targetLayout =
            (MetadataService.Layout) service.readMetadata('Layout',
                new String[] { targetLayoutNameAPI }).getRecords()[0];
        
        targetLayout.layoutSections = new MetadataService.LayoutSection[]{};
        
        for(Integer x=0; x < sourceLayout.layoutSections.size()-1; x++){
            targetLayout.layoutSections.add(sourceLayout.layoutSections[x]);
        }
        
        // Update target Layout
        handleSaveResults(
            service.updateMetadata(
                new MetadataService.Metadata[] { targetLayout })[0]);
    }
    */
    //Get Metadata Service 
    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }
    /* Test Only
    public static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new MetadataServiceExamplesException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new MetadataServiceExamplesException('Request failed with no specified error.');
    }
    
    public class MetadataServiceExamplesException extends Exception { }
    //*/
    public static String convertToSalesforceAPIStyle(String input){
        return input.replaceall('\\s+', '_')+ '__c';
    }
}
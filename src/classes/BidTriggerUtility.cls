public with sharing class BidTriggerUtility{                                                                       
    
    @future
    public static void triggerUnshare(Set<Id> bidSetIds/*List<Bid__c> lstBid, Map<Id,Bid__c> oldMap*/){
        List<Bid__c> lstBid = [Select Bid_Status__c, Id, Bid_Request__c, specright__Specification__c
                               From Bid__c
                               Where Id in :bidSetIds];
        Set<Id> setBidReqIds = new Set<Id>(); 
        Set<id> setSpecId = new Set<Id>();
        Set<Id> setUserIds = new Set<id>();
        Set<Id> setVendorIds = new Set<Id>(); 
        List<Spec_Event__c> lstSpecEvent = new List<Spec_Event__c>();  //List of SpecEvents to Insert    
        List<Bid_Request__c> lstBidReq = new List<Bid_Request__c>();
        Map<Id,Set<Id>> mapSpecIdUserIds = new Map<Id,Set<Id>>(); 
        Map<Id,Set<Id>> mapAwardedSpecIdUserIds = new Map<Id,Set<Id>>(); 
        List<Specification__Share > lstShareDelete = new List<Specification__Share>();   
        List<Spec_File__Share> lstSpecFileShareDelete = new List<Spec_File__Share>();
        List<Bid__Share> lstBidShareDelete = new List<Bid__Share>();
        List<Spec_File__Share> lstSpecFileToInsert = new List<Spec_File__Share>();
        Map<Id,List<Id>> mapVendorUserId = new Map<Id,List<Id>>(); 
        Set<Id> setAwardedBids = new Set<Id>();
        Map<Id,Set<Id>> mapBidUserId = new Map<Id,Set<Id>>();
        
        for(Bid__c objBid : lstBid ) {
            if(objBid.Bid_Status__c=='Awarded' && objBid.Bid_Request__c!=null){                        
                setBidReqIds.add(objBid.Bid_Request__c);  
                setAwardedBids.add(objBid.id);  
                setSpecId.add(objBid.specification__c);       
            } 
            /*
            //Check that Status field values is changed to Awarded and add create a SpecEvent 'Bid Award'
            if(oldMap.get(objBid.id).Bid_Status__c!=objBid.Bid_Status__c && objBid.Bid_Status__c=='Awarded'){  
                Spec_Event__c objSpecEvent = new Spec_Event__c();
                objSpecEvent.Specification__c=objBid.Specification__c;
                objSpecEvent.Event_Type__c='Bid Award';
                objSpecEvent.User__c=UserInfo.getUserID();
                lstSpecEvent.add(objSpecEvent);
            }  
			*/
        }   
              
        
        //get all parent bidrequest of awarded bids
        lstBidReq = [select id,Specification__c,Bid_Status__c,(select id,Supplier__c from Bids__r) from Bid_Request__c 
                        where id in :setBidReqIds];                  
            
        //get spec file id with respect to spec id
        Map<Id,Spec_File__c> mapSpecFile = new Map<Id,Spec_File__c>([select id,specification__c,File_Type__c 
                    from spec_file__c where specification__c in :setSpecId]);            
            
        //Get all vendor ids
        for(Bid_Request__c objBidReq: lstBidReq){        
            for(Bid__c objBid:objBidReq.Bids__r){             
                setVendorIds.add(objBid.Supplier__c);           
            }
        }
        

        //get userids of respective vendors
        for(User u: [select id,contact.AccountId from user where contact.AccountId in :setVendorIds]){
            setUserIds.add(u.id);
            //if vendor id key exists thn add user id to the list
            if(mapVendorUserId.keyset().contains(u.contact.AccountId)){
                mapVendorUserId.get(u.contact.AccountId).add(u.id);
            }
            //else put new key with new list consisting current user id
            else {
                mapVendorUserId.put(u.contact.AccountId,new List<Id>{u.id});
            }
        }      
        
        //update bid request with status awarded
        for(Bid_Request__c objBidReq: lstBidReq){
            System.debug('Start Loop1');
            objBidReq.Bid_Status__c = 'Awarded';
            for(Bid__c objBid:objBidReq.Bids__r){ 
                if(!setAwardedBids.contains(objBid.id)){  
                    if(mapVendorUserId.keyset().contains(objBid.Supplier__c)){
                    mapBidUserId.put(objBid.id,new Set<Id>(mapVendorUserId.get(objBid.Supplier__c)));   }   
                    //if spec id key exists thn add userids of respective vendors to the list
                    if(mapSpecIdUserIds.keyset().contains(objBidReq.Specification__c) && mapVendorUserId.keyset().contains(objBid.Supplier__c)){
                        mapSpecIdUserIds.get(objBidReq.Specification__c).addAll(mapVendorUserId.get(objBid.Supplier__c));
                    }
                    //else put new key with new list consisting current user id
                    else if(!mapSpecIdUserIds.keyset().contains(objBidReq.Specification__c) && mapVendorUserId.keyset().contains(objBid.Supplier__c)){
                        mapSpecIdUserIds.put(objBidReq.Specification__c,new Set<Id>(mapVendorUserId.get(objBid.Supplier__c)));
                    }
                }
                else {
                    //if spec id key exists thn add userids of respective vendors to the list
                    if(mapAwardedSpecIdUserIds.keyset().contains(objBidReq.Specification__c) && mapVendorUserId.keyset().contains(objBid.Supplier__c)){
                        mapAwardedSpecIdUserIds.get(objBidReq.Specification__c).addAll(mapVendorUserId.get(objBid.Supplier__c));
                    }
                    //else put new key with new list consisting current user id
                    else if(!mapAwardedSpecIdUserIds.keyset().contains(objBidReq.Specification__c) && mapVendorUserId.keyset().contains(objBid.Supplier__c)){
                        mapAwardedSpecIdUserIds.put(objBidReq.Specification__c,new Set<Id>(mapVendorUserId.get(objBid.Supplier__c)));
                    }
                }
            }
        } 
      

        //create list of access shares to be deleted
        for(Bid__Share objShare: [select id,parentId,UserOrGroupId from Bid__Share where parentId in :mapBidUserId.keySet() and rowcause='Manual' and UserOrGroupId in :setUserIds]){
            if(mapBidUserId.keyset().contains(objShare.parentId) && mapBidUserId.get(objShare.parentId).contains(objShare.UserOrGroupId)){
                lstBidShareDelete.add(objShare);
            }        
        }
                

        //create list of specification shares to be deleted
        for(Specification__Share objShare: [select id,parentId,UserOrGroupId from Specification__Share where parentId in :mapSpecIdUserIds.keyset() and rowcause='Manual' and UserOrGroupId in :setUserIds]){
            if(mapSpecIdUserIds.keyset().contains(objShare.parentId) && mapSpecIdUserIds.get(objShare.parentId).contains(objShare.UserOrGroupId)){
                lstShareDelete.add(objShare);
            }        
        }
               
        
        //create list of spec file shares to be deleted
        for(Spec_File__Share objShare: [select id,parentId,UserOrGroupId from Spec_File__Share where parentId in :mapSpecFile.keyset() and rowcause='Manual' and UserOrGroupId in :setUserIds]){
            if(mapSpecFile.keyset().contains(objShare.parentId) && 
            mapSpecIdUserIds.keyset().contains(mapSpecFile.get(objShare.parentId).specification__c) &&
            mapSpecIdUserIds.get(mapSpecFile.get(objShare.parentId).specification__c).contains(objShare.UserOrGroupId)){
                lstSpecFileShareDelete.add(objShare);
            }
        }
        
        //create share records for spec files
        for(spec_file__c objSpecFile : mapSpecFile.Values()){
            if((objSpecFile.File_Type__c=='Level 4' || objSpecFile.File_Type__c=='Level 5') 
                            && mapAwardedSpecIdUserIds.keyset().contains(objSpecFile.Specification__c)){
                for(Id userId: mapAwardedSpecIdUserIds.get(objSpecFile.Specification__c)){
                    spec_file__Share objShare= new spec_file__Share();
                    objShare.AccessLevel = 'Read'; //Read
                    objShare.parentID = objSpecFile.Id; // Set the ID of record being shared.
                    objShare.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular file.                          
                    objShare.UserOrGroupId = userId;                           
                    lstSpecFileToInsert.add(objShare); 
                }
            }
        }     
       

        //Delete Specification share records
        if (lstShareDelete!=null && lstShareDelete.size()>0){        
            delete lstShareDelete;                    
        } 
       
        //Delete bid share records
        if (lstBidShareDelete!=null && lstBidShareDelete.size()>0){        
            delete lstBidShareDelete;                    
        } 
     

        //Delete Spec file share records
        if (lstSpecFileShareDelete!=null && lstSpecFileShareDelete.size()>0){        
            delete lstSpecFileShareDelete;                    
        } 
           

        //Insert level 4 and 5 Spec file share records
        if (lstSpecFileToInsert!=null && lstSpecFileToInsert.size()>0){        
            insert lstSpecFileToInsert;                    
        }
    
        //update Bid request to awarded
        if(lstBidReq.size()>0){
            update lstBidReq;
        }        
         
        //Insert spec events
        if(lstSpecEvent.size()>0){
            insert lstSpecEvent;   
        }  
    }       
    
    public static void bidsShare(List<Bid__c> lstBid){
        System.debug(lstBid);
        Set<Id> setVendorIds = new Set<Id>(); 
        Set<Id> setBidReqIds = new Set<Id>();  
        Set<Id> setUserIds = new Set<id>(); 
        Set<id> setSpecId = new Set<Id>();
        Map<Id,List<Id>> mapVendorUserId = new Map<Id,List<Id>>(); 
        //Map<Id,List<Id>> mapVendorContactId = new Map<Id,List<Id>>(); 
        //Map<Id,Id> mapContactIdUserId = new Map<Id,Id>(); 
        Map<Id,List<Id>> mapSpecUserId = new Map<Id,List<Id>>();  
        List<spec_file__c> lstSpecFile = new List<spec_file__c>();
        Map<id,EmailStaging__c> mapEmailStagToInsert = new Map<Id,EmailStaging__c>();
        
        for(Bid__c objBid : lstBid) { 
            if(objBid.Bid_Status__c!='Awarded'){  
                if(objBid.Supplier__c!=null){
                    setVendorIds.add(objBid.Supplier__c);  
                }
                if(objBid.Bid_Request__c!=null){
                    setBidReqIds.add(objBid.Bid_Request__c);
                } 
                setSpecId.add(objBid.specification__c); 
            }
        }  
        System.debug('SSSSV:'+setVendorIds);
        //CRUD & FLS Enforecement on Contact
        if(!Schema.sObjectType.Contact.isAccessible()){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
        }
        
        //construct map vendor & contact ids
        /*for(Contact objCon:[select id,AccountId from contact where email!=null and Accountid in: setVendorIds]){
            //if vendor id key exists thn add contact id to the list
            if(mapVendorContactId.keyset().contains(objCon.AccountId)){
                mapVendorContactId.get(objCon.AccountId).add(objCon.id);
            }
            //else put new key with new list consisting current contact id
            else {
                mapVendorContactId.put(objCon.AccountId,new List<Id>{objCon.id});
            }
        }   */   
        
        
        // Check CRUD & FLS Enforcement for Specification
        String [] SpecificationFields = new String [] {'Spec_Name__c','Name'};
        Map<String,Schema.SObjectField> mapSpec = Schema.SObjectType.Specification__c.fields.getMap();
        for (String fieldToCheck : SpecificationFields){
             if(!mapSpec.get(fieldToCheck).getDescribe().isAccessible()) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
             }
         }
        //get spec file id with respect to spec id
        lstSpecFile = [select id,specification__c from spec_file__c where specification__c in :setSpecId]; //Get all file type not level 1 2 3 and File_Type__c in ('Level 1','Level 2','Level 3')            
        
        //CRUD & FLS Enforecement on User
        if(!Schema.sObjectType.User.isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        //get userids of respective vendors
        for(User u: [select id,contactid,contact.AccountId from user where IsActive = true and contact.AccountId in :setVendorIds]){
            setUserIds.add(u.id);
            //if vendor id key exists thn add user id to the list
            if(mapVendorUserId.keyset().contains(u.contact.AccountId)){
                mapVendorUserId.get(u.contact.AccountId).add(u.id);
            }
            //else put new key with new list consisting current user id
            else {
                mapVendorUserId.put(u.contact.AccountId,new List<Id>{u.id});
            }
            
            //update map of contactid - user id
            //mapContactIdUserId.put(u.contactid,u.id);
        }                                       
        System.debug(mapVendorUserId);
        List<Specification__Share> lstSharedSpecification = new List<Specification__Share>();
        List<Bid__Share> lstSharedBid = new List<Bid__Share>();
        List<spec_file__Share> lstSharedSpecFile = new List<spec_file__Share>();
        
        //CRUD & FLS Enforecement on User
        if(!Schema.sObjectType.EmailTemplate.isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        //Get Email Template id
        List<EmailTemplate> lstTemp=[Select id from emailtemplate where name ='BidInviteSupplierTemplate'];
        String templateid='';
        if(lstTemp!=null && lstTemp.size()>0){
            templateid=lstTemp[0].id;
        }
        Map<Id, RecordType> mapRT = new Map<Id, RecordType>([Select Id, Name from RecordType where SObjectType = :'specright__Bid__c']);
        
        Boolean isBidCreateable = Schema.sObjectType.Bid__c.isCreateable() && 
            				Schema.sObjectType.Bid__c.fields.Access_Start__c.isCreateable();
        if(!isBidCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            return;
        }
        
        List<Bid__c> lstbidToUpdate = new List<Bid__c>();
        for(Bid__c objBid : lstBid ) {
            if( objBid.Bid_Status__c!='Awarded' && objBid.Supplier__c != NULL && mapVendorUserId.keySet().contains(objBid.Supplier__c)) {
                objBid.Access_Start__c = Date.today();
                lstbidToUpdate.add(objBid);       
                for(Id userId:mapVendorUserId.get(objBid.Supplier__c)){
                    //Create share records for Bid
                    Bid__Share objBidShare= new Bid__Share();
                    objBidShare.AccessLevel = 'Edit';
                    objBidShare.parentID = objBid.id; // Set the ID of record being shared.
                    objBidShare.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular bid record.                          
                    objBidShare.UserOrGroupId = userId;                           
                    lstSharedBid.add(objBidShare);  
                    
                    //Create share records for specification
                    Specification__Share objSpec= new Specification__Share();
                    objSpec.AccessLevel = 'Read'; //Read
                    objSpec.parentID = objBid.Specification__c; // Set the ID of record being shared.
                    objSpec.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular Specification record.                          
                    objSpec.UserOrGroupId = userId;                           
                    lstSharedSpecification.add(objSpec);     
                    
                    //if specification id key exists thn add user id to the list
                    if(mapSpecUserId.keyset().contains(objBid.Specification__c)){
                        mapSpecUserId.get(objBid.Specification__c).add(userId);
                    }
                    //else put new key with new list consisting current user id
                    else {
                        mapSpecUserId.put(objBid.Specification__c,new List<Id>{userId});
                    }

                    RecordType rt = mapRT.get(objBid.RecordTypeId);  
                    //create email staging
                    if(rt.Name == 'Bid' || rt.Name == 'Push Audit') {
                        EmailStaging__c objEmailStag = new EmailStaging__c(User__c=userId,type__c='Supplier Invitation');   
                        mapEmailStagToInsert.put(userId,objEmailStag);
                    }
                    /*else if(rt.Name == 'Push Audit') {
                        EmailStaging__c objEmailStag = new EmailStaging__c(User__c=userId,type__c='Supplier Invitation to Push Audit');   
                        mapEmailStagToInsert.put(userId,objEmailStag);
                    }*/
                }                                
            }                        
        }  
        
        System.debug('*****'+mapEmailStagToInsert);
        //insert email staging        
        if(mapEmailStagToInsert.size()>0){
            insert mapEmailStagToInsert.values(); 
        }
        
        //create share records for spec files
        for(spec_file__c objSpecFile : lstSpecFile){
            if(mapSpecUserId.keyset().contains(objSpecFile.Specification__c)){
                for(Id userId: mapSpecUserId.get(objSpecFile.Specification__c)){
                    spec_file__Share objShare= new spec_file__Share();
                    objShare.AccessLevel = 'Read'; //Read
                    objShare.parentID = objSpecFile.Id; // Set the ID of record being shared.
                    objShare.RowCause = 'Manual';
                    // Assign user id to grant read access to this particular file.                          
                    objShare.UserOrGroupId = userId;                           
                    lstSharedSpecFile.add(objShare); 
                }
            }
        }      
        
        
        //CRUD & FLS for Specification_Share
        if(!Schema.sObjectType.Specification__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        //insert specification share records
        if( lstSharedSpecification != null && lstSharedSpecification.size()>0 ) {        
            insert lstSharedSpecification;        
        }
        
        //CRUD & FLS for Spec_File__Share
        if(!Schema.sObjectType.spec_file__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        
        //insert spec file share records
        if( lstSharedSpecFile != null && lstSharedSpecFile.size()>0 ) {        
            insert lstSharedSpecFile;        
        }
        
        
        //CRUD & FLS for Bid__Share
        if(!Schema.sObjectType.Bid__Share.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
        }
        System.debug('****'+lstSharedBid);
        //insert bid share records
        if( lstSharedBid!= null && lstSharedBid.size()>0 ) {        
            
            insert lstSharedBid;        
        }
        if(lstbidToUpdate.size() > 0){
            if (Schema.sObjectType.Bid__c.isUpdateable()) {
                update lstbidToUpdate;
			}
            else{
                return;
            }
        }

    }
}
/**
 * apex class to migrate the Specification object to Inbound and Outbound
 * objects
 * @author - Wisdomedge Inc.
 */

global class SpecMigration implements Database.batchable<sObject> {
    
    public boolean overwriteFlag {get; set;}
    public SpecMigration(boolean overwrite) {
        this.overwriteFlag = overwrite;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select id from Specification__c where id != null]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        SpecMigration.splitSpecToInboundOutbound(scope, overwriteFlag);
    }

    global void finish(Database.BatchableContext BC){
    } 

    /**
     * @params
     * lstSpec - scope of specification records,
     * overwrite - if true it will first delete the related inbound and outbound objects
     */
    public static void splitSpecToInboundOutbound(List<Specification__c> scope, boolean overwrite) {
        List<specright__specification__c> lstFlexibleSpec=[Select id from specright__specification__c where recordtype.name='Flexible' and id in :scope];// order by LastmodifiedDate desc limit 1];
        List<specright__specification__c> lstCorrugatedSpec=[Select id from specright__specification__c where recordtype.name='Corrugated' and id in :scope];// order by LastmodifiedDate desc limit 1];
        List<specright__specification__c> lstSpec = lstFlexibleSpec;
        handleSpec(lstSpec, overwrite);

        // corrugated
        lstSpec = lstCorrugatedSpec;
        handleSpec(lstSpec, overwrite);
    }

    public static void handleSpec(List<specright__Specification__c> lstSpec, boolean overwrite) {
        /**
         * list of fields from Specification that should go to Inbound object
         */
            Map<String, String> mapInboundFld = new Map<String, String>{
            'specright__Inbound_Packaging_Method1__c'=>'specright__Inbound_Packaging_Method__c',
            'specright__Inbound_Corner_Board__c'=>'specright__Inbound_Corner_Board__c',
            'specright__Inbound_Pallet_Size__c'=>'specright__Inbound_Pallet_Size__c',
            'specright__Inbound_Shipping_Method1__c'=>'specright__Inbound_Shipping_Method__c',
            'specright__Inbound_Stretch_Wrap__c'=>'specright__Inbound_Stretch_Wrap__c',
            'specright__Inbound_Unit_Packaging_Method1__c'=>'specright__Inbound_Unit_Packaging_Method__c',
            'specright__Layers_Pallet__c'=>'specright__Layers_Pallet__c',
            'specright__Rolls_Layer__c'=>'specright__Rolls_Layer__c',
            //'specright__COC__c'=>'specright__COC__c',
            'specright__Product_Count_per_Unit__c'=>'specright__Product_Count_per_Unit__c',
            'specright__Stacking_Pattern__c'=>'specright__Stacking_Pattern__c',
            'specright__Size_Restriction__c'=>'specright__Size_Restriction__c',
            'specright__Special_Packing_Materials__c'=>'specright__Special_Packing_Materials__c',
            'specright__Special_Instructions_Level_3__c'=>'specright__Special_Instructions_Level_3__c',
            'specright__Load_Tags1__c'=>'specright__Load_Tags__c',
            'specright__Over_Under__c'=>'specright__Over_Under__c',
            'specright__Non_Conforming_Location__c'=>'specright__Non_Conforming_Location__c',
            'specright__VMI__c'=>'specright__VMI__c',
            'specright__Warehousing1__c'=>'specright__Warehousing__c',
            'specright__Location__c'=>'specright__Location__c'

        };

        /**
         * list of fields from Specification that should go to Outbound object
         */
        Map<String, String> mapOutboundFld = new Map<String, String>{
            'specright__Outbound_Corner_Board__c'=>'specright__Outbound_Corner_Board__c',
            'specright__Outbound_Packaging_Method__c'=>'specright__Outbound_Packaging_Method__c',
            'specright__Outbound_Pallet_Size__c'=>'specright__Outbound_Pallet_Size__c',
            'specright__Outbound_Paperwork_Required__c'=>'specright__Outbound_Paperwork_Required__c',
            'specright__Outbound_Shipping_Method__c'=>'specright__Outbound_Shipping_Method__c',
            'specright__Outbound_Stretch_Wrap__c'=>'specright__Outbound_Stretch_Wrap__c',
            'specright__Outbound_Unit_Packaging_Method__c'=>'specright__Outbound_Unit_Packaging_Method__c'

        };

        /**
         * delete any existing inbound and outbound objects if the overwrite is true
         */
        if(overwrite) {
            if(Schema.SObjectType.specright__Inbound_Material__c.isDeletable()){
                delete [Select id from specright__Inbound_Material__c where specright__specification__c in :lstSpec];
            }
            else{
                return;
            }
            if(Schema.SObjectType.specright__Outbound_Finished_Product__c.isDeletable()){
                delete [Select id from specright__Outbound_Finished_Product__c where specright__specificationL__c in :lstSpec];
            }
            else{
                return;
            }
        }

        /**
         * query the Specification object with all fields from inbound and outbound
         */
        String query = 'SELECT Id, recordtype.name ';

        // inbound object fields
        for(String s: mapInboundFld.keyset()) {
            query += ','+s;
        }
        // outbound object fields
        for(String s: mapOutboundFld.keyset()) {
            query += ','+s;
        }

        query +=' FROM specright__Specification__c'; 

        // query based on ids supplied as input
        query += ' WHERE id in (';
        String idlist = '';
        Map<Id, RecordType> mapInboundRT = new Map<Id, RecordType>(
            [Select id, SObjectType, DeveloperName from RecordType 
            where developerName in ('Corrugated', 'Flexible')
            and SObjectType = 'specright__Inbound_Material__c']);
        
        Map<Id, RecordType> mapOutboundRT = new Map<Id, RecordType>(
            [Select id, SObjectType, DeveloperName from RecordType 
            where developerName in ('Corrugated', 'Flexible')
            and SObjectType = 'specright__Outbound_Finished_Product__c']);
        
        for(specright__Specification__c spec: lstSpec) {
            if(idlist == '')
                idlist = '\'' + spec.id + '\'';
            else
                idlist += ',\'' + spec.id + '\'';
        }
        if(idlist != '')
            query += idlist + ')';
        System.debug(query);
        List<specright__Inbound_Material__c> lstInbound = new List<specright__Inbound_Material__c>();
        List<specright__Outbound_Finished_Product__c> lstOutbound = new List<specright__Outbound_Finished_Product__c>();

        for(specright__Specification__c spec: Database.query(query)) {
            specright__Inbound_Material__c inObj = new specright__Inbound_Material__c(specright__Specification__c=spec.id);
            // inbound object fields
            System.debug('>>>spec: ' + spec);
            for(String s: mapInboundFld.keyset()) {
               
                inObj.put(mapInboundFld.get(s), spec.get(s));
            }
            // set record type
            for(RecordType rt: mapInboundRT.values()) {
                System.debug('>>RT: ' + spec.recordtype.name + ', ' + rt.developername);
                if(spec.recordType.name == rt.developername) {
                    inObj.recordTypeId = rt.id;
                    break;
                }
            }
            lstInbound.add(inObj);
        	
            Boolean isOutboundFinishedProductCreateable = Schema.sObjectType.specright__Outbound_Finished_Product__c.isCreateable() && 
            				Schema.sObjectType.specright__Outbound_Finished_Product__c.fields.recordTypeId.isCreateable();
            if(!isOutboundFinishedProductCreateable){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
            	return;
            }
            
            specright__Outbound_Finished_Product__c outObj = new specright__Outbound_Finished_Product__c(specright__SpecificationL__c=spec.id);
            // outbound object fields
            for(String s: mapOutboundFld.keyset()) {
                outObj.put(mapOutboundFld.get(s), spec.get(s));
                // set record type
                for(RecordType rt: mapOutboundRT.values()) {
                    if(spec.recordType.name == rt.developername) {
                        outObj.recordTypeId = rt.id;
                        break;
                    }
                }
            
            }
            lstOutbound.add(outObj);
        }

        /**
         * commit to database
         */
        if(lstInbound.size() > 0) {
            if (Schema.sObjectType.specright__Inbound_Material__c.isUpdateable() &&
               Schema.sObjectType.specright__Inbound_Material__c.isCreateable()) {
                upsert lstInbound;
			}
            else{
                return;
            }
        }

        if(lstOutbound.size() > 0) {
            if (Schema.sObjectType.specright__Outbound_Finished_Product__c.isUpdateable() &&
                Schema.sObjectType.specright__Outbound_Finished_Product__c.isCreateable()) {
                upsert lstOutbound;
			}
            else{
                return;
            }
        }
    }

    /**
     * Script for Client orgs
     */
    /*
    boolean overwrite=true;
List<specright__Specification__c> scope =[Select id from specright__Specification__c];
List<specright__specification__c> lstFlexibleSpec=[Select id from specright__specification__c where recordtype.name='Flexible' and id in :scope];// order by LastmodifiedDate desc limit 1];
        List<specright__specification__c> lstSpec = lstFlexibleSpec;
                
/**
         * delete any existing inbound and outbound objects if the overwrite is true
         */
/*          if(overwrite) {
            delete [Select id from specright__Inbound_Material__c where specright__specification__c in :lstSpec];
            delete [Select id from specright__Outbound_Finished_Product__c where specright__specification__c in :lstSpec];
        }
        /**
         * list of fields from Specification that should go to Inbound object
         */
    /*  Map<String, String> mapInboundFld = new Map<String, String>{
            'specright__Inbound_Packaging_Method1__c'=>'specright__Inbound_Packaging_Method__c',
            'specright__Inbound_Corner_Board__c'=>'specright__Inbound_Corner_Board__c',
            'specright__Inbound_Pallet_Size__c'=>'specright__Inbound_Pallet_Size__c',
            'specright__Inbound_Shipping_Method1__c'=>'specright__Inbound_Shipping_Method__c',
            'specright__Inbound_Stretch_Wrap__c'=>'specright__Inbound_Stretch_Wrap__c',
            'specright__Inbound_Unit_Packaging_Method1__c'=>'specright__Inbound_Unit_Packaging_Method__c',
            'specright__Layers_Pallet__c'=>'specright__Layers_Pallet__c',
            'specright__Rolls_Layer__c'=>'specright__Rolls_Layer__c',
            //'specright__COC__c'=>'specright__COC__c',
            'specright__Product_Count_per_Unit__c'=>'specright__Product_Count_per_Unit__c',
            'specright__Stacking_Pattern__c'=>'specright__Stacking_Pattern__c',
            'specright__Size_Restriction__c'=>'specright__Size_Restriction__c',
            'specright__Special_Packing_Materials__c'=>'specright__Special_Packing_Materials__c',
            'specright__Special_Instructions_Level_3__c'=>'specright__Special_Instructions_Level_3__c',
            'specright__Load_Tags1__c'=>'specright__Load_Tags__c',
            'specright__Over_Under__c'=>'specright__Over_Under__c',
            'specright__Non_Conforming_Location__c'=>'specright__Non_Conforming_Location__c',
            'specright__VMI__c'=>'specright__VMI__c',
            'specright__Warehousing1__c'=>'specright__Warehousing__c',
            'specright__Location__c'=>'specright__Location__c'

        };

        /**
         * list of fields from Specification that should go to Outbound object
         */
        /*Map<String, String> mapOutboundFld = new Map<String, String>{
            'specright__Outbound_Corner_Board__c'=>'specright__Outbound_Corner_Board__c',
            'specright__Outbound_Packaging_Method__c'=>'specright__Outbound_Packaging_Method__c',
            'specright__Outbound_Pallet_Size__c'=>'specright__Outbound_Pallet_Size__c',
            'specright__Outbound_Paperwork_Required__c'=>'specright__Outbound_Paperwork_Required__c',
            'specright__Outbound_Shipping_Method__c'=>'specright__Outbound_Shipping_Method__c',
            'specright__Outbound_Stretch_Wrap__c'=>'specright__Outbound_Stretch_Wrap__c',
            'specright__Outbound_Unit_Packaging_Method__c'=>'specright__Outbound_Unit_Packaging_Method__c'

        };

        String query = 'SELECT Id, recordtype.name ';

        for(String s: mapInboundFld.keyset()) {
            query += ','+s;
        }
        for(String s: mapOutboundFld.keyset()) {
            query += ','+s;
        }

        query +=' FROM specright__Specification__c'; 
        // query based on ids supplied as input
        query += ' WHERE id in (';
        String idlist = '';
        Map<Id, RecordType> mapInboundRT = new Map<Id, RecordType>(
            [Select id, SObjectType, Name from RecordType 
            where developerName in ('Corrugated', 'Flexible')
            and SObjectType = 'specright__Inbound_Material__c']);
        
        Map<Id, RecordType> mapOutboundRT = new Map<Id, RecordType>(
            [Select id, SObjectType, Name from RecordType 
            where developerName in ('Corrugated', 'Flexible')
            and SObjectType = 'specright__Outbound_Finished_Product__c']);
        
        for(specright__Specification__c spec: lstSpec) {
            if(idlist == '')
                idlist = '\'' + spec.id + '\'';
            else
                idlist += ',\'' + spec.id + '\'';
        }
        if(idlist != '')
            query += idlist + ')';
        List<specright__Inbound_Material__c> lstInbound = new List<specright__Inbound_Material__c>();
        List<specright__Outbound_Finished_Product__c> lstOutbound = new List<specright__Outbound_Finished_Product__c>();

        for(specright__Specification__c spec: Database.query(query)) {
            specright__Inbound_Material__c inObj = new specright__Inbound_Material__c(specright__Specification__c=spec.id);
            // inbound object fields
            for(String s: mapInboundFld.keyset()) {
               
                System.debug('>>>key: ' + s + ',val: ' + spec.get(s));
                inObj.put(mapInboundFld.get(s), spec.get(s));
            }
            // set record type
            for(RecordType rt: mapInboundRT.values()) {
                System.debug('>>RT: ' + spec.recordtype.name + ', ' + rt.name);
                
                if(spec.recordType.name == rt.name) {
                    inObj.recordTypeId = rt.id;
                    break;
                }
            }
            lstInbound.add(inObj);
        
            specright__Outbound_Finished_Product__c outObj = new specright__Outbound_Finished_Product__c(specright__Specification__c=spec.id);
            for(String s: mapOutboundFld.keyset()) {
                outObj.put(mapOutboundFld.get(s), spec.get(s));
                for(RecordType rt: mapOutboundRT.values()) {
                    if(spec.recordType.name == rt.name) {
                        outObj.recordTypeId = rt.id;
                        break;
                    }
                }
            
            }
            lstOutbound.add(outObj);
        }

        if(lstInbound.size() > 0) {
            upsert lstInbound;
        }

        if(lstOutbound.size() > 0) {
            upsert lstOutbound;
        }
        /**
         * handle corrugated
         */
        /*List<specright__specification__c> lstCorrugatedSpec=[Select id from specright__specification__c where recordtype.name='Corrugated' and id in :lstSpec];// order by LastmodifiedDate desc limit 1];
        lstSpec = lstFlexibleSpec;
        
        query = 'SELECT Id, recordtype.name ';

        // inbound object fields
        for(String s: mapInboundFld.keyset()) {
            query += ','+s;
        }
        // outbound object fields
        for(String s: mapOutboundFld.keyset()) {
            query += ','+s;
        }

        query +=' FROM specright__Specification__c'; 

        // query based on ids supplied as input
        query += ' WHERE id in (';
        idlist = '';
        
        for(specright__Specification__c spec: lstSpec) {
            if(idlist == '')
                idlist = '\'' + spec.id + '\'';
            else
                idlist += ',\'' + spec.id + '\'';
        }
        if(idlist != '')
            query += idlist + ')';
        System.debug(query);
        lstInbound = new List<specright__Inbound_Material__c>();
        lstOutbound = new List<specright__Outbound_Finished_Product__c>();

        for(specright__Specification__c spec: Database.query(query)) {
            specright__Inbound_Material__c inObj = new specright__Inbound_Material__c(specright__Specification__c=spec.id);
            // inbound object fields
            System.debug('>>>spec: ' + spec);
            for(String s: mapInboundFld.keyset()) {
               
                System.debug('>>>key: ' + s + ',val: ' + spec.get(s));
                inObj.put(mapInboundFld.get(s), spec.get(s));
            }
            // set record type
            for(RecordType rt: mapInboundRT.values()) {
                System.debug('>>RT: ' + spec.recordtype.name + ', ' + rt.name);
                
                if(spec.recordType.name == rt.name) {
                    inObj.recordTypeId = rt.id;
                    break;
                }
            }
            lstInbound.add(inObj);
            specright__Outbound_Finished_Product__c outObj = new specright__Outbound_Finished_Product__c(specright__Specification__c=spec.id);
            for(String s: mapOutboundFld.keyset()) {
                outObj.put(mapOutboundFld.get(s), spec.get(s));
                for(RecordType rt: mapOutboundRT.values()) {
                    if(spec.recordType.name == rt.name) {
                        outObj.recordTypeId = rt.id;
                        break;
                    }
                }
            
            }
            lstOutbound.add(outObj);
        }

        if(lstInbound.size() > 0) {
            upsert lstInbound;
        }

        if(lstOutbound.size() > 0) {
            upsert lstOutbound;
        }
    */
}
public with sharing class BidController {

    public BidController(ApexPages.StandardController controller) {
        //pageRedirect();
    }
    
    public PageReference pageRedirect(){
        PageReference pg = Page.BidEdit;
        /**
         * Bid_Group__c prefix
         */
        
        pg.getParameters().put('retURL','/' + BidGroup__c.sObjectType.getDescribe().getKeyPrefix() + '/o');
        //new PageReference('/apex/BidEdit?retURL=%2Fa0A%2Fo&save_new=1&sfdc.override=1');
        pg.setRedirect(true);
        return pg;
    }

}
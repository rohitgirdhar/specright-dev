public with sharing class SpecFileAttachDownloadController {
    string attId;
    string specId;
    string specFileId;
    
    public SpecFileAttachDownloadController(ApexPages.StandardController controller) {
        attId = ApexPages.currentPage().getParameters().get('AttachmentId'); 
        specId = ApexPages.currentPage().getParameters().get('SpecId');
        specFileId = ApexPages.currentPage().getParameters().get('SpecFileId');
        System.debug('Test: ' + specId + ' SpecFiledId: ' + specFileId);
        //sc=controller;
    }
    
    public pagereference downloadAtt(){
        Boolean isSpecEventCreateable = Schema.SObjectType.Spec_Event__c.isCreateable() &&
            							Schema.SObjectType.Spec_Event__c.fields.Spec_File__c.isCreateable() &&
            							Schema.SObjectType.Spec_Event__c.fields.Event_Type__c.isCreateable() &&
            							Schema.SObjectType.Spec_Event__c.fields.User__c.isCreateable();
        
        if(!isSpecEventCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.BidEdit_Insufficient_Access));
            return null;
        }
        Spec_Event__c objSpecEvent = new Spec_Event__c();
        //objSpecEvent.Specification__c=specId;
        objSpecEvent.Spec_File__c = specFileId;
        objSpecEvent.Event_Type__c='File Download';
        objSpecEvent.User__c=UserInfo.getUserID();
        
        if(Schema.sObjectType.Spec_Event__c.isCreateable()){
            insert objSpecEvent;
        }
        else{
            return null;
        }
        //return new pagereference('/servlet/servlet.FileDownload?file=' + attId);
        return new pagereference((Site.getPathPrefix() == null ? '' : Site.getPathPrefix()) + '/servlet/servlet.FileDownload?file=' + attId);
    }    

}
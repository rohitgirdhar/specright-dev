@isTest
private class AdministrationCtlrTest {
	
	static testmethod void testControllerWOOverwrite() {
		Test.startTest();
		AdministrationCtlr ctlr = new AdministrationCtlr();
		ctlr.splitSpec();
		Test.stopTest();
		System.assert(ctlr != null);
	}

	static testmethod void testControllerOverwrite() {
		Test.startTest();
		AdministrationCtlr ctlr = new AdministrationCtlr();
		ctlr.splitSpecOverwrite();
		Test.stopTest();
		System.assert(ctlr != null);
	}
}
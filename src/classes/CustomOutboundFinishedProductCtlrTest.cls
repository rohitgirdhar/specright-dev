@isTest
private class CustomOutboundFinishedProductCtlrTest {
    
    static Specification__c getFlexibleSpec() {
        RecordType rt =[select id ,Name from RecordType where Name = 'Flexible'  and 
                        sObjectType ='specright__Specification__c' limit 1];
        specright__Specification__c sp = new specright__Specification__c();
        sp.Name = 'Test Specification';
        sp.RecordTypeId = rt.id;
        insert sp;
        
        return sp;
    }
    
    static Outbound_Finished_Product__c getFlexibleOutboundProduct(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Flexible'  and 
                             sObjectType ='specright__Outbound_Finished_Product__c' limit 1];
        
        Outbound_Finished_Product__c IBM = new Outbound_Finished_Product__c();
        IBM.Name = 'Auto-generated';
        IBM.Location__c ='Test 234';
        IBM.SpecificationL__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;
        upsert IBM;
        return IBM;
    }
    
    static Outbound_Finished_Product__c getCorrugatedOutboundProduct(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Corrugated'  and 
                             sObjectType ='specright__Outbound_Finished_Product__c' limit 1];
        
        Outbound_Finished_Product__c IBM = new Outbound_Finished_Product__c();
        IBM.Name = 'Auto-generated';
        IBM.Location__c ='Test 234';
        IBM.SpecificationL__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;

        ApexPages.StandardController AController = new ApexPages.StandardController(IBM);
        Test.setCurrentPageReference(new PageReference('Page.CustomOutboundFinishedProduct')); 
        CustomOutboundFinishedProductCtlr controller = new CustomOutboundFinishedProductCtlr(AController);
        System.assertEquals(controller.outboundFinishedProductObj.Name , IBM.Name);
        
        return IBM;
    }
    
    static testMethod void  testCustomSavePositive() {
        Test.startTest();
        Specification__c flexibleSpec = getFlexibleSpec();
        // Handle test for Inbounds 
        Outbound_Finished_Product__c im = getFlexibleOutboundProduct(flexibleSpec);
        Test.stopTest();
        
        ApexPages.StandardController AController = new ApexPages.StandardController(im);
        Test.setCurrentPageReference(new PageReference('Page.CustomOutboundFinishedProduct')); 
        // test save for new record
        CustomOutboundFinishedProductCtlr controller = new CustomOutboundFinishedProductCtlr(AController);
        controller.CustomSave();
        System.assert(controller.outboundFinishedProductObj.id != null);
        
        // test edit scenario
        System.currentPageReference().getParameters().put('id', im.id);
        controller.customSaveAndNew();
    }
}
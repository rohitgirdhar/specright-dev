@isTest
public with sharing class SpecFileAttachDownloadControllerTest {
    public static testMethod void TestSpecFileAttachDownloadController() {
        Account a = new Account(Name='test Account');
        insert a;
        Test.StartTest();
        SpecFileAttachDownloadController spec = new SpecFileAttachDownloadController(new ApexPages.StandardController(a));
        Boolean isSpecEventCreateable= Schema.sObjectType.Spec_Event__c .isCreateable() && 
                                 //Schema.sObjectType.Spec_Event__c.fields.Specification__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.Event_Type__c.isCreateable() && 
                                 Schema.sObjectType.Spec_Event__c.fields.User__c.isCreateable();
        system.AssertEquals(isSpecEventCreateable, true);
        spec.downloadAtt();
        Test.StopTest();
    }
}
@isTest
private class BidRespondedQueueControllerTest {
      static testMethod void myUnitTest() {
            Specification__c Spec = new Specification__c();
            Spec.Name = 'Test 31/7';
            insert Spec;
             
            RecordType RT = [select id,Name from RecordType where SObjectType = :'specright__Bid__c' Limit 1]; 
            List<Bid__c > lstBidS = new List<Bid__c >();
            Bid__c bid = new Bid__c();
            bid.Bid_Status__c = 'Waiting for Response';
            //bid.Client_Award_Notes__c = 'Test';
            //bid.Client_Bid_Notes__c= 'test1';
            bid.RecordTypeId = RT.Id;
            //bid.Bid_Amount__c = 20;
            //bid.Bid_Delivery_Date__c = system.today();
            //bid.Bid_Due_Date__c = system.today()+5;
            //bid.Specification__c = Spec.id;
            //bid.Declined_Bid__c = false;
            //bid.Unit_Type__c = 'Pallet';
            //bid.Supplier_Bid_Notes__c = 'Description of Supplier';
            //bid.Quanity_per_Unit__c = 10;
            bid.Verified__c = 'Yes';
            lstBidS.add(bid);
            insert lstBidS; 
             
            List<Bid__c > lstBid = [Select id from Bid__c];
            ApexPages.StandardSetController controller =  new ApexPages.StandardSetController(lstBid);
            controller.setPageSize(100);
            controller.setSelected(lstBidS);
            BidRespondedQueueController bidCtlr = new BidRespondedQueueController(controller);
            bidCtlr.updateStatus();
            List<Bid__c > listBid = [Select id , Bid_Status__c from Bid__c where id =:lstBidS[0].id];
            System.debug('<<>>'+listBid);
            system.assertequals(listBid[0].Bid_Status__c,'Responded');

             
      }
}
public with sharing class CreateAttributeWizController {
    public string fieldType {get; set;}
    
    public MetadataService.CustomField customField;
    public List<MetadataService.Profile> updateProfiles = new List<MetadataService.Profile>();
    public boolean showSelectType {get; set;}
    public boolean showProfilePermission {get; set;}
    public boolean showAttributeDetail {get; set;}
    public boolean showRecordType {get; set;}
    public boolean showConfirmPage {get; set;}
    
    //Default Object
    string defaultObject = 'Specification__c';
    
    //Attributes Detail Field
    public string fieldName {get; set;}
    public boolean fieldNameError {get; set;}
    public void setFieldNameError() {
        fieldNameError = true;
    }
    public integer fieldLength {get; set;}
    public boolean fieldLengthError {get; set;}
    public void setFieldLengthError() {
        fieldLengthError = true;
    }
    public integer fieldPrecision {get; set;}
    public boolean fieldPrecisionError {get; set;}
    public void setFieldPrecisionError() {
        fieldPrecisionError = true;
    }
    public integer fieldscale {get; set;}
    public boolean fieldscaleError {get; set;}
    public void setFieldscaleError() {
        fieldscaleError = true;
    }
    public integer fieldstartingNumber {get; set;}
    public boolean fieldstartingNumberError {get; set;}
    public void setFieldstartingNumberError() {
        fieldstartingNumberError = true;
    }
    public string fieldCheckboxDefaultValue {get; set;}
    public boolean fieldCheckboxDefaultValueError {get; set;}
    public void setFieldCheckBoxDefaultValueError() {
        fieldCheckboxDefaultValueError = true;
    }
    public integer fieldVisibleLines {get; set;}
    
    public string fieldPicklistValues {get; set;}
    public boolean fieldPicklistValuesError {get; set;}
    public void setFieldPicklistValuesError() {
        fieldPicklistValuesError = true;
    }
    public boolean fieldPicklistSorted {get; set;}
    public boolean fieldPicklistSortedError {get; set;}
    public void setfieldPicklistSortedError() {
        fieldPicklistSortedError = true;
    }
    public string fieldDisplayFormat {get; set;}
    public boolean fieldDisplayFormatError {get; set;}
    public void setfieldDisplayFormatError() {
        fieldDisplayFormatError = true;
    }
    //List Select Checkbox DefaultValue
    public List<SelectOption> getcheckboxDefaultValue() {
        List<SelectOption> typeOption = new List<SelectOption>();
        typeOption.add(new SelectOption('True', 'Checked'));
        typeOption.add(new SelectOption('False', 'Unchecked'));
        return typeOption;
    }
    
    
        
    //Profile Permission Detail Field
    public String[] profiles = new String[]{};
        
    public String[] getProfiles() {
        return profiles;
    }
    public void setProfiles(String[] profiles) {
        this.profiles = profiles;
    }
    
    //Exsiting Record Type Detail Field
    public String[] existingRecordTypes = new String[]{};
    
    public String[] getExistingRecordTypes() {
        return existingRecordTypes;
    }
    public void setExistingRecordTypes(String[] existingRecordTypes) {
        this.existingRecordTypes = existingRecordTypes;
    }

    //New Record Type Detail Field
    public string newRecordTypeName {get; set;}
    public string newRecordTypeDescription {get; set;}
    
    public CreateAttributeWizController() {
        stageSelector('showSelectType');
    }
    
    public void stageSelector (String stage) {
        showSelectType = false;
        showAttributeDetail = false;
        showProfilePermission = false;
        showRecordType = false;
        showConfirmPage = false;
        if(stage.equals('showSelectType')){
            showSelectType = true;
        }
        else if(stage.equals('showAttributeDetail')){
            showAttributeDetail = true;
        }
        else if(stage.equals('showProfilePermission')){
            showProfilePermission = true;
        }
        else if(stage.equals('showRecordType')){
            showRecordType = true;
        }
    }
    
    //List Select Type Options
    public List<SelectOption> gettypeOption() {
        List<SelectOption> typeOption = new List<SelectOption>();
        typeOption.add(new SelectOption('AutoNumber', 'Auto Number'));
        typeOption.add(new SelectOption('Checkbox', 'Checkbox'));
        typeOption.add(new SelectOption('Currency', 'Currency'));
        typeOption.add(new SelectOption('Date', 'Date'));
        typeOption.add(new SelectOption('Number', 'Number'));
        typeOption.add(new SelectOption('Percent', 'Percent'));
        typeOption.add(new SelectOption('Picklist', 'Picklist'));
        typeOption.add(new SelectOption('MultiselectPicklist', 'Picklist (Multi-Select)'));
        typeOption.add(new SelectOption('Text', 'Text'));
        typeOption.add(new SelectOption('TextArea', 'Text Area'));
        typeOption.add(new SelectOption('LongTextArea', 'Text Area (Long)'));
        return typeOption;
    }
    
    //From Select Type to Type Detail Page
    public pagereference goToAttributeDetail() {
        System.debug('backTo ' + fieldType);
        
        //force to select a type then enable NEXT
        if(fieldType != null) {
            stageSelector('showAttributeDetail');
        }
        return null;
    }
    
    //From Any Where to Select Type Page
    public pagereference backToSelectType() {
        stageSelector('showSelectType');
        resetErrorMessage();
        return null;
    }
    
    public void resetErrorMessage() {
        fieldNameError = false;
        fieldLengthError = false;
        fieldPrecisionError = false;
        fieldscaleError = false;
        fieldstartingNumberError = false;
        fieldCheckboxDefaultValueError = false;
        fieldPicklistValuesError = false;
        fieldDisplayFormatError = false;
    }
    
    //From Type Detail to Profile Permission Page
    public pageReference goToProfilePermissions() {
        resetErrorMessage();
        if(fieldType == 'Text'){
            if(fieldName != '' && fieldLength != null) {
                customField = CreateFieldService.createTextField(fieldType, defaultObject, fieldName, fieldLength);    
            }
            //CreateFieldService.saveField(textField);
        }
        else if(fieldType == 'Number'){
            if(fieldName != '' && fieldscale != null && fieldPrecision != null){
                customField = CreateFieldService.createNumberField(fieldType, defaultObject, fieldName, fieldPrecision, fieldscale);
            }
        }
        else if(fieldType == 'AutoNumber'){
            customField = CreateFieldService.createAutoNumberField(fieldType, defaultObject, fieldName, fieldstartingNumber, fieldDisplayFormat);
        }
        else if(fieldType == 'Checkbox'){
            if(fieldCheckboxDefaultValue != null){
                customField = CreateFieldService.createCheckboxField(fieldType, defaultObject, fieldName, fieldCheckboxDefaultValue);
            }
            else{
                customField = CreateFieldService.createCheckboxField(fieldType, defaultObject, fieldName, 'False');
            }
            
        }
        else if(fieldType == 'Currency'){
            customField = CreateFieldService.createNumberField(fieldType, defaultObject, fieldName, fieldPrecision, fieldscale);
        }
        else if(fieldType == 'Date'){
            customField = CreateFieldService.createDateField(fieldType, defaultObject, fieldName);
        }
        else if(fieldType == 'Percent'){
            customField = CreateFieldService.createNumberField(fieldType, defaultObject, fieldName, fieldPrecision, fieldscale);
        }
        else if(fieldType == 'Picklist'){
            customField = CreateFieldService.createPicklistField(fieldType, defaultObject, fieldName, fieldPicklistValues, fieldPicklistSorted, null);
        }
        else if(fieldType == 'MultiselectPicklist'){
            customField = CreateFieldService.createPicklistField(fieldType, defaultObject, fieldName, fieldPicklistValues, fieldPicklistSorted, fieldLength);
        }
        else if(fieldType == 'TextArea'){
            customField = CreateFieldService.createDateField(fieldType, defaultObject, fieldName);
        }
        else if(fieldType == 'LongTextArea'){
            customField = CreateFieldService.createLongTextAreaField(fieldType, defaultObject, fieldName, fieldLength, fieldVisibleLines);
        }
        
        stageSelector('showProfilePermission');
        return null;
    }
    
    //List Select Profile Permission Options
    public List<SelectOption> getprofileList() {
        List<SelectOption> profileList = new List<SelectOption>();
        List<Profile> profilesList = [Select Id, Name
                                  From Profile
                                  Where Name like '%Specright -%'];
        for(Profile profile : profilesList) {
            profileList.add(new SelectOption(profile.Name, profile.Name));
        }
        return profileList;
    }
    
    public pageReference goToSelectRecordType() {
        for(String profileName : profiles) {
            //System.debug('Profile ID: ' + profileName);
            if(profileName != ''){
                MetadataService.Profile eachProfile = 
                CreateFieldService.updateFieldLevelSecurity(profileName, true, customField);
                if(eachProfile != null) {
                    updateProfiles.add(eachProfile);
                }
            }
            
            //CreateFieldService.saveField(customField);
            //CreateFieldService.saveFieldLevelSecurity(eachProfile);
        }
        /*System.debug('test');
        Pagereference pg=new pagereference('http://www.google.com/');
        pg.setRedirect(true);
        return pg;*/
        
        stageSelector('showRecordType');
        return null;
    }
    
    public List<SelectOption> getexistingRecordTypeList() {
        List<SelectOption> existingRecordTypeList = new List<SelectOption>();
        List<RecordType> recordTypes = [Select r.SobjectType, r.Id, r.Name
                                        From RecordType r 
                                        Where SobjectType =: defaultObject];
        for(RecordType eachRecordType : recordTypes) {
            existingRecordTypeList.add(new SelectOption(eachRecordType.Name, eachRecordType.Name));
        }
        return existingRecordTypeList;
    }
    
    public pageReference goToConfirmation() {
        
        //Create Custom Fields
        CreateFieldService.saveField(customField);
        
        //Update Profile Level Security to view that Fields
        for(MetadataService.Profile profile : updateProfiles){
            CreateFieldService.saveFieldLevelSecurity(profile);
        }
        
        //Base on RecordType Selected Add field to Layout
        for(String eachRecordType : existingRecordTypes){
            String layoutName = eachRecordType + ' Layout';
            MetadataService.Layout layout = 
                CreateFieldService.addFieldToLayout(defaultObject, layoutName, customField);
            CreateFieldService.saveAddFieldToLayout(layout);
        }
        
        //Create new RecordType and Page Layout
        if(newRecordTypeName != '' && newRecordTypeDescription != ''){
            MetadataService.RecordType newRecordType = 
                CreateFieldService.createNewRecordType(defaultObject, newRecordTypeName, newRecordTypeDescription);
            CreateFieldService.saveNewRecordType(newRecordType);
            
            //Create Page Layout
            MetadataService.Layout newLayout =
                CreateFieldService.createLayout(defaultObject, newRecordTypeName, customField);
            CreateFieldService.saveCreateLayout(newLayout);
            
            //Assign new RecordType and New Layout to Profile
            for(String eachProfile : profiles){
                MetadataService.Profile updateProfile =
                    CreateFieldService.updateRecordTypePermission(eachProfile, newRecordType, newRecordTypeName);
                
                CreateFieldService.saveFieldLevelSecurity(updateProfile);
            }
            
        }
        
        //stageSelector('showConfirmPage');
        Pagereference pg=new pagereference('/apex/CreateAttributeWiz');
        pg.setRedirect(true);
        return pg;
    }
    
    public pageReference backToSelectProfilePermission() {
        //Clean Profile Permission Cache
        updateProfiles.clear();
        stageSelector('showProfilePermission');
        
        return null;
    }
    
    public pageReference cancel() {
        Schema.DescribeSObjectResult dsr = Specification__c.SObjectType.getDescribe();
        Pagereference pg=new pagereference('/' + dsr.getKeyPrefix());
        pg.setRedirect(true);
        return pg;
    }
    
}
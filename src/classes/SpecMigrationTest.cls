@isTest
private class SpecMigrationTest {
	static testmethod void testBatch() {
		Test.startTest();
		Specification__c spec = new Specification__c();
		insert spec;
		
		database.executeBatch(new specright.SpecMigration(true));
		Test.stopTest();

		// assert that there is atleast one inbound material record created for the related spec
		System.assert([Select id from Inbound_Material__c where specification__c=:spec.id].size() > 0);

	}
}
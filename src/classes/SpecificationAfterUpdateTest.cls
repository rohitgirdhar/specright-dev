@isTest
public with sharing class SpecificationAfterUpdateTest {

    public static testMethod void validateSpecifiactionAfterUpdate() {
        /*List < RecordType > racctypes = [Select Name, Id From RecordType where sObjectType = 'Account'];
        Map < String, String > AccRecordTypes = new Map < String, String > {};
        for (RecordType rt: racctypes)
        {   
            AccRecordTypes.put(rt.Name, rt.Id);
        }
        //Commented to delete account record type
        */
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        //objAccount.RecordTypeId = AccRecordTypes.get('Supplier');
        insert objAccount;

        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User user = new User(Alias = 'TDemo', Email = 'test@gmail.com',
            EmailEncodingKey = 'ISO-8859-1', FirstName = 'Demo',
            LanguageLocaleKey = 'en_US', LastName = 'User', LocaleSidKey = 'en_US',
            ProfileId = prof.Id, TimeZoneSidKey = 'America/Denver',
            Username = 'demo.test.user@gmail.com');
        insert user;

        List < RecordType > rtypes = [Select Name, Id From RecordType where sObjectType = 'specright__Specification__c'];
        Map < String, String > SpecRecordTypes = new Map < String, String > {};
        for (RecordType rt: rtypes)
            SpecRecordTypes.put(rt.Name, rt.Id);
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test Specification';
        objSpecification.RecordTypeId = SpecRecordTypes.get('Corrugated');
        insert objSpecification;

        Bid_Request__c objBidRequest = new Bid_Request__c();
        objBidRequest.Bid_Status__c = 'In Process';
        objBidRequest.Specification__c = objSpecification.Id;
        insert objBidRequest;

        Bid__c objBid = new Bid__c();
        objBid.Bid_Request__c = objBidRequest.Id;
        objBid.Specification__c = objSpecification.Id;
        objBid.Supplier__c = objAccount.Id;
        insert objBid;

        Contact objContact = new Contact();
        objContact.LastName = 'Test LastName';
        objContact.AccountId = objAccount.Id;
        insert objContact;
        system.assertEquals('Test LastName', objContact.LastName);
        //Create Portal USer
        Set < String > customerUserTypes = new Set < String > {
            'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'
        };
        Profile p = [select Id, name from Profile where UserType in : customerUserTypes limit 1];

        User newUser = new User(
            profileId = p.id,
            username = 'julie.andrews123@yahoo.com',
            email = 'ja@ff.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias = 'nuser',
            lastname = 'lastname',
            contactId = objContact.id
        );
        insert newUser;

        objSpecification.OwnerId = user.id;
        update objSpecification;


    }

}
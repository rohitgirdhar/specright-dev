@isTest(SeeAllData=true)
public with sharing class BidEditControllerTest {

    public static testMethod void validateBidEditController(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.BillingState = 'Test State';
        objAccount.Phone = '123456';
        system.assertEquals('Test Account',objAccount.Name);  
        insert objAccount;
        
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='specright__Specification__c']; 
        Map<String,String> SpecRecordTypes = new Map<String,String>{};
            for(RecordType rt: rtypes)
            SpecRecordTypes.put(rt.Name,rt.Id);
        
        BidGroup__c BidGroup = new BidGroup__c();
        BidGroup.Access_Duration__c = 5;
        BidGroup.Access_End__c = system.today()+1;
        insert BidGroup;
        
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test internalPart';
        objSpecification.Prior_Part_Numbers__c = 'Test priorPart';
        objSpecification.RecordTypeId = SpecRecordTypes.get('Corrugated');
        insert objSpecification; 
        
        List<Bid_Request__c> lstBidReq = new List<Bid_Request__c>();
        Bid_Request__c objBidRequest = new Bid_Request__c();
        objBidRequest.Bid_Status__c = 'In Process';
        objBidRequest.Specification__c = objSpecification.Id;
        objBidRequest.BidGroup__c = BidGroup.Id;
        lstBidReq.add(objBidRequest);
        insert lstBidReq;
        system.debug('<<>>'+lstBidReq.size());
        
        List<Spec_Event__c> lstSpec = new List<Spec_Event__c>();
        Spec_Event__c SpecEvent = new Spec_Event__c();
        SpecEvent.Specification__c = objSpecification.id;
        SpecEvent.Event_Type__c='Bid Request';
        SpecEvent.User__c=UserInfo.getUserID();
        lstSpec.add(SpecEvent);
        insert lstSpec;
        system.debug('<<>>'+lstSpec.size());
        
        Bid__c objBidc = new Bid__c();
        objBidc.Bid_Request__c = objBidRequest.Id;
        objBidc.Bid_Status__c = 'In Process';
        objBidc.Specification__c = objSpecification.Id;
        objBidc.Supplier__c = objAccount.Id;
        insert objBidc;
        
        BidEditController objBidEditCls = new BidEditController(new ApexPages.StandardController(objBidc));
        objBidEditCls.selectedSpecification.add(new BidEditController.SpecSelection(objSpecification));
        objBidEditCls.selectedVendor.add(new BidEditController.VendorSelection(objAccount));
        objBidEditCls.csvFileBody = blob.valueOf('internal Part\r\nTest internalPart\r\nTest New');
        
        //objBidEditCls.selectedSpecification.get(0).selected = false;
        //objBidEditCls.selectedSpecification.get(0).selected = true;
        //objBidEditCls.NonSelectedVendor.get(0).selected = true;
        objBidEditCls.csvAsString = '';
        objBidEditCls.selectedVendor.get(0).selected = true;
        objBidEditCls.internalPart = 'Test';
        objBidEditCls.priorPart = 'Test';
        objBidEditCls.specRight = 'test';
        objBidEditCls.productFamily = 'Test';
        objBidEditCls.vendorName = 'Test';
        objBidEditCls.specName = 'Test';
        objBidEditCls.otherinterval = String.ValueOf(5);
        objBidEditCls.duration = 'other';
        objBidEditCls.importCSVFile();
        objBidEditCls.searchSpecification();
        objBidEditCls.searchVendor();
        objBidEditCls.goToStep2();
        objBidEditCls.goToStep4();
        objBidEditCls.confirm();
        objBidEditCls.finishProcess();
        objBidEditCls.finishAndNew();
        objBidEditCls.addSelection();
        objBidEditCls.objBid = objBidc.clone(true);
        
        //objBidEditCls.selectedSpecification.add(new BidEditController.SpecSelection(objSpecification));
        objBidEditCls.goToStep3();
        objBidEditCls.goToStep5();
        objBidEditCls.goToStep6();
        objBidEditCls.addVendor();
        objBidEditCls.removeVendor();
        objBidEditCls.clearSpecFilter();
        objBidEditCls.clearVendFilter();
        objBidEditCls.filterVend=false;
        objBidEditCls.finishProcess();
        objBidEditCls.finishAndNew();
        objBidEditCls.clearBlob();
        objBidEditCls.removeSelection();
        //objBidEditCls.enableFirstTab = true;
        objBidEditCls.dummy();
        
        BidEditController.SpecSelection ss = new BidEditController.SpecSelection(objSpecification );
        ss.selected = True;
        ss.objSpec = objSpecification ;
        
        BidEditController.VendorSelection vs = new BidEditController.VendorSelection(objAccount);
        vs.selected = True;
        vs.objVendor = objAccount;
        
        objBidEditCls.selectedSpecification.add(ss);
        ApexPages.currentPage().getParameters().put('RecordType','BidEdit');
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objBidc);
        BidEditController obj = new BidEditController (sc);
        obj.confirm();
        obj.removeSelection();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());
        
        // Check that the error message you are expecting is in pageMessages
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            messageFound = true;        
            
        }
        
    }

      public static testMethod void validateBidduration(){  

        Bid__c objBid = new Bid__c();
        objBid.Bid_Status__c = 'In Process';
        insert objBid;

        BidEditController objBidEditCls1 = new BidEditController(new ApexPages.StandardController(objBid));

        objBidEditCls1.csvFileBody = blob.valueOf('');
        objBidEditCls1.csvAsString = '';
        objBidEditCls1.importCSVFile();
        objBidEditCls1.goToStep5();
        objBidEditCls1.selectedSpecification = null;
        objBidEditCls1.vendorName = '';
        objBidEditCls1.duration = null;
        objBidEditCls1.isAccessible = false;
        //objBidEditCls1.evalTab();
        objBidEditCls1.goToStep2();
        objBidEditCls1.goToStep6();
        objBidEditCls1.removeVendor();
        List<Bid__c> lstbid = [Select Id,Bid_Status__c,Access_Duration__c from Bid__c where id =: objBid.Id];
        System.debug('<<>>'+lstbid);
        System.assertEquals(lstbid[0].Access_Duration__c,null);
      }  
      
      public static testMethod void validateBidotherinterval(){ 
              
        List<Bid__c> lstbid = new List<Bid__c>();
        Bid__c objBid = new Bid__c();
        objBid.Bid_Status__c = 'In Process';
        lstbid.add(objBid);
        insert lstbid;

        BidEditController objBidEditCls2 = new BidEditController(new ApexPages.StandardController(objBid));
        objBidEditCls2.otherinterval = null;
        objBidEditCls2.confirm();
        objBidEditCls2.duration = 'other';
        objBidEditCls2.csvFileBody = null;
        objBidEditCls2.csvAsString = '';
        objBidEditCls2.importCSVFile();
        objBidEditCls2.goToStep2();
        objBidEditCls2.goToStep6();
        List<Bid__c> lstbids = [Select id from Bid__c where id = : lstbid[0].Id];
        System.assert(lstbid.size()>0);
        System.assert(ApexPages.getMessages().size() > 0);        
      }
    
    public static testMethod void validateSpecReportUpload(){
        List<Bid__c> lstbid = new List<Bid__c>();
        Bid__c objBid = new Bid__c();
        objBid.Bid_Status__c = 'In Process';
        lstbid.add(objBid);
        insert lstbid;
        
        BidEditController objBidEditCls3 = new BidEditController(new ApexPages.StandardController(objBid));
        //Did not select a report
        objBidEditCls3.importReport();
        
        //Report List w/o Search
        List<SelectOption> reportOption = objBidEditCls3.getselectedReportOption();
        System.assertNotEquals(0, reportOption.size());
        
        //Report List w/ Search
        objBidEditCls3.searchReport = 'No !@#$% Report';
        reportOption = objBidEditCls3.getselectedReportOption();
        System.assertEquals(1, reportOption.size());
        
        objBidEditCls3.searchReport = 'report';
        reportOption = objBidEditCls3.getselectedReportOption();
        System.assertNotEquals(0, reportOption.size());
        
        //Selected a Report with SpecID
        List<Report> reportList = [Select Id, DeveloperName
                                   From Report
                                   Where DeveloperName = 'SimpleExchangeReport'];
        objBidEditCls3.selectedReport = (String)reportList.get(0).get('Id');
        objBidEditCls3.importReport();
        
        //Selected a Report with No SpecID
        reportList = [Select Id, DeveloperName
                      From Report
                      Where DeveloperName = 'SimpleExchangeReportwithNoId'];
        objBidEditCls3.selectedReport = (String)reportList.get(0).get('Id');
        objBidEditCls3.importReport();
    } 
  }
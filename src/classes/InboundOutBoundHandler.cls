public with sharing class InboundOutBoundHandler {
    //Handle Inbound Material Entries and Specification
 
     public static void handleInboundRT(list<Inbound_Material__c> lstIBM){
            //Get Specification Id from Inbound Material
            Set<Id> setSpecId = new Set<Id>();
            Set<Id> setRTSpecs = new Set<Id>();
            Map<Id,Id> mapInboundIdRecId = new Map<Id,Id>();
            for(Inbound_Material__c sIBM :lstIBM){
                setSpecId.add(sIBM.Specification__c);
                mapInboundIdRecId.put(sIBM.Id, sIBM.RecordTypeId);
            }
            if(!mapInboundIdRecId.isEmpty()) {
                //Map Record Type Id To each Record Of Inbound material
                Map<id,RecordType> mapRTInbounds = new Map<id,RecordType>(
                                    [Select id,Name from RecordType where Id IN :mapInboundIdRecId.values()]);
                Map<Id,Id> mapspecIdRecId = new Map<Id,Id>();

                list<Specification__c> lstSpecification =[select Id, RecordTypeId From Specification__c
                                                                        where Id IN :setSpecId];
                for(Specification__c Sp :lstSpecification){
                    if(Sp.RecordTypeId!=null){
                        setRTSpecs.add(Sp.RecordTypeId);
                        mapspecIdRecId.put(Sp.Id, Sp.RecordTypeId);
                    }
                }                                                 
                
                //Map RecordType Id to specific specifiaction Id
                Map<id,RecordType> mapRecordTypeOfSpeci = new Map<id,RecordType>([Select id,Name from RecordType where Id IN :setRTSpecs]);

                for(Inbound_Material__c IBM :lstIBM){
                    if(IBM.RecordTypeId == null) return;//Inbound Material with null record type do nothing
                    if(mapspecIdRecId.get(IBM.Specification__c)==null) return;//If Specification with null record type do nothing
                    Id specrecid = mapspecIdRecId.get(IBM.Specification__c);
                    Id inrecid = mapInboundIdRecId.get(IBM.Id);
                    if(specrecid!=null && inrecid!=null && (mapRTInbounds.get(inrecid).Name != mapRecordTypeOfSpeci.get(specrecid).Name)){
                        IBM.addError('Error:Alignment of Record type should be same as parent i.e. '+mapRecordTypeOfSpeci.get(specrecid).Name);         
                  }   
                }
            }
                
     }
     public static void handleOutboundRT(list<Outbound_Finished_Product__c> lstOFP){
            //Get ParentId of outbound Finished Product
            Set<id> setSpecId = new Set<id>();
            Set<id> setRTSpecs = new Set<Id>();
            Map<Id,Id> mapOutboundIdRecId = new Map<Id,Id>();
            for(Outbound_Finished_Product__c sofp :lstOFP){
                setSpecId.add(sofp.SpecificationL__c);
                mapOutboundIdRecId.put(sofp.Id, sofp.RecordTypeId);
            }
            if(!mapOutboundIdRecId.isEmpty()) {
                //Map Record Type Id To each Record Of OutBound Finished product
                map<id,RecordType> mapRTOutbounds = new map<id,RecordType>(
                                    [Select id,Name from RecordType where Id IN :mapOutboundIdRecId.values()]);
                
                Map<Id,Id> mapspecIdRecId = new Map<Id,Id>();
                if(setSpecId.size() > 0) {
                    list<Specification__c> lstSpecification =[select Id, RecordTypeId From Specification__c
                                                                            where Id IN :setSpecId];
                    for(Specification__c Sp :lstSpecification){
                        if(Sp.RecordTypeId!=null){
                            setRTSpecs.add(Sp.RecordTypeId);
                            mapspecIdRecId.put(Sp.Id, Sp.RecordTypeId);
                        }
                    }
                    //Map RecordType Id to specific specifiaction Id                                                 
                    map<id,RecordType> mapRecordTypeOfSpeci = new map<id,RecordType>([Select id,Name from RecordType where Id IN :setRTSpecs]);
                    for(Outbound_Finished_Product__c ofp :lstOFP){
                        if(ofp.RecordTypeId == null) return;////O F P with null record type do nothing
                        if(mapspecIdRecId.get(ofp.SpecificationL__c)==null) return;//If Specification with null record type do nothing
                        Id specrecid = mapspecIdRecId.get(ofp.SpecificationL__c);
                        Id inrecid = mapOutboundIdRecId.get(ofp.Id);
                        if(mapRTOutbounds.get(inrecid).Name != mapRecordTypeOfSpeci.get(specrecid).Name){
                            ofp.addError('Error:Alignment of Record type should be same as parent i.e. '+mapRecordTypeOfSpeci.get(specrecid).Name);         
                        }     
                    }
                }   
            }
     }
}
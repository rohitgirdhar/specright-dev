public with sharing class DocumentAutoSelectRecordTypeController {
    private SObjectType specObject = Specification__c.SObjectType;
    private SObjectType docSobType = Document__c.SObjectType;
    //private Document__c 
    public DocumentAutoSelectRecordTypeController(ApexPages.StandardController std){
		     
    }
	public PageReference newRecord() {
        PageReference p = new PageReference('/' + docSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        Id parentRecordID = getParentSObjectId(m);
        if(isParentSpec(parentRecordID)){
            //From Spec
            m.put('RecordType', getDocRecordTypeId('Specification Doc'));
        }
        else{
            m.put('RecordType', getDocRecordTypeId('Account Doc'));
        }
        m.put('nooverride', '1');
        return p;
    }
    
    private Id getDocRecordTypeId(String recordTypeName) {
        return Schema.SObjectType.Document__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    private Id getParentSObjectId(Map<String, String> m) {
        for (String key : m.keySet()) {
            if (key.endsWith('_lkid')) {
                return m.get(key);
            }
        }
        return null;
    }
    
    private Boolean isParentSpec(Id parentId){
        List<Specification__c> specList = [Select Id
                                           From Specification__c
                                           Where id =: parentId];
        return !specList.isEmpty();
    }
}
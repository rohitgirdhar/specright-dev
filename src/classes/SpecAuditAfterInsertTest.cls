@isTest
public with sharing class SpecAuditAfterInsertTest {
    public static Testmethod void validateTrigger()
    {
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='specright__Specification__c']; 
        Map<String,String> SpecRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        {   
            SpecRecordTypes.put(rt.Name,rt.Id);
        }
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test Specification';
        objSpecification.RecordTypeId = SpecRecordTypes.get('Corrugated');
        insert objSpecification;
        system.assertEquals('Test Specification',objSpecification.Name);  

        Spec_Audit__c objAudit = new Spec_Audit__c();
        objAudit.Specification__c = objSpecification.id;
        insert objAudit;
        
    }
        

}
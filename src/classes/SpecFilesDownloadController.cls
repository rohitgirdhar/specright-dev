global with sharing class SpecFilesDownloadController {
    public List<Attachment> atts{get;set;}
    public Map<Id,Spec_File__c> specFiles{get;set;}
    public Specification__c spec{get;set;}
    public string fileIds{get;set;}
    public string specId{get;set;}
    public SpecFilesDownloadController(){
        System.debug('TEST In SpecFileDownload Controller');
        fileIds='';
        specId=ApexPages.currentPage().getParameters().get('specId');
        
        //CRUD & FLS for Specification
        Boolean isSpecificationAccessible=Schema.sObjectType.Specification__c.isAccessible() && Schema.sObjectType.Specification__c.fields.Name.isAccessible();
        if (!isSpecificationAccessible) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        
        spec = [select id,name from Specification__c where id=:specId];
        
        //CRUD & FLS
        Boolean isSpecFileAccessible=Schema.sObjectType.Spec_File__c.isAccessible() && Schema.sObjectType.Spec_File__c.fields.File_Type__c.isAccessible();
        if (!isSpecFileAccessible) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        specFiles = new Map<Id,Spec_File__c>([select id, File_Type__c, Specification__r.name from Spec_File__c where Specification__c=:ApexPages.currentPage().getParameters().get('specId')]);
        
        
        //CRUD & FLS
        Boolean isAttachmentAccessible=Schema.sObjectType.attachment.isAccessible() && 
                                       Schema.sObjectType.attachment.fields.parentId.isAccessible() && 
                                       Schema.sObjectType.attachment.fields.Name.isAccessible();
        if (!isAttachmentAccessible) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        for(Attachment att:[select id, parentId, Name from attachment where parentId in:specFiles.keySet()]){
            if(fileIds!='')
                fileIds+=';'+att.id+'_'+specFiles.get(att.ParentId).File_Type__c;
            else
               fileIds+=att.id+'_'+specFiles.get(att.ParentId).File_Type__c;
        }
    }
    @RemoteAction
    global static AttachmentWrapper getAttachment(String attId) {
         
        //CRUD & FLS
        Boolean isAttachmentAccessible=Schema.sObjectType.attachment.isAccessible() && 
                                       Schema.sObjectType.attachment.fields.parentId.isAccessible() && 
                                       Schema.sObjectType.attachment.fields.Name.isAccessible() &&
                                       Schema.sObjectType.attachment.fields.ContentType.isAccessible() &&
                                       Schema.sObjectType.attachment.fields.Body.isAccessible();
        if (!isAttachmentAccessible) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        Attachment att = [select Id,ParentId, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
                          
        
        //CRUD & FLS for SpecFile
        Boolean isSpecFileAccessible2=Schema.sObjectType.Spec_File__c.isAccessible() && Schema.sObjectType.Spec_File__c.fields.File_Type__c.isAccessible();
        if (!isSpecFileAccessible2) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Read Access.'));
        }
        Spec_File__c specFile = [select id, File_Type__c from Spec_File__c where id=:att.ParentId];
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attWrapper.attName = att.Name;
        attWrapper.folder = specFile.File_Type__c;
                           
        return attWrapper;
    }
    global class AttachmentWrapper {
        public String folder{get;set;}
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
    global void redirect(){
        List<Spec_Event__c> updateSpecEventList = new List<Spec_Event__c>();
        List<Spec_File__c> specFileList = [Select Id, Specification__c
                                                From Spec_File__c
                                                Where Specification__c =: specId];
        for(Spec_File__c specFile : specFileList){
            Spec_Event__c objSpecEvent = new Spec_Event__c();
            objSpecEvent.Specification__c=specId;
            objSpecEvent.Spec_File__c = specFile.Id;
            objSpecEvent.Event_Type__c='File Download (Exchange)';
            objSpecEvent.User__c=UserInfo.getUserID();
            
            Boolean isSpecEventCreateable= Schema.sObjectType.Spec_Event__c .isCreateable() && 
                Schema.sObjectType.Spec_Event__c.fields.Specification__c.isCreateable() && 
                Schema.sObjectType.Spec_Event__c.fields.Event_Type__c.isCreateable() && 
                Schema.sObjectType.Spec_Event__c.fields.User__c.isCreateable();
            if(!isSpecEventCreateable){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
                return;
            }
            updateSpecEventList.add(objSpecEvent);
        }
        if(updateSpecEventList.size() > 0){
            if(Schema.sObjectType.Spec_Event__c.isCreateable()){
                insert updateSpecEventList;
            }
        }
        //insert objSpecEvent;
        //return new Pagereference('/a02?fcf=00Bo0000001wVWU');
    }
}
@isTest
public with sharing class SpecFileAttachmentClsTest {

    public static testMethod void validateSpecFileAttachmentCls() {

        List < RecordType > rtypes = [Select Name, Id From RecordType where sObjectType = 'specright__Specification__c'];
        Map < String, String > SpecRecordTypes = new Map < String, String > {};
        for (RecordType rt: rtypes)
            {
                SpecRecordTypes.put(rt.Name, rt.Id);
            }
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test Specification';
        objSpecification.RecordTypeId = SpecRecordTypes.get('Corrugated');
        insert objSpecification;
        system.assertEquals('Test Specification', objSpecification.Name);
        Spec_File__c objSpecFiles = new Spec_File__c();
        objSpecFiles.Specification__c = objSpecification.Id;
        objSpecFiles.File_Type__c = 'Level 1';
        //insert objSpecFiles;        
        
        ApexPages.currentPage().getParameters().put('_lkid',objSpecification.Id);
        SpecFileAttachmentController objCls = new SpecFileAttachmentController(new ApexPages.StandardController(objSpecFiles));
        objCls.isNewRecord = True;
        objCls.objSpecFile.Spec_File_Name__c='Test';
        objCls.myAttachment.Body = Blob.valueOf('Test');
        objCls.myAttachment.Name = 'Test';      
        objCls.UploadFile();
        
        Spec_File__c objSpecFile1 = new Spec_File__c();
        objSpecFile1.Specification__c = objSpecification.Id;
        objSpecFile1.File_Type__c = 'Level 1';
        insert objSpecFile1;    
                            
        SpecFileAttachmentController objCls1 = new SpecFileAttachmentController(new ApexPages.StandardController(objSpecFile1));
        
        objCls1.isNewRecord = False;
        objCls1.UploadFile();
        
        objCls1.isNewRecord = False;
        objCls1.objSpecFile.Spec_File_Name__c='Test';
        objCls1.UploadFile();                
        
        objCls1.isNewRecord = False;
        objCls1.myAttachment.Body = Blob.valueOf('Test');
        objCls1.myAttachment.Name = 'Test';
        objCls1.objSpecFile.Spec_File_Name__c='Test';
        objCls1.UploadFile();
                
        Attachment myAttachment= new Attachment(ParentId = objCls1.objSpecFile.id);
        //attach.contentType = 'text/plain';
        myAttachment.Body = Blob.valueOf('attachment');
        myAttachment.Name = 'Test';
        //myAttachment.ParentId = objCls1.objSpecFile.id;
        insert myAttachment;
        objCls1.UploadFile();               
        objCls1.cancel();
    }
    
    public static testMethod void specFamilyFileAttachmentTest(){
        List < RecordType > rtypes = [Select Name, Id From RecordType where sObjectType = 'specright__Specification__c'];
        Map < String, String > SpecRecordTypes = new Map < String, String > {};
        for (RecordType rt: rtypes)
            {
                SpecRecordTypes.put(rt.Name, rt.Id);
            }
        Spec_Family__c objSpecFamily = new Spec_Family__c();
        objSpecFamily.Name = 'Test Spec Family';
        insert objSpecFamily;
        
        Spec_File__c objSpecFiles = new Spec_File__c();
        objSpecFiles.Spec_Family__c = objSpecFamily.Id;
        //objSpecFiles.File_Type__c = 'Level 1';
        insert objSpecFiles;        
        
        ApexPages.currentPage().getParameters().put('_lkid',objSpecFamily.Id);
        SpecFileAttachmentController objCls = new SpecFileAttachmentController(new ApexPages.StandardController(objSpecFiles));
        objCls.isNewRecord = False;
        objCls.objSpecFile.Spec_File_Name__c='Test';
        objCls.myAttachment.Body = Blob.valueOf('Test');
        objCls.myAttachment.Name = 'Test';      
        objCls.UploadFile();
        objCls.Cancel();
        System.assert(true);
    }


}
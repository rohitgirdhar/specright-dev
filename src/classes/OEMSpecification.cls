@RestResource(urlMapping='/webhook')
global class OEMSpecification{
    
    
    @HttpPost
    global static string doPost(specright__Specification__c spec, list<specright__Spec_File__c> lstsFiles, map<String, String> parentChild) {
        system.debug('<<>>specdata : ' + spec);
        system.debug('<<>>lstsFiles : ' + lstsFiles);
        system.debug('<<>>parentChild : ' + parentChild);
        upsert spec specright__HQspecId__c;
        list<specright__Spec_File__c> specFiles=new list<specright__Spec_File__c>();
        for(specright__Spec_File__c specF:lstsFiles){
            specF.specright__HQspecFileId__c=parentChild.get(spec.specright__HQspecId__c);
            specF.specright__Specification__c=spec.id;
            specFiles.add(specF);
            system.debug('<<>>specAlldata : ' + specF);
        }
        if(specFiles.size() >0)
        upsert specFiles specright__HQspecFileId__c;
        return null;
    } 
}
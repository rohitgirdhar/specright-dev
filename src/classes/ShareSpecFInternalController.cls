public with sharing class ShareSpecFInternalController {
    public Spec_Family__c specRecord {get; set;}
    public Task taskNew {get; set;}
    public ShareSpecFInternalController(ApexPages.StandardController std){
        this.specRecord = (Spec_Family__c)std.getRecord();
        specRecord = [Select id, name
                      From Spec_Family__c
                      Where id =: specRecord.Id];
        taskNew = new Task();
        taskNew.Status = 'In Progress';
    }
    
    public PageReference shareTask(){
        
        Task ts = new Task();
        ts.OwnerId = taskNew.OwnerId;
        ts.Status = taskNew.Status;
        ts.Subject = 'Check '+ specRecord.Name + ' from ' + System.UserInfo.getName();
        ts.Priority = taskNew.Priority;
        ts.Description = taskNew.Description;
        ts.Specification_URL__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + specRecord.Id;
        insert ts;
        PageReference pg = new PageReference('/' + specRecord.Id);
        pg.setRedirect(true);
        return pg;
        
    }
}
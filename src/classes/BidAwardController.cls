public with sharing class BidAwardController{
    public List<BidRequestCustom> lstBRCustom{get;set;}    
    public string internalPart{get;set;}
    public string specName{get;set;}
    public string specRight{get;set;}
    public Bid__c objBid{get;set;}
    public boolean filterSpec{get;set;}
    public string specRType{get;set;}
    public Integer totalNoOfSpec{get;set;}
    public Integer totalAwaitingAward{get;set;}
    public Integer totalAward{get;set;}
    public Integer totalUniqueSupp{get;set;}
    
    public String [] BidsFields;
    public String [] BidsRequestFields;
    public String [] SpecificationFields;

    public String accessRT;

    public BidAwardController(){
        BidsFields = new String [] {'Bid_Status__c','Supplier__c','Bid_Amount__c','Unit_Type__c','Client_Award_Notes__c','Supplier_Bid_Notes__c'};
        BidsRequestFields = new String [] {'Specification__c','Bid_Status__c'};
        SpecificationFields = new String [] {'Spec_Name__c','Quantity_per_Year__c','Price_per_Unit__c',
                                             'Name','Prior_Part_Numbers__c'};
                                                             
        objBid=new Bid__c();
        filterSpec=false;
        internalPart='';
        specName='';
        specRight='';
        specRType='';
        queryBidRequest(new Set<Id>());
        totalNoOfSpec=0;totalAwaitingAward=0;totalAward=0;totalUniqueSupp=0;
        assignTotal(); 
                     
    }
    
    public pageReference queryBidRequest(Set<Id> setSpecId){
        lstBRCustom=new List<BidRequestCustom>();
        List<Bid_Request__c> lstBR = new List<Bid_Request__c>();
        
        // Check CRUD & FLS Enforcement
       // For BID__c
       // Obtaining the field name/token map for the Bid__c object
       Map<String,Schema.SObjectField> mapBid = Schema.SObjectType.Bid__c.fields.getMap();
       for (String fieldToCheck : BidsFields){
          // Check if the user has create access on the each field
          if (!mapBid.get(fieldToCheck).getDescribe().isAccessible()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                 'Insufficient access to Read Access.'));
            return null;
          }
       }

       //For Specification__c
       // Obtaining the field name/token map for the Specification__c object
        Map<String,Schema.SObjectField> mapSpec = Schema.SObjectType.Specification__c.fields.getMap();
        for (String fieldToCheck : SpecificationFields){
          
          // Check if the user has create access on the each field
          if (!mapSpec.get(fieldToCheck).getDescribe().isAccessible()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                 'Insufficient access to Read Access.'));
            return null;
          }
        }

       //For Bid_Request__c
       // Obtaining the field name/token map for the Bid_Request__c object
        Map<String,Schema.SObjectField> mapBidRequest = Schema.SObjectType.Bid_Request__c.fields.getMap();
        for (String fieldToCheck : BidsRequestFields){
          System.debug('******'+fieldToCheck);
          // Check if the user has create access on the each field
          if (!mapBidRequest.get(fieldToCheck).getDescribe().isAccessible()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                 'Insufficient access to Read Access.'));
             return null;
          }
        }   
                
        List<RecordType> lstRT = [Select Id, Name from RecordType where SObjectType = :'specright__Bid__c' and Name=:'Bid'];
        if(lstRt.size() > 0) {
            accessRT = lstRT[0].Id; 
        }
        //Query all Bid request which are not awarded
        if(setSpecId.size()>0 || filterSpec){
            lstBR = [select id,Specification__r.Name,Specification__r.RecordType.Name,
                            (select id,Bid_Status__c,Supplier__c,Bid_Amount__c,specright__Access_Start__c,
                            Unit_Type__c,Client_Award_Notes__c,Supplier_Bid_Notes__c,Specification__c,
                            Declined_Bid__c 
                            from Bids__r where Bid_Status__c='Submitted' and RecordType.Id =:accessRT and Declined_Bid__c=false),Specification__r.Spec_Name__c ,
                            Specification__r.Quantity_per_Year__c,Specification__r.Price_per_Unit__c,Specification__r.HQspecId__c 
                            from Bid_Request__c 
                            where Specification__c in :setSpecId and Bid_Status__c!='Awarded' limit 1000];
        }
        else{
            lstBR = [select id,Specification__r.Name,Specification__r.RecordType.Name,
                            (select id,Bid_Status__c,Supplier__c,Bid_Amount__c,specright__Access_Start__c,
                            Unit_Type__c,Client_Award_Notes__c,Supplier_Bid_Notes__c,Specification__c,
                            Declined_Bid__c 
                            from Bids__r where Bid_Status__c='Submitted' and RecordType.Id =:accessRT and Declined_Bid__c = false),
                            Specification__r.Spec_Name__c,
                            Specification__r.Quantity_per_Year__c,Specification__r.Price_per_Unit__c,Specification__r.HQspecId__c
                            from Bid_Request__c where Bid_Status__c!='Awarded' limit 1000];
        }

        //Create list of custom BidRequest records
        for(Bid_Request__c objBR:lstBR){            
            if(objBR.Bids__r.size()>0){
                lstBRCustom.add(new BidRequestCustom(objBR));
            }
        }
        
        return null;
    }
    
    public void assignTotal(){
        totalAward=0;
        totalAwaitingAward=0;
        
        //total no. of specifications
        totalNoOfSpec = lstBRCustom.size();
        Map<Id,Integer> mapUniqueVendor=new Map<Id,Integer>();
        
        //Count vendor uniqueness
        for(BidRequestCustom br:lstBRCustom){ 
            if(!br.awarded){                    
                totalAwaitingAward+=br.objBr.bids__r.size(); 
            
                for(Bid__c bid: br.objBr.bids__r){
                    if(!mapUniqueVendor.keyset().contains(bid.Supplier__c)){
                        mapUniqueVendor.put(bid.Supplier__c,1);
                    }                    
                } 
            }   
            else {
                totalAward+=1;
            }         
        }
        totalUniqueSupp=0;                
        totalUniqueSupp=mapUniqueVendor.keyset().size();                    
    }
    
    public pagereference searchSpecification(){   
       if(internalPart!='' || specName!='' || specRight!='' || specRType!=''){
           filterSpec=true;
       }
       else {
           filterSpec=false;
       }
       
       //CRUD & FLS Checking For Specification__c
        Map<String,Schema.SObjectField> mapSpec = Schema.SObjectType.Specification__c.fields.getMap();
        for (String fieldToCheck : SpecificationFields){
          // Check if the user has create access on the each field
          if (!mapSpec.get(fieldToCheck).getDescribe().isAccessible()) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                 'Insufficient access to Read Access.'));
            return null;
          }
        }
       string soql = 'select id,Spec_Name__c,Name,Prior_Part_Numbers__c,RecordType.Name, HQspecId__c  from Specification__c where id !=null';

       if(internalPart!=''){
           soql+=' and Name like \'%'+internalPart+'%\'';  }
       if(specName !=''){
           soql+=' and Spec_Name__c like \'%'+specName+'%\'';  }
       if(specRType!=''){
           soql+=' and RecordType.Name like \'%'+specRType+'%\'';  }    
        if(specRight != '')
          soql += ' AND HQspecId__c LIKE \'%' + specRight + '%\'';
                
       soql+=' limit 1000';
      
       List<Specification__c> lstSpec = Database.query(soql);
       Set<Id> setSpecID = new Set<id>();
       if(lstSpec !=null){
           for(Specification__c spec:lstSpec ){
               setSpecID.add(spec.Id);
           }
       }
       
       //query bid request
       queryBidRequest(setSpecID);
       return null;
   }
   
   public pagereference clearSpecFilter(){
       filterSpec=false;
       internalPart='';
       specRType='';
       specName='';
       specRight='';
       searchSpecification();       
       return null;
   }
   
   //Quick Save
   public pagereference quickSave(){
        filterSpec=false;
        List<Bid__c> lstBidToAward = new List<Bid__c>();
        List<Bid__c> lstToInsertAccess = new list<Bid__c>();
        string brid = ApexPages.currentPage().getParameters().get('brId');
        integer index;
        String accessRTId ='';
        accessRTId = [Select Id, Name from RecordType where SObjectType = :'specright__Bid__c' and Name = :'Incumbent Supplier'].Id;
        if(accessRTId == '') {
          return null;
        }
        if(brid!=null){
            index=Integer.valueOf(brid); 
        }
        
        
        if(index!=null && index<lstBRCustom.size()){ 
            BidRequestCustom newBr = lstBRCustom[index];         
            System.debug('*****2'+newBr.selectSameVendor + '  '+newBr.selectedVendor + '   '+newBr.mapVendBid.keyset());
            //if select same vendor is not selected
            if(!newBr.selectSameVendor && 
                newBr.selectedVendor!=null && newBr.selectedVendor!='' && 
                newBr.mapVendBid.keyset().contains(newBr.selectedVendor)){
                System.debug(newBr.mapVendBid);
                newBr.mapVendBid.get(newBr.selectedVendor).Bid_Status__c='Awarded';
                //Set Access End Date to today for Awarded Access
                Date dt = Date.ValueOf(newBr.mapVendBid.get(newBr.selectedVendor).specright__Access_Start__c);
                Integer getdiff = dt.daysBetween(Date.today());
                newBr.mapVendBid.get(newBr.selectedVendor).specright__Access_Duration__c = getdiff - 1;
                //Create new Incumbent Supplier Access record
                Bid__c bidobj = 
                  new Bid__c(Bid_Status__c='Waiting for Response', 
                            specright__Supplier__c= newBr.selectedVendor,
                            RecordTypeId = accessRTId,
                            specright__Access_Start__c = Date.today(),
                            specright__Access_Duration__c = 365,
                            Specification__c = newBr.mapVendBid.get(newBr.selectedVendor).Specification__c);
                lstToInsertAccess.add(bidobj);
                lstBRCustom[index].awarded=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,System.Label.BidAward_Bid_Awarded_Successfully));//'This Bid has been successfully awarded'
                
                // For BID__c
               // Obtaining the field name/token map for the Bid__c object
               Map<String,Schema.SObjectField> mapBid = Schema.SObjectType.Bid__c.fields.getMap();
               for (String fieldToCheck : BidsFields){
                  // Check if the user has create access on the each field
                  if (!mapBid.get(fieldToCheck).getDescribe().isUpdateable()) {
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                          'Insufficient access to Read Access.'));
                    return null;
                  }
               }
               
              update newBr.mapVendBid.get(newBr.selectedVendor);
              if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){ 
                  if ([SELECT count() FROM AsyncApexJob WHERE JobType='ScheduledApex' 
                        AND ApexClass.Name = 'AccessShareBatch' AND (Status = 'Processing' OR Status = 'Preparing')] < 1)
                           
                    Id batchId = Database.executeBatch(new AccessShareBatch(), 50);
              }
              if(lstToInsertAccess.size() > 0) {
                insert lstToInsertAccess;
                BidTriggerUtility.bidsShare(lstToInsertAccess);
              }
            }     
            
            //if select same vendor is true
            if(newBr.selectSameVendor && newBr.selectedVendor!=null && newBr.selectedVendor!=''){
                lstToInsertAccess = new List<Bid__c>();
                for(BidRequestCustom br:lstBRCustom){
                    if(br.mapVendBid.keyset().contains(newBr.selectedVendor)){
                        br.mapVendBid.get(newBr.selectedVendor).Bid_Status__c='Awarded';
                        //Set Access End Date to today for Awarded Access
                        Date dt = Date.ValueOf(br.mapVendBid.get(newBr.selectedVendor).specright__Access_Start__c);
                        Integer getdiff = dt.daysBetween(Date.today());
                        br.mapVendBid.get(newBr.selectedVendor).specright__Access_Duration__c = getdiff - 1;
                        //Create new Incumbent Supplier Access record
                        Bid__c bidobj = 
                          new Bid__c(Bid_Status__c='Waiting for Response', 
                                    specright__Supplier__c= newBr.selectedVendor,
                                    RecordTypeId = accessRTId,
                                    specright__Access_Start__c = Date.today(),
                                    specright__Access_Duration__c = 365,
                                    Specification__c = br.mapVendBid.get(newBr.selectedVendor).Specification__c);
                        lstToInsertAccess.add(bidobj);
                        br.awarded=true;
                        lstBidToAward.add(br.mapVendBid.get(newBr.selectedVendor));
                    }
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,System.Label.BidAward_Bid_Awarded_Successfully)); //'This Bid has been successfully awarded'
            }                
        }   
        
        //update bids to award
        if(lstBidToAward.size()>0){
        
          // For BID__c
          // Obtaining the field name/token map for the Bid__c object
          Map<String,Schema.SObjectField> mapBid = Schema.SObjectType.Bid__c.fields.getMap();
          for (String fieldToCheck : BidsFields) {
              // Check if the user has create access on the each field
              if (!mapBid.get(fieldToCheck).getDescribe().isUpdateable()) {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                     'Insufficient access to Read Access.'));
                return null;
              }
           }
          update lstBidToAward;
          if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){ 
            if ([SELECT count() FROM AsyncApexJob WHERE JobType='ScheduledApex' 
                  AND ApexClass.Name = 'AccessShareBatch' AND (Status = 'Processing' OR Status = 'Preparing')] < 1)
              Id batchId = Database.executeBatch(new AccessShareBatch(), 50);
          }
          if(lstToInsertAccess.size() > 0) {
            insert lstToInsertAccess;
            BidTriggerUtility.bidsShare(lstToInsertAccess);
          }
        } 
        
        //Calculate total                 
        assignTotal();        
        return null;
   }    
   
   public pagereference saveAll(){
      List<Bid__c> lstBidToAward = new List<Bid__c>();
      List<Bid__c> lstToInsertAccess = new list<Bid__c>();
      String accessRTId ='';
      accessRTId = [Select Id, Name from RecordType where SObjectType = :'specright__Bid__c' and Name = :'Incumbent Supplier'].Id;
      if(accessRTId == '') {
        return null;
      }
      system.debug(lstBRCustom);

      String venderid = '';
      for(BidRequestCustom objBr :lstBRCustom){
        if(objBr.selectSameVendor == true && objBr.selectedVendor!=null && objBr.selectedVendor!='' && 
                !objBr.awarded && objBr.mapVendBid.keyset().contains(objBr.selectedVendor)) {
          venderid = objBr.selectedVendor;
        }
      }
      //Award the bid with selected vendor id
      for(BidRequestCustom objBr :lstBRCustom){
            if(objBr.selectedVendor!=null && objBr.selectedVendor!='' && 
                !objBr.awarded && objBr.mapVendBid.keyset().contains(objBr.selectedVendor)){
              venderid = objBr.selectedVendor;
            }
            if(venderid != ''){

                objBr.mapVendBid.get(venderid).Bid_Status__c='Awarded';
                //Set Access End Date to today for Awarded Access
                Date dt = objBr.mapVendBid.get(venderid).specright__Access_Start__c;
                Integer getdiff = dt.daysBetween(Date.today());
                objBr.mapVendBid.get(venderid).specright__Access_Duration__c = getdiff - 1;
                lstBidToAward.add(objBr.mapVendBid.get(venderid));
                //Create new Incumbent Supplier Access record
                Bid__c bidobj = 
                  new Bid__c(Bid_Status__c='Waiting for Response', 
                          specright__Supplier__c= venderid,
                          RecordTypeId = accessRTId,
                          specright__Access_Start__c = Date.today(),
                          specright__Access_Duration__c = 365,
                          Specification__c = objBr.mapVendBid.get(venderid).Specification__c);
                lstToInsertAccess.add(bidobj);
            }
       }
       System.debug(lstBidToAward);
       if(lstBidToAward.size()>0){
           // For BID__c
           // Obtaining the field name/token map for the Bid__c object
           Map<String,Schema.SObjectField> mapBid = Schema.SObjectType.Bid__c.fields.getMap();
           for (String fieldToCheck : BidsFields){
              // Check if the user has create access on the each field
              if (!mapBid.get(fieldToCheck).getDescribe().isUpdateable()) {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                     'Insufficient access to Read Access.'));
                return null;
              }
           }
           
          update lstBidToAward;
          if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){ 
            if ([SELECT count() FROM AsyncApexJob WHERE JobType='ScheduledApex' 
                        AND ApexClass.Name = 'AccessShareBatch' AND (Status = 'Processing' OR Status = 'Preparing')] < 1)
              Id batchId = Database.executeBatch(new AccessShareBatch(), 50);
          }
          if(lstToInsertAccess.size() > 0) {
            insert lstToInsertAccess;
            BidTriggerUtility.bidsShare(lstToInsertAccess);
          }
       }  
       
       pagereference pg = new pagereference('/apex/BidAward');
       pg.setRedirect(true);
       return pg;
   }  
    

    public class BidRequestCustom{
        public Bid_Request__c objBR{get;set;}
        public string selectedVendor{get;set;}
        //public List<BidCustom> lstBid{get;set;}
        public Map<Id,Bid__c> mapVendBid{get;set;}
        public boolean selectSameVendor{get;set;}
        public boolean awarded{get;set;}
        
        public BidRequestCustom(Bid_Request__c br){
            objBR = br;
            //lstBid=new List<BidCustom>();
            mapVendBid = new Map<Id,Bid__c>();
            selectSameVendor=false;
            selectedVendor='';
            awarded=false;
            //create custom bid records from bids list
            for(Bid__c b:br.bids__r){
                mapVendBid.put(b.Supplier__c,b);
                //lstBid.add(new BidCustom(b));
            }
            //selectedVendor=br.bids__r[br.bids__r.size()-1].Supplier__c;
        }
                
    }
    
    public class BidCustom{
        public Bid__c objBid{get;set;}
        public boolean selected{get;set;}
        
        public BidCustom(Bid__c b){
            selected=false;
            objBid=b;
        }
    }

}
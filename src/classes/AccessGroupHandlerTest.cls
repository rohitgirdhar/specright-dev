@isTest
private class AccessGroupHandlerTest {

        AccessGroupHandler ObjHandler = new AccessGroupHandler();

    static testMethod void AccessGroupHandlerTestMethod() {
        
       List<BidGroup__c> lstbidGrp = new List<BidGroup__c>(); 
       BidGroup__c BidGroup = new BidGroup__c();
       BidGroup.Access_Duration__c = 5;
       BidGroup.Access_End__c = system.today()+1;
       lstbidGrp.add(BidGroup);
       insert lstbidGrp;
       
       Specification__c Spec = new Specification__c();
       Spec.Name = 'Spec Test';
       insert Spec;
       
       Bid_Request__c BidRequest = new Bid_Request__c();
       BidRequest.Bid_Status__c = 'In Process';
       BidRequest.Specification__c = Spec.Id;
       BidRequest.BidGroup__c = BidGroup.Id;
       insert BidRequest;
       
       List<Bid__c> lstbid = new List<Bid__c>();
       Bid__c bid = new Bid__c();
       bid.Access_Duration__c = 7;
       bid.Access_Start__c = system.today();
       bid.Bid_Request__c = BidRequest.Id;
       lstbid.add(bid);
       insert lstbid;
       
       Map<Id, BidGroup__c> newMapAccessGroup = new Map<Id, BidGroup__c>();
       newMapAccessGroup.put(BidGroup.id , BidGroup);
       AccessGroupHandler.setAccessEnd(newMapAccessGroup);
       AccessGroupHandler.updateRecordtype(lstbidGrp);
       List<BidGroup__c> lstBidGroup = [Select Id from BidGroup__c where id =: lstbidGrp[0].id];
       system.assert(lstBidGroup[0].Id != null);
    }
    
     static testMethod void AccessGroupHandlerMethodForAccessDuration() {
      
       BidGroup__c BidGroup = new BidGroup__c(Access_Duration__c = 5,
                                            Access_End__c = system.today()-1);
       insert BidGroup;
       
       Map<Id, BidGroup__c> newMapAccessGroup = new Map<Id, BidGroup__c>();
       newMapAccessGroup.put(BidGroup.id , BidGroup);
       AccessGroupHandler.setAccessEnd(newMapAccessGroup);
       List<BidGroup__c> lstSpecBidGroup = [Select id,Access_End__c from BidGroup__c where id =: BidGroup.Id];
       System.assert(lstSpecBidGroup.size()>0);
     }
     
     static testMethod void AccessGroupHandlerMethodForAccessDurationnull() {
      
       BidGroup__c BidGroup = new BidGroup__c(Access_Duration__c = null,
                                                  Access_End__c = system.today());
       insert BidGroup;
       
       Map<Id, BidGroup__c> newMapAccessGroup = new Map<Id, BidGroup__c>();
       newMapAccessGroup.put(BidGroup.id , BidGroup);
       AccessGroupHandler.setAccessEnd(newMapAccessGroup); 
       List<BidGroup__c> lstSpecBidGroup = [Select id,Access_End__c, Access_Duration__c from BidGroup__c where id =: BidGroup.Id];
       system.assertNotEquals(lstSpecBidGroup[0].Access_Duration__c,10);

     }
  }
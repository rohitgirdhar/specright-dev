@isTest
private class AccessShareBatchTest {
//Test for the AccessShareBatchTest method

static testmethod void  BatchTest() {	
	// Create new Specification.
	specright__Specification__c sp = new specright__Specification__c();
	sp.Name = 'Test internalPart';
	sp.specright__Prior_Part_Numbers__c = 'Test priorPar';
	insert sp;

	// Create new Access.
	specright__Bid__c sb = new specright__Bid__c();
	sb.specright__Specification__c = sp.id;
	sb.Access_Start__c = system.today();
	sb.Access_Duration__c = 2;
	insert sb;

	// Query Bid sharing records.
	List<Bid__c> lstbid = new List<Bid__c>();

	Bid__c bid = [Select Id, Access_End__c, Specification__c from Bid__c where Specification__c  = :sp.Id ];
	lstbid.add(bid);


	SchedulableContext sc;
	Database.BatchableContext BC;

	// Create Instance of class to call Methods
	AccessShareBatch share = new AccessShareBatch();
	share.execute(sc);
	share.start (BC);
	share.finish(BC);
	share.execute(BC,lstbid);

	system.assert(lstbid!=null);
}
	
}
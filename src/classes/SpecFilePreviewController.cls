public with sharing class SpecFilePreviewController {
    public Specification__c spec;
    public List<Id> photoIds = new List<Id>();
    public Map<Id, String> allFileName = new Map<Id, String>();
    public Map<Id, String> photoNameMap = new Map<Id, String>();
    public Map<Id, Id> fileIdMap = new Map<Id, Id>();
    public List<Id> pdfIds = new List<Id>();
    public List<Id> docIds = new List<Id>();
    public Map<Id, String> docLinkMap = new Map<Id, String>();
    public Set<String> supportedPDF = new Set<String> {'pdf'};
    public Set<String> supportedImageType = new Set<String> {'jpeg', 'jpg', 'gif', 'png'};      
    public Set<String> supportedDoc = new Set<String> {'msword', 'doc', 'docx'};
    public Set<Id> fileIdsSet = new Set<Id>();
    public Boolean showImage {get; set;}
    public Boolean showPDF {get; set;}
    public Boolean showDOC {get; set;}
    //public Map<Id,Spec_File__c> specFiles{get;set;}
    public SpecFilePreviewController(ApexPages.StandardController std) {
        //setDockLink();
        spec = (Specification__c)std.getRecord();
        /*
        specFiles = new Map<Id,Spec_File__c>([Select id, File_Type__c, Specification__r.name, File_Type2__c
                                              From Spec_File__c
                                              Where Specification__c =: spec.Id]);
        */
        showImage = false;
        showPDF = false;
        showDOC = false;
        typeOfFiles();
    }
    
    public void typeOfFiles(){
        for(Spec_File__c files :[Select id, Name, Spec_File_Name__c, File_Type__c, Specification__r.name, File_Type2__c From Spec_File__c Where Specification__c =: spec.Id]) {
            fileIdsSet.add(files.Id);
            //allFileName.put(files.Id, files.Spec_File_Name__c);
            allFileName.put(files.Id, files.Spec_File_Name__c==null ? '' : files.Spec_File_Name__c);
            /*
            String fileType = files.File_Type2__c;
            fileType = fileType.replaceAll('^(.*[\\/])', '');
            if(supportedImageType.contains(fileType)){
                showImage = true;
                imageIdsSet.add(files.Id);
            }
            else if(supportedPDF.contains(fileType)){
                showPDF = true;
                pdfIdsSet.add(files.Id);
            }
            else if(supportedDoc.contains(fileType)){
                showDOC = true;
                docIdsSet.add(files.Id);
            }
			*/
        }
        for(Attachment att : [Select Id, Name, ParentId From Attachment Where ParentId in: fileIdsSet]){
            String extension = att.Name;
            extension = extension.replaceAll('^(.*[\\.])', '');
            extension = extension.toLowerCase();
            System.debug('Here: ' + extension);
            if(supportedImageType.contains(extension)){
                showImage = true;
                photoIds.add(att.Id);
                photoNameMap.put(att.Id, allFileName.get(att.ParentId));
                fileIdMap.put(att.Id, att.ParentId);
            }
            else if(supportedPDF.contains(extension)){
                showPDF = true;
                pdfIds.add(att.Id);
                fileIdMap.put(att.Id, att.ParentId);
                //pdfNameMap.put(att.Id, allFileName.get(att.ParentId));
            }
            else if(supportedDoc.contains(extension)){
                showDOC = true;
                docIds.add(att.Id);
                docLinkMap.put(att.Id, 'https://view.officeapps.live.com/op/embed.aspx?src=https://s3.amazonaws.com/SpecBucket/'+ att.Name);
                fileIdMap.put(att.Id, att.ParentId);
                //wordNameMap.put(att.Id, allFileName.get(att.ParentId));
                //docIds.add(att.Name);
            }
        }
    }
    
    public Map<Id, String> photoMap{
        get{
            return photoNameMap;
        }
        set;
    }
    
    public Map<Id, Id> fileId{
        get{
            return fileIdMap;
        }
        set;
    }
    
    public Id specRecordId{
        get{
            return spec.Id;
        }
        set;
    }
    
    public List<Id> photos{
        get{
            /*
            if(photoIds == null){
                photoIds = new List<Id>();
                for(Attachment att : [select Id, Name from Attachment where ParentId in: imageIdsSet]) {
                    photoIds.Add(att.Id);
                }
            }
			*/
            return photoIds;
        }
        set;
    }
    
    public List<Id> pdfs{
        get{
            /*
            if(pdfIds == null){
                pdfIds = new List<Id>();
                for(Attachment att : [Select Id, Name From Attachment Where ParentId in: pdfIdsSet]){
                    pdfIds.Add(att.Id);
                }
            }
			*/
            return pdfIds;
        }
        set;
    }
    
    public Map<Id, String> docLinks{
        get{
            return docLinkMap;
        }
        set;
    }
    
    public List<Id> docs{
        get{
            /*
            if(docIds == null){
                docIds = new List<Id>();
                for(Attachment att : [Select Id From Attachment Where ParentId in: docIdsSet]){
                    docIds.add(att.Id);
                }
            }
			*/
            return docIds;
        }
        set;
    }
}
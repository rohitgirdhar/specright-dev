@isTest
public with sharing class CRelatedListCtlrTest {

    public static testMethod void testController(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        system.assertEquals('Test Account',objAccount.Name);  
        insert objAccount;
        
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test internalPart';
        objSpecification.Prior_Part_Numbers__c = 'Test priorPart';
        insert objSpecification; 
        
        // insert any one of the related list like Embellishments
        Embellishments__c em = new Embellishments__c(specification__c=objSpecification.id, HQ_Embellishments_ID__c='12313',
            Embellishments__c='Test');
        insert em;
        
        Test.startTest();
        CRelatedListCtlr ctlr = new CRelatedListCtlr();
        ctlr.sObjType = 'specright__PMS_Instructions__c';
        ctlr.parentObjId = objSpecification.id; 
        ctlr.objLabel = 'test';
        ctlr.flsName = 'specright__Instructions';
        
        ctlr.initComponent();
        
        // assert that the lstRecords is not null
        System.assert(ctlr.lstRecords != null);
        Test.stopTest();
    }
}
public with sharing class SpecFileAttachmentController {
    
    public Spec_File__c objSpecFile { get;set; }
    public String fileName { get;set; }
    public List<Spec_File__c> lstSpecFile{get;set;}
    public Blob fileBody { get;set; }
    public ID specId { get;set; }  
    public ID parentId { get;set; }
    public Boolean isNewRecord{ get;set;}
    public String str{get;set;}
    public Integer filesize{get;set;}
    PUBLIC Attachment myAttachment{get;set;}
    public Boolean isSpec = true;
    public String actualFileName {get; set;}
    public String fileType {get; set;}
    public Boolean isDocument = true;
    
    public SpecFileAttachmentController(ApexPages.StandardController controller) {
        isNewRecord=False;
        objSpecFile=(Spec_File__c)controller.getRecord();
        specId =controller.getrecord().id;
        myAttachment = new Attachment();
        for(String str: ApexPages.currentPage().getParameters().keyset()){
            if(str.contains('_lkid')){  //At the Time NEW
                isNewRecord=True;//For section title
                parentId = ApexPages.currentPage().getParameters().get(str);
                List<Specification__c> findSpec = [Select Id
                                             From Specification__c
                                             Where Id =: parentId limit 1];
                if(!findSpec.isEmpty()){
                    isDocument = false;
                    objSpecFile.RecordTypeId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Specification').getRecordTypeId();
                    objSpecFile.Specification__c = parentId;
                }
                else{
                    isDocument = false;
                    isSpec = false;
                    objSpecFile.RecordTypeId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Spec Family').getRecordTypeId();
                    objSpecFile.Spec_Family__c = parentId;
                }
                //objSpecFile.Specification__c=specificationID;
                //objSpecFile.Spec_Family__c = ;            
            }
        }
        if(isDocument){
            if(specId == null){
                isNewRecord=True;
            }
            objSpecFile.RecordTypeId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Document').getRecordTypeId();
        }
         lstSpecFile= new List<Spec_File__c>();
         //CRUD & FLS - Check if sufficent access available on Spec_File__C object & Fields

            Boolean isSpecFileAccesible= Schema.sObjectType.Spec_File__c.isAccessible() && 
                                 Schema.sObjectType.Spec_File__c.fields.File_Type__c.isAccessible() && 
                                 Schema.sObjectType.Spec_File__c.fields.Spec_File_Name__c.isAccessible() &&
                                 Schema.sObjectType.Spec_File__c.fields.Specification__c.isAccessible() &&
                                 Schema.sObjectType.Spec_File__c.fields.Attachment_Id__c.isAccessible();
               
            if(!isSpecFileAccesible){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access));
            }
         
         lstSpecFile=[select id, File_Type__c, Standard_File_Type__c, Spec_File_Name__c, Specification__c, Attachment_Id__c  from Spec_File__c where id=:specID];
         if(!lstSpecFile.isEmpty()){
            objSpecFile=lstSpecFile[0];            
         }         
         str=(isNewRecord==True)?System.Label.SpecFileAttachment_Header_New:System.Label.SpecFileAttachment_Header_Replace;  // 'New Spec File Attachment' ----Replace Spec File Attachment
    
    }

    // SR - 214 - Standard Cancel button depends on retURL
    // so we need to override the behavior 
    
    public PageReference Cancel() {
        if(parentId == null && lstSpecFile.size() > 0) {
            // try to get specification record for the specfile
            if(isDocument){
                Schema.DescribeSObjectResult result = Spec_File__c.SObjectType.getDescribe();
                return new PageReference('/'+ result.getKeyPrefix() + '/o');
            }
            if(isSpec){
                parentId = lstSpecFile[0].Specification__c;
            }
            else{
                parentId = lstSpecFile[0].Spec_Family__c;
            }
            
        }
        if(parentId != null){
            return new PageReference('/' + parentId);
        }
        else if(specId != null){
            //For Replace File Only
            return new PageReference('/' + specId);
        }
        else{
            Schema.DescribeSObjectResult result = Spec_File__c.SObjectType.getDescribe();
            return new PageReference('/'+ result.getKeyPrefix() + '/o');
            // cannot determine specification file so stay on the same page.
        }
            
    }
    
    public PageReference UploadFile()  
    {
        System.debug('file name2 ' + actualFileName);
        System.debug('file type2 ' + fileType);
        boolean isError=false;                          
        boolean replaceFile=false;
        //Check if file is attached
        if(myAttachment.Body==null || myAttachment.Name==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.SpecFileAttachment_Attach_File)); //'Please attach a file.'
            myAttachment = new Attachment();
            isError=true;
        }
        
        //Check file name not null
        if(objSpecFile.Spec_File_Name__c==null || objSpecFile.Spec_File_Name__c==''){    
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Spec_File_Name_Required)); //Insufficient Access
            myAttachment = new Attachment();//to avoid maximum view state size limit
            isError=true;
        }
        
        //error if file not attached or spec file name not specified
        if(isError){
            return null;
        }
        
        //When record is New, Check Attachment null & insert Spec                     
        if(isNewRecord){
            //Check if sufficent access available
            if(!Schema.sObjectType.Spec_File__c.isCreateable()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access)); //'Insufficient access'
                myAttachment = new Attachment();//to avoid maximum view state size limit
                return null;
            }
            //Set File Name and Typ
            System.debug('file name ' + actualFileName);
            System.debug('file type ' + fileType);
            objSpecFile.Actual_File_Name__c = actualFileName;
            objSpecFile.File_Type2__c = fileType;
            insert objSpecFile;

			// share the newly created SpecFile with parent Spec Users  
            shareSpecFile(objSpecFile);
        }                           
        
        // to Delete Old Attachments before inserting the new attachment, if there is any.
        if(!Schema.sObjectType.Attachment.isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access)); //Insufficient Access
            myAttachment = new Attachment();//to avoid maximum view state size limit
            return null;
        }
        
        //Check for old attachment
        List<Attachment> lstDeleteAttachment = [select id from Attachment where ParentId=:objSpecFile.id];
        
        //CRUD & FLS Check if sufficent access available
        if(!Schema.sObjectType.Attachment.isDeletable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access)); //Insufficient Access
            myAttachment = new Attachment();//to avoid maximum view state size limit
            return null;
        }
        
        //Delete old Attachment
        if(!lstDeleteAttachment.isEmpty()){
            replaceFile = true;
            if(Schema.SObjectType.Attachment.isDeletable()){
                delete lstDeleteAttachment;
            }
            else{
                return null;
            }
        }                
        
        //Check if sufficent access available to update specFile
        if(!Schema.sObjectType.Attachment.isCreateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access)); //Insufficient Access
            myAttachment = new Attachment();//to avoid maximum view state size limit
            return null;
        }
        
        //Insert new Attachment
        Boolean isAttachmentCreateable = Schema.SObjectType.Attachment.isCreateable() &&
            Schema.SObjectType.Attachment.fields.Body.isCreateable() &&
            Schema.SObjectType.Attachment.fields.Name.isCreateable();
        if(!isAttachmentCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access));
            return null;
        }
        
        Attachment newAtt = new Attachment(ParentId = objSpecFile.id);
        newAtt.Body = myAttachment.Body;
        newAtt.Name = myAttachment.Name;
        if(Schema.sObjectType.Attachment.isCreateable()){
            insert newAtt;
        }
        //When Spec Editing with Attachment                                                               
        //objSpecFile.Attachment_Id__c = myAttachment.id;
        objSpecFile.Attachment_Id__c = newAtt.Id;
        
        //Check if sufficent access available
        if(!Schema.sObjectType.Spec_File__c.isUpdateable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.SpecFileAttachment_Insufficient_Access)); //Insufficient Access
            myAttachment = new Attachment();//to avoid maximum view state size limit
            return null;
        }                      
        
        upsert objSpecFile;  //update specfile with current attachment id   
        if(replaceFile){
            PageReference pg =new PageReference('/'+objSpecFile.Id);
            pg.setRedirect(true);        
            return pg;
        }
        else{
            PageReference pg =new PageReference('/'+objSpecFile.Id);
            pg.setRedirect(true);        
            return pg;
            //To Different Page By Spec Family to Spec
            /*
            if(isSpec){
                PageReference pg =new PageReference('/'+objSpecFile.Id);
                pg.setRedirect(true);        
                return pg;
            }
            else{
                PageReference pg =new PageReference('/'+objSpecFile.Id);
                pg.setRedirect(true);        
                return pg;
            }
			*/
            
        }
        
    }

	// share a SpecFile with its parent Specification Users
    public static list<specright__Spec_File__Share> shareSpecFile(specright__Spec_File__c specFile) {
    	list<specright__Spec_File__Share> specFileShares = new list<specright__Spec_File__Share>();

    	if (specFile != null && specFile.specright__Specification__c != null && Schema.sObjectType.specright__Spec_File__Share.isCreateable()) {
    		for (specright__Specification__Share specShare : getSpecShares(specFile.specright__Specification__c)) {

    			specFileShares.add(new specright__Spec_File__Share(ParentId = specFile.Id,
												                   AccessLevel = specShare.AccessLevel,
												                   RowCause = specShare.RowCause,
												                   UserOrGroupId = specShare.UserOrGroupId));
    		}

    		if (!specFileShares.isEmpty()) {
    			// perform a partial save
    			Database.insert(specFileShares, false);
    		}
    	}

    	return specFileShares;
    }

	// retrieve all Specification Shares
    public static list<specright__Specification__Share> getSpecShares(Id specId) {	//, String rowcause
    	list<specright__Specification__Share> specShares = new list<specright__Specification__Share>();

    	if (specId != null && Schema.sObjectType.specright__Specification__Share.isAccessible()) {
    		specShares = [Select 	ParentId, UserOrGroupId, AccessLevel, RowCause
    						from 	specright__Specification__Share
    						where 	ParentId = :specId];
    	}

    	return specShares;
    }
}
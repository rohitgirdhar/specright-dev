@RestResource(urlMapping='/webhooknew')
global class weOEMSpecification{

    public static RequestData testData {get; set;}
    @HttpPost
    global static String doPost(){
        RequestData d = null;

        if(!Test.isRunningTest())
            d = (RequestData) JSON.deserialize(RestContext.request.requestBody.tostring(), RequestData.class);
        else
            d = testData;
        //d.spec.specright__HQspecId__c = d.spec.id;
        d.spec.id = null;
        Map<String, RecordType> mapRT = new Map<String, RecordType>();
        for(RecordType rt: [Select id, name from RecordType where sObjectType='specright__Specification__c']) {
            mapRT.put(rt.name, rt);
        }

        if(d.spec.recordtype.name.startsWith('Flexible')) {
            System.debug('>>>Changing RT: ' + d.spec.recordtype);
            // get flexible record type from map
            RecordType rt = mapRT.get('Flexible');
            if(rt != null) {
                String rtId = rt.id;
                if(rtId != null)
                    d.spec.recordtypeid = rtId;            
            }
        }
        d.spec.recordtype = null;
        
        upsert d.spec specright__HQspecId__c;
        if(d.lstcoatings != null && d.lstcoatings.size() > 0) {
            Set<String> setHQId = new Set<String>();
            for(Coatings__c coating: d.lstcoatings) {
                coating.id = null;
                coating.HQ_Coatings_ID__c = coating.name;
                coating.specification__c = d.spec.id;
                setHQId.add(coating.HQ_Coatings_ID__c);
            }
            delete [Select id from Coatings__c where specification__c=:d.spec.id and
                HQ_Coatings_ID__c not in :setHQId];
            upsert d.lstcoatings specright__HQ_Coatings_ID__c;
        }
        else {
            delete [Select id from Coatings__c where specification__c=:d.spec.id];            
        }
        if(d.lstembelling != null && d.lstembelling.size() > 0) {
            Set<String> setEHQId = new Set<String>();
            for(Embellishments__c em: d.lstEmbelling) {
                em.id = null;
                em.HQ_Embellishments_ID__c = em.name;
                em.specification__c = d.spec.id;
                setEHQId.add(em.HQ_Embellishments_ID__c );
            }
            delete [Select id from Embellishments__c where specification__c=:d.spec.id and
                HQ_Embellishments_ID__c not in :setEHQId];
            
            upsert d.lstembelling specright__HQ_Embellishments_ID__c;
        }
        else {
            delete [Select id from Embellishments__c where specification__c=:d.spec.id];            
        }
        if(d.lstlayer != null && d.lstlayer.size() > 0) {
         Set<String> setlHQId = new Set<String>();
            for(Layer_Material__c lm: d.lstLayer) {
                lm.id = null;
                lm.HQ_Layer_Materials_ID__c = lm.name;
                lm.specification__c = d.spec.id;
                setlHQId.add(lm.HQ_Layer_Materials_ID__c );
            }
            delete [Select id from Layer_Material__c where specification__c=:d.spec.id and
                HQ_Layer_Materials_ID__c not in :setlHQId];
            upsert d.lstlayer specright__HQ_Layer_Materials_ID__c;
        }
        else {
            delete [Select id from Layer_Material__c where specification__c=:d.spec.id];               
        }

        if(d.lstpms != null && d.lstpms.size() > 0) {
         Set<String> setpHQId = new Set<String>();
            for(PMS_Instructions__c pm: d.lstPMS) {
                pm.id = null;
                pm.HQ_PMS_Instructions_ID__c = pm.name;
                pm.specification__c = d.spec.id;
                setpHQId.add(pm.HQ_PMS_Instructions_ID__c );
            }
            delete [Select id from PMS_Instructions__c where specification__c=:d.spec.id and
                HQ_PMS_Instructions_ID__c not in :setpHQId];
            upsert d.lstpms specright__HQ_PMS_Instructions_ID__c;            
        }
        else {
            delete [Select id from PMS_Instructions__c where specification__c=:d.spec.id];
            
        }
        for(specright__Spec_File__c s : d.specFileList){
            s.specright__HQspecFileId__c = s.Id;
            s.specright__Specification__c = d.spec.Id;
            s.id = null;
        }
        upsert d.specFileList specright__HQspecFileId__c;
        
        //Map of SpecFile external id to internal id
        Map<String, specright__Spec_File__c> specFileMap = new Map<String, specright__Spec_File__c>();
        for(specright__Spec_File__c f : d.specFileList){
            specFileMap.put(f.specright__HQspecFileId__c, f);
        }
        
        
        //Delete existing attachments
        Map<Id, specright__Spec_File__c> specFileMapOld = new Map<Id, specright__Spec_File__c>(d.specFileList);
        delete [SELECT Id FROM Attachment WHERE ParentId IN :specFileMapOld.keySet()];
        
        
        if(d.specFileMap != null){
            //Upload new attachments
            List<Attachment> aList = new List<Attachment>();
            for(String sFileId : d.specFileMap.keySet()){
                List<String> aStringList = d.specFileMap.get(sFileId);
                Integer index = 0;
                for(String s : aStringList){
                    aList.add(new Attachment(
                        Body = EncodingUtil.base64Decode(s),
                        ParentId = specFileMap.get(sFileId).Id,
                        Name = specFileMap.get(sFileId).Attachments.get(index).Name
                    ));
                    index++;
                }
            }
            insert aList;
            //Update the attachment id
            for(Attachment a : aList){
                specFileMapOld.get(a.ParentId).Attachment_Id__c = a.Id;
            }
        }
        
        update specFileMapOld.values();

        return null;
}


    global class RequestData{
        public specright__Specification__c spec{get;set;}
        public List<specright__Spec_File__c> specFileList{get;set;}
        public List<specright__Coatings__c> lstcoatings{get;set;}
        public List<Layer_Material__c> lstlayer {get; set;}
        public List<PMS_Instructions__c> lstpms {get; set;}
        public List<Embellishments__c> lstembelling {get; set;}
        public Map<String, List<String>> specFileMap{get;set;}
        public RequestData(){}
        public RequestData(specright__Specification__c spec, 
                List<specright__Spec_File__c> specFileList, 
               List<specright__Coatings__c> lstcoatings,
               List<specright__Embellishments__c> lstembelling, 
               List<specright__Layer_Material__c> lstlayer,   
               List<specright__PMS_Instructions__c> lstpms){
         // public RequestData(specright__Specification__c spec, List<specright__Spec_File__c> specFileList) {          
            this.spec = spec;
            this.specFileList = specFileList;
            this.lstcoatings = lstcoatings;
            this.lstembelling = lstembelling;
            this.lstlayer = lstlayer;
            this.lstpms = lstpms;
        }

    }

}
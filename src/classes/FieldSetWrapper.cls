/**
 * RA, 06/09/2017
 * The apex class supports the lightning component for rendering a fieldset
 * for an sObject.
 */
public with sharing class FieldSetWrapper {

	public FieldSetWrapper(Schema.FieldSetMember f, Object val) {
        this.DBRequired = f.DBRequired;
        this.fieldPath = f.fieldPath;
        this.label = f.label;
        this.required = f.required;
        this.type = '' + f.getType();
        if(val != null) {
            this.val = String.valueof(val);
        }
    }

    public FieldSetWrapper(Boolean DBRequired) {
        this.DBRequired = DBRequired;
    }

    @AuraEnabled
    public Boolean DBRequired { get;set; }

    @AuraEnabled
    public String fieldPath { get;set; }

    @AuraEnabled
    public String label { get;set; }

    @AuraEnabled
    public Boolean required { get;set; }

    @AuraEnabled
    public String type { get; set; }

    // value for the field itself
    @AuraEnabled
    public String val {get; set;}

    @AuraEnabled
    public static List<FieldSetWrapper> getFields(String typeName, String fsName, String recordId) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
        
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetWrapper> fset = new List<FieldSetWrapper>();
        
        /**
         * initialize the object and the values
         */
        String query = 'SELECT ' + getQueryStringForSObject(typeName, targetType) + ' FROM ' + typeName
            + ' WHERE id=\'' + recordId + '\'';
        SObject sObj = Database.query(query)[0];

        for (Schema.FieldSetMember f: fieldSet) {
            if(f != null) {
                fset.add(new FieldSetWrapper(f, sObj.get(f.getFieldPath())));
            }
        }

        System.debug('>>>fset: ' + fset);

        return fset;
    }

    public static String getQueryStringForSObject(String objName, Schema.SObjectType objTokenFile){
        //Get spec file related to above specification
        if(objTokenFile == null) {
            objTokenFile = Schema.getGlobalDescribe().get(objName);
        }

        DescribeSObjectResult objDeffile = objTokenFile.getDescribe();
        
        //Get fields of sObject
        Map<String, SObjectField> fieldsFile = objDeffile.fields.getMap();
        string fieldLstFile='';
        Set<String> fieldSetFiles = fieldsFile.keySet();
        for(String s:fieldSetFiles)
        {
            SObjectField fieldToken = fieldsFile.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            fieldLstFile=fieldLstFile+selectedField.getName()+',';
        }
        fieldLstFile= fieldLstFile.removeEnd(',');
        return fieldLstFile;
    }
}
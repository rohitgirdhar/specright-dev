@isTest
public class SpecRightInstallScriptTest{
   public static testMethod void validateSpecRightInstallScript() 
   {
    SpecRightPostInstallScript postinstall = new SpecRightPostInstallScript();
    Test.testInstall(postinstall, null);
    
    /**
     * RA, 08/21/2017
     * PROD-71 - since the post install script has been deprecated, no real code
     * is expected to execute, so only assertion possible is if the code has reached this spot.
     */
    System.assert(postInstall != null);
   }
}
@isTest
public class specHistoryTrackTest {
    public static testMethod void specHistoryTracker() {
        User u = getUser();
        System.runAs(u) {
            //Test When a record is created
            Specification__c objSpecification = new Specification__c();
            objSpecification.name = 'Test Specification';
            objSpecification.Style__c = 'test123';
            objSpecification.Description2__c = '';
            insert objSpecification;
            
            Inbound_Material__c objIM = new Inbound_Material__c();
            objIM.name = 'Test IM';
            objIM.Specification__c = objSpecification.Id;
            objIM.Core__c = 'abcde';
            insert objIM;
            
            Outbound_Finished_Product__c objOFP = new Outbound_Finished_Product__c();
            objOFP.name = 'Test OFP';
            objOFP.SpecificationL__c = objSpecification.Id;
            objOFP.Location__c = 'Location ABDE';
            insert objOFP;
            
            History__c createdHistory = [Select s.User__c, s.Specification__c, s.Date__c, s.Action__c 
                                                    From History__c s
                                                    Where s.Specification__c =: objSpecification.Id limit 1];
            System.assertEquals('Specification Created.', createdHistory.Action__c);
            System.assertEquals(u.Id, createdHistory.User__c);
            
            
            //Test When Updating some Fields
            objSpecification.Style__c = 'test';
            objSpecification.Description2__c = getRandomStr();
            update objSpecification;
            
            History__c updateHistory = [Select s.User__c, s.Specification__c, s.Date__c, s.Action__c 
                                                   From History__c s
                                                   Where s.Specification__c =: objSpecification.Id
                                                   Order By s.Date__c DESC limit 1];
            System.assertEquals(u.Id, updateHistory.User__c);
        }
    }
    
    private static User getUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testSystemAdmin1234@testorg.com');
        return u;
    }
    
    public static testMethod void exchangeHistoryTest(){
        User u = getUser();
        System.runAs(u) {
            Specification__c objSpecification = new Specification__c();
            objSpecification.name = 'Test Specification';
            objSpecification.Style__c = 'test123';
            objSpecification.Description2__c = '';
            insert objSpecification;
            
            BidGroup__c objE = new BidGroup__c();
            objE.Notes__c = 'test abcde';
            insert objE;
            
            objE.Notes__c = 'Update abdcde';
            update objE;
            
            Bid_Request__c objES = new Bid_Request__c();
            objES.Specification__c = objSpecification.Id;
            objES.BidGroup__c = objE.Id;
            objES.Transaction_ID__c = 'Some ID';
            insert objES;
            
            objES.Transaction_ID__c = 'Update Id';
            update objES;
            
            Bid__c objESP = new Bid__c();
            objESP.Supplier_Bid_Notes__c = 'Some Notes';
            insert objESP;
            
            objESP.Supplier_Bid_Notes__c = 'Update Notes';
            update objESP;
            
            Bid__c testObjESP = [Select Supplier_Bid_Notes__c From Bid__c Where Supplier_Bid_Notes__c =: objESP.Supplier_Bid_Notes__c];
            System.assertEquals('Update Notes', testObjESP.Supplier_Bid_Notes__c);
        }
        
    }
    
    private static String getRandomStr() {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 150) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }
}
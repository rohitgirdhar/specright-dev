@isTest
public class AVPSSpecificationNewButtonControllerTest {
    @isTest
    static void testCloneOldSpec(){
        Specification__c spec = new Specification__c();
        spec.Name = 'test spec';
        insert spec;
        
        Coatings__c coating = new Coatings__c();
        coating.Specification__c = spec.id;
        insert coating;
        
        Embellishments__c embell = new Embellishments__c();
        embell.Specification__c = spec.id;
        insert embell;
        
        Layer_Material__c LM = new Layer_Material__c();
        LM.Specification__c = spec.id;
        insert LM;
        
        PMS_Instructions__c PMS = new PMS_Instructions__c();
        PMS.Specification__c = spec.id;
        insert PMS;
        
        Account testAccount = new Account();
        testAccount.name = 'test Account';
        insert testAccount;
        
        Specification__c specSupplier = new Specification__c();
        specSupplier.Supplier__c = testAccount.Id;
        
        ApexPages.StandardController sc = new ApexPages.standardController(spec);
        AVPSSpecificationNewButtonController ctr = new AVPSSpecificationNewButtonController(sc);
        ctr.specId = spec.Id;
        ctr.specSupplier = specSupplier;
        Specification__c newSpec = ctr.cloneOldSpec();
        insert newSpec;
        System.assertEquals(spec.Name + ' - ' + testAccount.Name, newSpec.Name);
        ctr.cloneFlexiableRelatedSpec(spec.id, newSpec.id);
    }
    
    @isTest
    static void testCancel() {
        
        Specification__c spec = new Specification__c();
        spec.Name = 'test spec';
        insert spec;
        
        ApexPages.currentPage().getParameters().put('specId', spec.id);
        ApexPages.StandardController sc = new ApexPages.standardController(spec);
        AVPSSpecificationNewButtonController ctr = new AVPSSpecificationNewButtonController(sc);
        PageReference backToSpec = ctr.cancel();
        System.assertEquals(true, backToSpec.getRedirect());
    }
    
    @isTest
    static void testToApproveRecord() {
        Specification__c spec = new Specification__c();
        spec.Name = 'test spec';
        insert spec;
        
        Account testAccount = new Account();
        testAccount.name = 'test Account';
        insert testAccount;
        
        Specification__c specSupplier = new Specification__c();
        specSupplier.Supplier__c = testAccount.Id;
        
        ApexPages.StandardController sc = new ApexPages.standardController(spec);
        AVPSSpecificationNewButtonController ctr = new AVPSSpecificationNewButtonController(sc);
        ctr.specSupplier = specSupplier;
        ctr.specId = spec.id;
        Pagereference pg = ctr.ToApproveRecord();
        System.assertEquals(true, pg.getRedirect());
    }
    
}
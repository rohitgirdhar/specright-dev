@isTest
private class BidControllerTest
{
    @isTest
    static void TestBidController()
    {

        Specification__c sp = new Specification__c();
        sp.Name = 'CDLECS4C';
        sp.Spec_Name__c = 'Test';
        sp.HQspecId__c = '4rd4t34ctw3t';
        insert sp;

        Bid__c bid = new Bid__c();
        bid.Specification__c = sp.Id;
        insert bid;

        ApexPages.StandardController sc = new ApexPages.StandardController(bid);
        BidController testAccPlan = new BidController(sc);
        PageReference pg = testAccPlan.pageRedirect();
        system.assertEquals(pg.getURL(),'/apex/specright__bidedit?retURL=%2Fa0A%2Fo');
    }
}
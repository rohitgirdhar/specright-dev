@isTest
private class SpecFilesDownloadControllerTest {
    
    @isTest static void testConstructor() {
        // Implement test code
        Specification__c spec = new Specification__c();
        spec.Name = 'test spec';
        insert spec;
		
        Spec_File__c objSpecFile =new Spec_File__c();
        objSpecFile.Spec_File_Name__c='Test File';
        objSpecFile.Specification__c=spec.id;
        objSpecFile.File_Type__c='Level 1';
        insert objSpecFile;
        
        Attachment attachment = new Attachment(Name='Test Attachment',body=blob.valueof('testbody'),parentId=objSpecFile.Id);
        insert attachment;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('specId', spec.id);     
        SpecFilesDownloadController ctlr = new SpecFilesDownloadController();
        ctlr.specId = spec.Id;
        ctlr.atts = new List<Attachment>();
        ctlr.atts.add(attachment);
        ctlr.redirect();        
        Boolean isSpecFileAccessible2=Schema.sObjectType.Spec_File__c.isAccessible() && Schema.sObjectType.Spec_File__c.fields.File_Type__c.isAccessible();
        system.assertequals(isSpecFileAccessible2,true);
        Test.stopTest();
    }
    
    @isTest static void testGetAttachment() {
        // Implement test code
        Spec_File__c spec = new Spec_File__c();
        insert spec;

        Attachment att = new Attachment(name='dGVzdA');
        att.body = (Blob.valueOf('test'));
        att.parentId = spec.id;
        insert att;

        Test.startTest();
        SpecFilesDownloadController.getAttachment(att.id);
        Boolean isSpecFileAccessible2=Schema.sObjectType.Spec_File__c.isAccessible() && Schema.sObjectType.Spec_File__c.fields.File_Type__c.isAccessible();
        system.assertequals(isSpecFileAccessible2,true);
        Test.stopTest();
    }
    
}
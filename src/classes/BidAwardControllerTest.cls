@isTest(seeAlldata=true)
private class BidAwardControllerTest {
  public static testMethod void validateBidAwardController(){

    
    Set<Id> setSpecId = new Set<Id>();   
    Map<Id,Bid__c > mapBid = new Map<Id, Bid__c>();
    Account acc = new Account();
    acc.Name = 'Test Account';
    system.assertEquals('Test Account',acc.Name);
    insert acc;
        
    Specification__c objSpec = new Specification__c();
    objSpec.Name = 'Test internalPart';
    objSpec.Prior_Part_Numbers__c = 'Test priorPart';
    insert objSpec;
    
    List<Bid_Request__c> lstBRreq = new List<Bid_Request__c>();
    Bid_Request__c objBidRequest = new Bid_Request__c();
    objBidRequest.Bid_Status__c = 'Submitted';
    objBidRequest.Specification__c = objSpec.Id;
    lstBRreq.add(objBidRequest);
    insert lstBRreq;
    
    List<Bid__c> lstBidToAward = new List<Bid__c>();
    Bid__c objBid = new Bid__c();
    objBid.Bid_Status__c = 'Submitted';
    objBid.Supplier__c = acc.Id;
    lstBidToAward.add(objBid);
    insert lstBidToAward;
  
    
    Bid__c objBid1 = new Bid__c();
    objBid1.id = objBid.id;
    objBid1.Bid_Status__c  = 'Awarded';
    objBid1.Supplier__c = acc.Id;
    update objBid1;
      mapBid.put( objBid.Supplier__c,objBid1);
    
     pagereference pr=page.BidAward;
     pr.getParameters().put('x', 'y'); 
     Test.setCurrentPage(pr);
     
    
    List<BidAwardController.BidRequestCustom> lstbr=new List<BidAwardController.BidRequestCustom>();

    BidAwardController.BidRequestCustom bidReq = new BidAwardController.BidRequestCustom(objBidRequest);
    
    bidReq.objBR = objBidRequest;
    bidReq.selectedVendor = acc.Id;
    bidReq.awarded = false;
    bidReq.mapVendBid = mapBid;
    bidReq.selectSameVendor = false;
    lstbr.add(bidReq);
    
    BidAwardController.BidCustom bidCustom = new BidAwardController.BidCustom(objBid);
    bidCustom.selected = false;
    bidCustom.objBid = objBid1;
    
    BidAwardController bidawardctlr = new BidAwardController();
    bidawardctlr.internalPart = 'Test';
    bidawardctlr.specName = 'Test';
    bidawardctlr.specRight = 'Test';
    bidawardctlr.totalNoOfSpec = 1;
    bidawardctlr.totalAwaitingAward = 1;
    bidawardctlr.totalAward = 1;
    bidawardctlr.totalUniqueSupp =1;
    bidawardctlr.objBid = objBid1;
    
    
    bidawardctlr.assignTotal();
    ApexPages.currentPage().getParameters().put('brId','0');
    bidawardctlr.quickSave();
    bidawardctlr.saveAll();
    bidawardctlr.searchSpecification();
    bidawardctlr.queryBidRequest(setSpecId);
    bidawardctlr.clearSpecFilter();
    
    ApexPages.Message[] pageMessages = ApexPages.getMessages();
     
    // Check that the error message you are expecting is in pageMessages
            Boolean messageFound = false;
            for(ApexPages.Message message : pageMessages) {
                  messageFound = true;        
                 
            }
    
    
    system.assertequals(bidawardctlr.searchSpecification(),null);
    system.assertequals(bidawardctlr.clearSpecFilter(),null);  
    }

   

   
}
public with sharing class FieldToFieldMigrationController {
    public string objectName {get; set;}
    public string oldFieldName {get; set;}
    public string newFieldName {get; set;}
    
    public String oldFields {get; set;}
    public String newFields {get; set;}
    //private static String stopTriggerField = 'specright__TriggerStopper__c';
    public FieldToFieldMigrationController() {
        
    }
    public pageReference migrateList() {
        List<String> oldFieldList = oldFields.split('\\s*,\\s*');
        List<String> newFieldList = newFields.split('\\s*,\\s*');
        
        if(String.isEmpty(oldFields) || String.isEmpty(newFields) || String.isEmpty(objectName)){
            return null;
        }
        
        String query = getDataFields(objectName, oldFieldList, newFieldList);
        System.debug('Query: ' + query);
        List<sObject> dataList = Database.query(query);
        List<sObject> updateList = new List<sObject>();
        for(sObject data : dataList){
            Boolean getUpdated = false;
            for(Integer i=0; i < oldFieldList.size(); i++){
                String newData = (String)data.get(oldFieldList[i]);
                System.debug('Old Field Data: ' + newData);
                System.debug('Old: ' + oldFieldList[i] + 'To New: ' + newFieldList[i]);
                if(!String.isEmpty(newData)) {
                    data.put(newFieldList[i], newData);
                    getUpdated = true;
                }
            }
            if(getUpdated){
                //data.put(stopTriggerField, true);
                updateList.add(data);
            }
            
        }
        if(updateList.size() > 0){
            update updateList;
        }
        
        
        Pagereference pg=new pagereference('/apex/FieldToFieldMigration');
        pg.setRedirect(true);
        return pg;
        //return null;
    }
    public pageReference migrate() {
        if(String.isEmpty(objectName) || String.isEmpty(oldFieldName) || String.isEmpty(newFieldName)) {
            return null;
        }
        String query = getDataFields(objectName, oldFieldName, newFieldName);
        //System.debug('Query: ' + query);
        List<sObject> dataList = Database.query(query);
        List<sObject> updateList = new List<sObject>();
        
        for(sObject data : dataList){
            String newData = (String)data.get(oldFieldName);
            if(!String.isEmpty(newData)) {
                data.put(newFieldName, newData);
                updateList.add(data);
            }
            //System.debug('Data: ' + newData);
        }
        if(updateList.size() > 0){
            update updateList;
        }
        
        Pagereference pg=new pagereference('/apex/FieldToFieldMigration');
        pg.setRedirect(true);
        return pg;
    }
    
    public static string getDataFields(String objectName, String oldFieldName, String newFieldName){
        
        String selects = '';
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    //System.debug('Name: ' + fd.getName());
                    if(fd.getName().equals(oldFieldName) || fd.getName().equals(newFieldName)){
                        selectFields.add(fd.getName());
                    }
                    
                }
            }
        }
        
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            }
            
        }
        return 'SELECT id, ' + selects + ' FROM ' + objectName;
        
    }
    
    public static string getDataFields(String objectName, List<String> oldFieldList, List<String> newFieldList){
        
        String selects = '';
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    //System.debug('Name: ' + fd.getName());
                    Set<String> oldSet = new Set<String>(oldFieldList);
                    Set<String> newSet = new Set<String>(newFieldList);
                    if(oldSet.contains(fd.getName()) || newSet.contains(fd.getName())){
                        selectFields.add(fd.getName());
                    }
                    
                }
            }
        }
        
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            }
            
        }
        return 'SELECT id, ' + String.escapeSingleQuotes(selects) + ' FROM ' + String.escapeSingleQuotes(objectName);
        
    }
}
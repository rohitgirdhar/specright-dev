@isTest
public class SpecificationSearchControllerTest {
    
    public static testMethod void pickListTest() {
        SpecificationSearchController ssc = new SpecificationSearchController();
        System.assertNotEquals(0, ssc.specStatusList.size()); 
    }
    
    public static testMethod void getLabelTest() {
        SpecificationSearchController ssc = new SpecificationSearchController();
        System.assertNotEquals(null, ssc.specInternalIdLabel);
        System.assertNotEquals(null, ssc.specNameLabel);
        System.assertNotEquals(null, ssc.specRTLabel);
        System.assertNotEquals(null, ssc.specStatusLabel);
    }
    
    public static testMethod void runQueryTest() {
        //For Suppliers
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        insert testAccount;
        
        Specification__c masterSpec = new Specification__c();
        masterSpec.Name = 'Master Spec';
        insert masterSpec;
        List<Specification__c> specTestList = new List<Specification__c>();
        Integer numberinList = 20;
        while(numberinList > 0){
            Specification__c objSpecification = new Specification__c();
            objSpecification.name = 'Test Specification ' + numberinList;
            objSpecification.Supplier__c = testAccount.Id;
            objSpecification.Status__c = 'Approved';
            objSpecification.Master_Specification__c = masterSpec.Id;
            specTestList.add(objSpecification);
            numberinList--;
        }
        insert specTestList;
        
        SpecificationSearchController ssc = new SpecificationSearchController();
        System.assertNotEquals(0, ssc.specList.size());
        String sortTest = ssc.sortDir;
        ssc.toggleSort();
        System.assertNotEquals(sortTest, ssc.sortDir);
        System.assertNotEquals('', ssc.debugSoql);
        
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', 'Cannot find me');
        System.currentPageReference().getParameters().put('specName', 'Test');
        System.currentPageReference().getParameters().put('masterSpec', 'Test');
        //System.currentPageReference().getParameters().put('specSupplier', 'Test');
        System.currentPageReference().getParameters().put('specStatus', 'Test');
        
        ssc.runSearch();
        System.assertEquals(0, ssc.specList.size());
        
        //Query will failed
        ssc.soql = 'Select failed From failed';
        ssc.runQuery();
    }
}
@isTest
private class InboundOutBoundHandlerTest {

    static testMethod void testInboundOutBoundHandler() {
        RecordType rt =[select id ,Name from RecordType where Name = 'Flexible'  and sObjectType ='specright__Specification__c' limit 1];
        Specification__c sp = new Specification__c();
        sp.Name = 'Test Specification';
        sp.RecordTypeId = rt.id;
        insert sp;
       
       // Handle test for Inbounds 
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Flexible'  and sObjectType ='specright__Inbound_Material__c' limit 1];
        
        Inbound_Material__c IBM = new Inbound_Material__c();
        IBM.Name = 'Test IBM';
        IBM.Specification__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;
        insert IBM;
        
        //Handle test for Outbounds
        RecordType rtOFP =[select id ,Name from RecordType where Name = 'Flexible'  and sObjectType ='specright__Outbound_Finished_Product__c' limit 1];
        
        Outbound_Finished_Product__c OFP = new Outbound_Finished_Product__c();
        OFP.Name = 'Test IBM';
        OFP.SpecificationL__c = sp.id;
        OFP.RecordTypeId = rtOFP.id;
        insert OFP;
        
        IBM =[Select Id from Inbound_Material__c];
        system.assert(IBM.Id != null);
        
        OFP =[Select Id from Outbound_Finished_Product__c];
        system.assert(OFP.Id != null); 
         
    }
}
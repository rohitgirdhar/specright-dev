/**
 * Controller class for VF Page to handle the New and Edit event of 
 * InboundMaterial custom object
 * @author - Wisdomedge Inc.
 */
 
public with sharing class CustomInboundMaterialCtlr {

    // record type derive from Specification and the same will apply to the related object.
    public String selectedRecordType {get; set;}
    
    // Custom object instance
    public Inbound_Material__c inboundMaterialObj {get; set;}
    
    // Custom Object instance for showing Id which is non-writable field
    public Inbound_Material__c inboundMaterialObjForId {get; set;}
    
    // inbound material prefix
    public String inboundMaterialPrefix {get; set;}
    
    // page message
    public String pageMessage {get; set;}
    
    /**
     * default page constructor
     * initialize the Inbound Material Record
     * also initializes the specRecordType
     */
    public CustomInboundMaterialCtlr(ApexPages.StandardController controller) {
        inboundMaterialObj = (Inbound_Material__c)controller.getRecord();
        initPage();
    }
    
    /**
     * custom page initialization method
     */
    public void initPage() {
        Boolean isInboundMaterialCreateable = Schema.sObjectType.Inbound_Material__c.isCreateable() &&
            Schema.sObjectType.Inbound_Material__c.fields.Name.isCreateable() &&
            Schema.sObjectType.Inbound_Material__c.fields.RecordTypeId.isCreateable();
        if(!isInboundMaterialCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
            return;
        }
        if (inboundMaterialObj.id == null) {
            System.debug('>>>inbound: ' + inboundMaterialObj);
            List<Specification__c> lstspecs = 
                    [Select Id, RecordType.Name from Specification__c where Id = :inboundMaterialObj.Specification__c];
            if(lstspecs.size() < 1) return;
            String specRT = lstspecs[0].RecordType.Name;
            String sobjtype = Inbound_Material__c.getSObjectType()+'';
            List<RecordType> lstRT = 
                    [Select Id from RecordType where SObjectType =:sobjtype and Name =:specRT];
            /*
            if(lstRT.isEmpty()) {
                System.debug('Empty Record');
                toDefaultRecordType();
            }
            */
            inboundMaterialObj.Name = 'Auto-generated';
            inboundMaterialObj.RecordTypeId = lstRT[0].Id;
            String rtid = lstRT[0].Id;
            selectedRecordType = specRT;
        }
        else {
            Inbound_Material__c lstinbounds =
                 [Select Id, Name, Id__c from Inbound_Material__c where Id=:inboundMaterialObj.Id limit 1];
            if(lstinbounds != null) {
                inboundMaterialObj.Name = lstinbounds.Name;
                inboundMaterialObjForId = lstInbounds;
            }
        }
        // initialize selectedRecordType
        RecordType lstRT = [Select Name from RecordType where id=:inboundMaterialObj.recordTypeId limit 1];
        if(lstRT != null) {
            selectedRecordType = lstRT.Name;
        }
    
    }
    
    /**
     * saves the current record and redirects to the specification
     * detail page.
     */
    public PageReference customSave() {
        pageMessage = '';
        try{
            if (Schema.sObjectType.Inbound_Material__c.isUpdateable() &&
                Schema.sObjectType.Inbound_Material__c.isCreateable()) {
                upsert inboundMaterialObj;
            	return new PageReference('/'+inboundMaterialObj.specification__c);
			}
            else{
                return null;
            }
        }
        catch(Exception e) {
            pageMessage = e.getMessage();
        }
        return null;
    }
    
    /**
     * custom Save and New method that will not only upsert the 
     * outbound finished product but will also prepare it for adding
     * another record under the same specification
     */
    public PageReference customSaveAndNew() {
        customSave();
        // re-initialize custom object for adding another record under same specification
        inboundMaterialObj = new Inbound_Material__c
            (specification__c=inboundMaterialObj.specification__c);
        initPage();
        return null;
    }
    
    //Direct to correct page
    public PageReference DirectToPage() {
        if (inboundMaterialObj.id == null) {
            System.debug('>>>inbound: ' + inboundMaterialObj);
            List<Specification__c> lstspecs = 
                    [Select Id, RecordType.Name from Specification__c where Id = :inboundMaterialObj.Specification__c];
            if(lstspecs.size() < 1) return null;
            String specRT = lstspecs[0].RecordType.Name;
            String sobjtype = Inbound_Material__c.getSObjectType()+'';
            List<RecordType> lstRT = 
                    [Select Id from RecordType where SObjectType =:sobjtype and Name =:specRT];
            
            if(lstRT.isEmpty()) {
                system.debug('test');
                PageReference returnPage = new PageReference('/' + inboundMaterialObj.Specification__c);
                
                PageReference saveNewPage = new PageReference('/' + inboundMaterialObj.Id + '/e');
                saveNewPage.getParameters().put('nooverride', '1');
                saveNewPage.getParameters().put('retURL', '/' + inboundMaterialObj.Specification__c);
                
                PageReference pg = new PageReference('/setup/ui/recordtypeselect.jsp');
                // set the entity type for the recordtype screen
                
                pg.getParameters().put('ent', '01Io0000001KUkE');
                pg.getParameters().put('save_new_url', saveNewPage.getURL());
                pg.getParameters().put('retURL', returnPage.getURL());

                //pg.setRedirect(true);
                return pg;
            }
            
            PageReference customPage = new PageReference('/apex/CustomInboundMaterial');
            return customPage;
        }
        return null;
    }
    
    public id getRecordTypeId(String name) {
        String recordTypeName = name;
        Map<String, Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Specification__c.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtInfo = rtMapByName.get(recordTypeName);
        return rtInfo.getRecordTypeId();
    }
}
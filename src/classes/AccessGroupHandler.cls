public with sharing class AccessGroupHandler {
	public static void setAccessEnd(Map<Id, BidGroup__c> newMapAccessGroup) {
		//Map of Access Group Id and Access Duration of Access group
		Map<Id, Decimal> mapAGIdAccessDuration = new Map<Id, Decimal>();
		//Map of Access Start date and Access Group Id
		Map<Id, Date> mapAGIdAccessStartDate = new Map<Id, Date>();

		for(BidGroup__c accessgroup :newMapAccessGroup.values()) {
			if(accessgroup.Access_Duration__c==null) {
				return;
			}
			//if Access End is passed, user can not update duration for Access Group
			if(accessgroup.Access_End__c < Date.today()) {
				accessgroup.addError(System.Label.Access_Group_Expired);
				mapAGIdAccessDuration = new Map<Id, Decimal>();
				break;
			}
			else {
				if(accessgroup.Access_Duration__c > 730)
					return;
				mapAGIdAccessDuration.put(accessgroup.Id, accessgroup.Access_Duration__c);
			}
		}
		Set<Id> lstaccessreq =new Set<Id>();
		List<Bid__c> lstAccessToUpdate = new List<Bid__c>();
		
		for(Bid__c objBid :
			[select id,Bid_Status__c,Access_Duration__c, Bid_Request__r.Id, Bid_Request__r.BidGroup__c,Access_Start__c
                                    from bid__c where Bid_Request__r.BidGroup__c in:newMapAccessGroup.keyset()]){ 
			lstaccessreq.add(objBid.Bid_Request__r.Id);
		}
	

		if(lstaccessreq.size() > 0) {
			List<Bid__c> lstaccess = new List<Bid__c>();
			for(Bid_Request__c objBidR :[select id, (Select Id, Bid_Request__r.BidGroup__c,Access_Start__c from Bids__r)
	                                    from Bid_Request__c where Id in:lstaccessreq]){ 
				for(Bid__c access :objBidR.Bids__r){
					lstaccess.add(access);
				}
			}
			System.debug(lstaccess);

			//Update Access Duration of Community Access in Access Group
			if(lstaccess.size() > 0 && !mapAGIdAccessDuration.isEmpty()) {
				for(Bid__c objBid :lstaccess) {
					objBid.Access_Duration__c = mapAGIdAccessDuration.get(objBid.Bid_Request__r.BidGroup__c);
					lstAccessToUpdate.add(objBid);
					mapAGIdAccessStartDate.put(objBid.Bid_Request__r.BidGroup__c, objBid.Access_Start__c);
				}
				if(lstAccessToUpdate.size() > 0)
					Update lstAccessToUpdate;
			}
		}

		//Update Access End date of Access Group
		if(!mapAGIdAccessStartDate.isEmpty()) {
			for(BidGroup__c accessgroup :newMapAccessGroup.values()) {
				if(accessgroup.Access_Duration__c!=null) {
					Integer duration = Integer.valueOf(accessgroup.Access_Duration__c);
		            Date dt = mapAGIdAccessStartDate.get(accessgroup.Id);      
		            if(duration!=null && dt!=null) {
			            dt = dt.addDays(duration);      
			            accessgroup.Access_End__c = dt;
		        	}
	        	}
	        }
		}
	}

	/**
	 * function to set the record type of Exchange Request, Exchange to match Exchange group
	 * record type
	 */
	public static void updateRecordtype(List<BidGroup__c> lstBidGroup) {

        Set<Id> bidGrpId = new Set<Id>();
        for(BidGroup__c objGrp : lstBidGroup){
            bidGrpId.add(objGrp.Id);
        }

        /**
         * map of record type name and exchange request
         */
        Map<String, Id> mapRTER = new Map<String, Id>();
        for(RecordType rt: [Select name, id from RecordType where sObjectType='Bid_Request__c']){
        	mapRTER.put(rt.name, rt.id);
        }

        /**
         * map of record type name and exchange
         */
        Map<String, Id> mapRTEx = new Map<String, Id>();
        for(RecordType rt: [Select name, id from RecordType where sObjectType='Bid__c']){
        	mapRTEx.put(rt.name, rt.id);
        }

        Map<Id, BidGroup__c> mapBG = new Map<Id, BidGroup__c>([Select id, recordtype.name from BidGroup__c
        	where id in :lstBidGroup]);
        
        List<Bid_Request__c> lstBidReqToUpdate = [Select id, BidGroup__r.recordtype.name from Bid_Request__c 
        	where BidGroup__c in :mapBG.keyset()];
        for(Bid_Request__c br: lstBidReqToUpdate) {
        	br.recordTypeId = mapRTER.get(br.BidGroup__r.recordtype.name);
        }
        
        if(lstBidReqToUpdate.size()>0){
            update lstBidReqToUpdate;
        }

        List<Bid__c> lstBidsToUpdate = [Select id, Bid_Request__r.recordtype.name from Bid__c 
        	where Bid_Request__c in :lstBidReqToUpdate ];

		for(Bid__C bid: lstBidsToUpdate) {
        	bid.recordTypeId = mapRTEx.get(bid.Bid_Request__r.recordtype.name);
        }
        
        if(lstBidsToUpdate.size()>0){
            update lstBidsToUpdate;
        }
    }
}
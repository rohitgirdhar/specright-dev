@isTest(SeeAllData=true)
public with sharing class BidTriggerUtilityTest {

    public static testMethod void ValidateBidTriggerUtility(){
        
        List<Bid__c> lstBid = new List<Bid__c>();
        List<Bid__c> lstBid1 = new List<Bid__c>();
        Set<Id> setBidId = new Set<Id>();
        Map<Id,Bid__c> oldMap = new Map<Id,Bid__c>();
        
             
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        system.assertEquals('Test Account',objAccount.Name);  
        insert objAccount;
        
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test internalPart';
        objSpecification.Prior_Part_Numbers__c = 'Test priorPart';
        insert objSpecification; 
        
        Bid_Request__c objBidRequest = new Bid_Request__c();
        objBidRequest.Bid_Status__c = 'In Process';
        objBidRequest.Specification__c = objSpecification.Id;
        insert objBidRequest;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test LastName';
        objContact.AccountId = objAccount.Id;
        insert objContact;
        //Create Portal USer
        Set < String > customerUserTypes = new Set < String > {
            'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'
        };
        Profile p = [select Id, name from Profile where UserType in : customerUserTypes limit 1];

        User newUser = new User(
            profileId = p.id,
            username = 'paul.henry22@yahoo.com',
            email = 'ph@ff.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias = 'nuser',
            lastname = 'lastname',
            contactId = objContact.id
        );
        insert newUser;

        Spec_File__c objSpecFiles = new Spec_File__c();
        objSpecFiles.Specification__c = objSpecification.Id;
        objSpecFiles.File_Type__c = 'Level 1';
        insert objSpecFiles;
          
        Bid__c objBid = new Bid__c();
        objBid.Bid_Request__c = objBidRequest.Id;
        objBid.Bid_Status__c = 'In Process';
        objBid.Specification__c = objSpecification.Id;
        objBid.Supplier__c = objAccount.Id;
        insert objBid;
        objBid.Bid_Status__c = 'Awarded';
        update objBid;
        //lstBid.add(objBid);
        setBidId.add(objBid.Id);
        oldMap.put(objBid.id, objBid);        
        //BidTriggerUtility objBidTriggerUtility = new BidTriggerUtility();
        
        Bid__c objBid1 = new Bid__c();
        objBid1.Bid_Request__c = objBidRequest.Id;
        objBid1.Bid_Status__c = 'In Process';
        objBid1.Specification__c = objSpecification.Id;
        objBid1.Supplier__c = objAccount.Id;
        
        /**
         * RA, 08/18 - Record Type is needed in the new model
         */
        List<RecordType> lstRT = [Select id from RecordType where sObjectType=:(Bid__c.getSObjectType() + '') and name='Bid'];
        if(lstRT.size() > 0)
            objBid1.recordTypeId = lstRT[0].id;
        insert objBid1;
        lstBid1.add(objBid1);
        BidTriggerUtility.bidsShare(lstBid1);
        BidTriggerUtility.triggerUnshare(setBidId);//, oldMap);
        
    
    }
}
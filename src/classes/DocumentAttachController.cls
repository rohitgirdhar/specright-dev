public with sharing class DocumentAttachController {
    public List<SelectOption> allSpec {
        get{
            allSpec = new List<SelectOption>();
            List<Specification__c> specList = [Select id, name
                                               From Specification__c limit 100];
            for(Specification__c spec : specList){
                allSpec.add(new SelectOption(spec.id, spec.name));
            }
            
            return allSpec;
        } 
        set;}
    public List<SelectOption> selectedSpec {get; set;}
    public DocumentAttachController(ApexPages.StandardController std){
        
    }
    /*
    public SelectOption[] allSpec(){
        allSpec.add(new SelectOption('US','US'));
        return allSpec;
    }
	*/
    public List<SelectOption> getItems() {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption('US','US'));
 	 	options.add(new SelectOption('CANADA','Canada'));
 	 	options.add(new SelectOption('MEXICO','Mexico'));
 	 	return options;
  	}
}
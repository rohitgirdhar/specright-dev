@isTest
public class SpecAnalyzerControllerTest {
    public static testMethod void getLabelTest() {
        SpecAnalyzerController ssc = new SpecAnalyzerController();
        System.assertNotEquals(null, ssc.specInternalIdLabel);
        System.assertNotEquals(null, ssc.specDepthLabel);
        System.assertNotEquals(null, ssc.specLengthLabel);
        System.assertNotEquals(null, ssc.specRTLabel);
        System.assertNotEquals(null, ssc.specStyleLabel);
        System.assertNotEquals(null, ssc.specWidthLabel);
        System.assertNotEquals(null, ssc.specFluteLabel);
        System.assertNotEquals(null, ssc.specMaterialComboLabel);
        System.assertNotEquals(null, ssc.specWeightGradeLabel);
        System.assertNotEquals(null, ssc.specSpecNameLabel);
        System.assertNotEquals(null, ssc.specSubstrateLabel);
        System.assertNotEquals(null, ssc.specStatusLabel);
    }
    
    public static testMethod void runQueryTest() {
        List<Specification__c> specTestList = new List<Specification__c>();
        Integer numberinList = 20;
        while(numberinList > 0){
            Specification__c objSpecification = new Specification__c();
            objSpecification.name = 'Test Specification ' + numberinList;
            specTestList.add(objSpecification);
            numberinList--;
        }
        insert specTestList;
        
        SpecAnalyzerController ssc = new SpecAnalyzerController();
        System.assertNotEquals(0, ssc.specList.size());
        String sortTest = ssc.sortDir;
        ssc.toggleSort();
        System.assertNotEquals(sortTest, ssc.sortDir);
        System.assertNotEquals('', ssc.debugSoql);
        
        //Search Everything
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        //System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', '');
        System.currentPageReference().getParameters().put('specLength', '');
        System.currentPageReference().getParameters().put('specWidth', '');
        System.currentPageReference().getParameters().put('specDepth', '');
        System.currentPageReference().getParameters().put('specStyle', '');
        System.currentPageReference().getParameters().put('lengthTolerance', '');
        System.currentPageReference().getParameters().put('depthTolerance', '');
        System.currentPageReference().getParameters().put('widthTolerance', '');
        System.currentPageReference().getParameters().put('specVolume', '');
        ssc.runSearch();
        System.assertEquals(20, ssc.specList.size());
        ssc.specFamilyName = 'test1234';
        ssc.saveToSpecFamily();
        ssc.specFamilyName = '';
        ssc.saveToSpecFamily();
        
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        //System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', 'testType');
        System.currentPageReference().getParameters().put('specLength', '2');
        System.currentPageReference().getParameters().put('specWidth', '2');
        System.currentPageReference().getParameters().put('specDepth', '2');
        System.currentPageReference().getParameters().put('specStyle', 'abc');
        System.currentPageReference().getParameters().put('lengthTolerance', '1');
        System.currentPageReference().getParameters().put('depthTolerance', '1');
        System.currentPageReference().getParameters().put('widthTolerance', '1');
        System.currentPageReference().getParameters().put('specVolume', 'true');
        
        ssc.runSearch();
        System.assertEquals(0, ssc.specList.size());
        ssc.saveToSpecFamily();
        
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        //System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', 'testType');
        System.currentPageReference().getParameters().put('specLength', '2');
        System.currentPageReference().getParameters().put('specWidth', '2');
        System.currentPageReference().getParameters().put('specDepth', '2');
        System.currentPageReference().getParameters().put('specStyle', 'abc');
        System.currentPageReference().getParameters().put('lengthTolerance', '');
        System.currentPageReference().getParameters().put('depthTolerance', '');
        System.currentPageReference().getParameters().put('widthTolerance', '');
        System.currentPageReference().getParameters().put('specVolume', 'true');
        
        ssc.runSearch();
        System.assertEquals(0, ssc.specList.size());
        
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        //System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', 'testType');
        System.currentPageReference().getParameters().put('specLength', '2');
        System.currentPageReference().getParameters().put('specWidth', '2');
        System.currentPageReference().getParameters().put('specDepth', '2');
        System.currentPageReference().getParameters().put('specStyle', 'abc');
        System.currentPageReference().getParameters().put('lengthTolerance', '');
        System.currentPageReference().getParameters().put('depthTolerance', '');
        System.currentPageReference().getParameters().put('widthTolerance', '');
        System.currentPageReference().getParameters().put('specVolume', 'false');
        
        ssc.runSearch();
        System.assertEquals(0, ssc.specList.size());
        
        Test.setCurrentPageReference(new PageReference('/apex/SpecificationSearch'));
        //System.currentPageReference().getParameters().put('internalName', 'Test');
        System.currentPageReference().getParameters().put('recordTypeName', 'testType');
        System.currentPageReference().getParameters().put('specLength', '2');
        System.currentPageReference().getParameters().put('specWidth', '2');
        System.currentPageReference().getParameters().put('specDepth', '2');
        System.currentPageReference().getParameters().put('specStyle', 'abc');
        System.currentPageReference().getParameters().put('lengthTolerance', '1');
        System.currentPageReference().getParameters().put('depthTolerance', '1');
        System.currentPageReference().getParameters().put('widthTolerance', '1');
        System.currentPageReference().getParameters().put('specVolume', 'false');
        
        ssc.runSearch();
        System.assertEquals(0, ssc.specList.size());
        
        //Query will failed
        ssc.soql = 'Select failed From failed';
        ssc.runQuery();
    }
}
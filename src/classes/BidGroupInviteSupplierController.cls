public with sharing class BidGroupInviteSupplierController {
    public  List<Bid_Request__c> lstBidReq;    
    public  List<Bid__c> lstBids;       
    public String msg;
    public pagereference cancelPage;
    public Set<Id> setGrId{get;set;}
    public Set<Id> setSpecIds{get;set;}
    public Set<Id> setVendorIds{get;set;}
    public List<BidGroup__c> lstBidGroup{get;set;}
    public Map<Id, Decimal> mapAccessDuration = new Map<Id, Decimal>();
    public BidGroupInviteSupplierController(ApexPages.StandardSetController controller) {
        controller.setPageSize(100);
        lstBidReq= new List<Bid_Request__c>();
        lstBids = new List<Bid__c>();
        setGrId= new Set<Id>();
        setSpecIds = new Set<Id>();
        setVendorIds = new Set<Id>();
        lstBidGroup = new List<BidGroup__c>();
           
        //cntr = (ApexPages.StandardSetController)controller;
        cancelPage = controller.cancel();                             
             
        for(SObject obj: controller.getSelected()){
            lstBidGroup.add((BidGroup__c)obj);                        
            setGrId.add(obj.id);
        }
        
        //Check if sufficent access available
        Boolean isBidAccessible= Schema.sObjectType.Bid__c.isAccessible() && 
                             Schema.sObjectType.Bid__c.fields.specification__c.isAccessible() && 
                             Schema.sObjectType.Bid__c.fields.Supplier__c.isAccessible() && 
                             Schema.sObjectType.Bid__c.fields.Bid_Request__c.isAccessible() &&
                             Schema.sObjectType.Bid__c.fields.Bid_Status__c.isAccessible();
           
        if(!isBidAccessible){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.BidEdit_Insufficient_Access));
        }                
        
        //create specification & suppliers ids set to count unique specs & suppliers
        for(Bid__c objBid :[select id,Bid_Status__c,Supplier__c,Bid_Request__c,specification__c,Access_Duration__c,
                                    Bid_Request__r.BidGroup__c, recordTypeId
                                    from bid__c where Bid_Request__r.BidGroup__c in:setGrId]){        
            setSpecIds.add(objBid.specification__c);
            setVendorIds.add(objBid.Supplier__c);
            lstBids.add(objBid);
            if(objBid.Access_Duration__c!=null && objBid.Bid_Request__r.BidGroup__c!=null)
                mapAccessDuration.put(objBid.Bid_Request__r.BidGroup__c, objBid.Access_Duration__c);
        }  
        
        System.debug('****'+setSpecIds+'\n'+setVendorIds);                          
    }
    
    public pagereference inviteSupp(){        
        
        //error if not a single group is selected 
        if (setGrId.size()<1) {
            msg= System.Label.BidGroup_Invite_Supplier_Error_Message; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, msg));
            return null;
        } 
        BidTriggerUtility.bidsShare(lstBids);
        
        //Construct Time Stamp        
        Date dtToday = System.now().date();
        string tstamp = dtToday.year()+''+dtToday.month()+''+dtToday.day()+'-'+ // Date YYYYMMDD
                        setSpecIds.size()+'-'+setVendorIds.size()+ //Spec Size
                        '-'+datetime.now().getTime(); //Time stamp
        
        //Bid Group list to update same timstamp for all
        List<BidGroup__c> lstBidGrpToUpdate = new List<BidGroup__c>();         
        //Update Time stamp on Bid Group
        for(BidGroup__c objGrp : lstBidGroup){
            
            Boolean isBidGroupCreateable = Schema.sObjectType.BidGroup__c.isCreateable() && 
            				Schema.sObjectType.BidGroup__c.fields.Access_Duration__c.isCreateable() &&
                    		Schema.sObjectType.BidGroup__c.fields.Exchange_Status__c.isCreateable() &&
                    		Schema.sObjectType.BidGroup__c.fields.Exchange_Start__c.isCreateable() &&
                    		Schema.sObjectType.BidGroup__c.fields.TransactionID__c.isCreateable() &&
                			Schema.sObjectType.BidGroup__c.fields.Access_End__c.isCreateable();
            if(!isBidGroupCreateable){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
                return null;
            }
            
            objGrp.Access_Duration__c = mapAccessDuration.get(objGrp.Id);
            Integer duration = Integer.valueOf(mapAccessDuration.get(objGrp.Id));
            Date dt = Date.today();
            objGrp.Exchange_Status__c = 'Active';
            objGrp.Exchange_Start__c = Date.today();
            objGrp.TransactionID__c = tstamp;
            if(duration!=null){
                dt = dt.addDays(duration);
                objGrp.Access_End__c = dt;
            }            

            lstBidGrpToUpdate.add(objGrp);                                   
        }            
        
        //update bid groups
        if(lstBidGrpToUpdate.size()>0){
            if (Schema.sObjectType.BidGroup__c.isUpdateable()) {
                update lstBidGrpToUpdate;
			}
            else{
                return null;
            }
        }
        //Bid Request list to update same timstamp for all
        List<Bid_Request__c> lstBidReqToUpdate = new List<Bid_Request__c>();  
        Set<Id> bidGrpId = new Set<Id>();

        for(BidGroup__c objGrp : lstBidGrpToUpdate){
            bidGrpId.add(objGrp.Id);
        }

        for(Bid_Request__c bidReq : [Select Id, Exchange_Start__c,Exchange_End__c from Bid_Request__c where BidGroup__c in :bidGrpId ]){
            for(BidGroup__c objGrp : lstBidGrpToUpdate){
                Boolean isBidRequestCreateable = Schema.sObjectType.Bid_Request__c.isCreateable() && 
            				Schema.sObjectType.Bid_Request__c.fields.Id.isCreateable() &&
                    		Schema.sObjectType.Bid_Request__c.fields.Exchange_Status__c.isCreateable() &&
                    		Schema.sObjectType.Bid_Request__c.fields.Exchange_Start__c.isCreateable() &&
                    		Schema.sObjectType.Bid_Request__c.fields.Exchange_End__c.isCreateable();
                if(!isBidRequestCreateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
                    return null;
                }
                
                Bid_Request__c objBidReq = new Bid_Request__c();
                objBidReq.Id = bidReq.Id;
                objBidReq.Exchange_Status__c = 'Active';
                objBidReq.Exchange_Start__c = objGrp.Exchange_Start__c;
                objBidReq.Exchange_End__c = objGrp.Access_End__c;
                lstBidReqToUpdate.add(objBidReq);                
            }
        }       
        //update bid requests
        if(lstBidReqToUpdate.size()>0){
            if (Schema.sObjectType.Bid_Request__c.isUpdateable()) {
                update lstBidReqToUpdate;
			}
            else{
                return null;
            }
        }        
        return cancelPage;
    }
}
@IsTest
public class CreateFieldServiceTest {
    
    private class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			if(request instanceof MetadataService.retrieve_element)
				response.put('response_x', new MetadataService.retrieveResponse_element());
			else if(request instanceof MetadataService.checkDeployStatus_element)
				response.put('response_x', new MetadataService.checkDeployStatusResponse_element());
			else if(request instanceof MetadataService.listMetadata_element)
				response.put('response_x', new MetadataService.listMetadataResponse_element());
			else if(request instanceof MetadataService.checkRetrieveStatus_element)
				response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			else if(request instanceof MetadataService.describeMetadata_element)
				response.put('response_x', new MetadataService.describeMetadataResponse_element());
			else if(request instanceof MetadataService.deploy_element)
				response.put('response_x', new MetadataService.deployResponse_element());
            else if(request instanceof MetadataService.updateMetadata_element)
                response.put('response_x', new MetadataService.updateMetadataResponse_element());
            else if(request instanceof MetadataService.renameMetadata_element)
                response.put('response_x', new MetadataService.renameMetadataResponse_element());
            else if(request instanceof  MetadataService.cancelDeploy_element)
                response.put('response_x', new MetadataService.cancelDeployResponse_element());
            else if(request instanceof  MetadataService.deleteMetadata_element)
                response.put('response_x', new MetadataService.deleteMetadataResponse_element());
            else if(request instanceof  MetadataService.upsertMetadata_element)
                response.put('response_x', new MetadataService.upsertMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element)
                response.put('response_x', new MetadataService.createMetadataResponse_element());
            else if(request instanceof  MetadataService.deployRecentValidation_element)
                response.put('response_x', new MetadataService.deployRecentValidationResponse_element());
            else if(request instanceof MetadataService.describeValueType_element)
                response.put('response_x', new MetadataService.describeValueTypeResponse_element());
            else if(request instanceof MetadataService.checkRetrieveStatus_element)
                response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			return;
		}
	}
    
	@IsTest
    private static void createFieldTest(){
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        MetadataService.CustomField ss = CreateFieldService.createTextField('Text', 'Account', 'textText', 22);
        CreateFieldService.createAutoNumberField('AutoNumber', 'Account', 'testAutoNumber', 0, 'A-{000}');
        CreateFieldService.createCheckboxField('Checkbox', 'Account', 'testCheckBox', 'false');
        CreateFieldService.createDateField('Date', 'Account', 'testDate');
        MetadataService.Layout layout =  CreateFieldService.createLayout('Account', 'testRecord', ss);
        CreateFieldService.createLongTextAreaField('LongTextArea', 'Account', 'testLongText', 222, 2);
        MetadataService.RecordType recordTypeObject =  CreateFieldService.createNewRecordType('Account', 'recordTypeName', 'description');
        CreateFieldService.createNumberField('Number', 'Account', 'testNumber', 1, 1);
        CreateFieldService.createPicklistField('MultiselectPicklist', 'objectName', 'fieldLable', 'fieldPicklistValues', false, 1);
        CreateFieldService.updateFieldLevelSecurity('profileName', true, ss);
        
        MetadataService.Profile profile = CreateFieldService.updateRecordTypePermission('profileName', recordTypeObject, 'recordTypeName');
        CreateFieldService.saveField(ss);
        CreateFieldService.saveFieldLevelSecurity(profile);
        CreateFieldService.saveNewRecordType(recordTypeObject);
        CreateFieldService.saveCreateLayout(layout);
        CreateFieldService.saveAddFieldToLayout(layout);
        //MetadataService.Layout existingLayout = CreateFieldService.addFieldToLayout('objectName', 'layoutName', ss);
        //Not using this service
        System.assert(true);
    }
}
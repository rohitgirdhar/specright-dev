public with sharing class SpecificationSearchController {
    
    // the soql without the order and limit
    public String soql {get;set;}
    // the collection of contacts to display
    public List<Specification__c> specList {get;set;}
    
    private String numberRetrieve = '15';
    
    // the current sort direction. defaults to asc
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }
    
    // the current field to sort by. defaults to spec name
    public String sortField {
        get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
        set;
    }
    
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit ' + numberRetrieve; }
        set;
    }
    
    public String specNameLabel {
        get {
            Schema.DescribeFieldResult specName = Specification__c.Spec_Name__c.getDescribe();
            return specName.getLabel();
        }
        set;
    }
    
    public String specInternalIdLabel {
        get{
            Schema.DescribeFieldResult specInternalId = Specification__c.Name.getDescribe();
            return specInternalId.getLabel();
        }
        set;
    }
    
    public String specRTLabel {
        get{
			Schema.DescribeFieldResult specRT = Specification__c.RecordTypeId.getDescribe();
            String result = specRT.getLabel();
            return result.remove('ID');            
        }
        set;
    }
    
    public String specStatusLabel {
        get{
            Schema.DescribeFieldResult specStatus = Specification__c.Status__c.getDescribe();
            return specStatus.getLabel();
        }
        set;
    }
    
    // init the controller and display some sample data when the page loads
    public SpecificationSearchController() {
        soql = 'Select Id, Supplier__c, Status__c, Spec_Name__c, Master_Specification__c, RecordTypeId, Name' +
            ' From Specification__c ';
        
        //Select Id, s.Supplier__c, s.Status__c, s.Spec_Name__c, s.Master_Specification__c, s.RecordTypeId, s.Name 
        //    From Specification__c s 
        
        runQuery();
    }
    
    // toggles the sorting of query from asc<-->desc
    public void toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        // run the query again
        runQuery();
    }
    
    // runs the actual query
    public void runQuery() {
        
        try {
            specList = Database.query(soql + ' order by ' + String.escapeSingleQuotes(sortField) + ' ' + String.escapeSingleQuotes(sortDir) + ' limit ' + String.escapeSingleQuotes(numberRetrieve));
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops! ' + e));
        }
        
    }
    
    // runs the search with parameters passed via Javascript
    public PageReference runSearch() {
        String internalName = Apexpages.currentPage().getParameters().get('internalName');
        String recordType = Apexpages.currentPage().getParameters().get('recordTypeName');
        String specName = Apexpages.currentPage().getParameters().get('specName');
        String specStatus = Apexpages.currentPage().getParameters().get('specStatus');
        //String masterSpec = Apexpages.currentPage().getParameters().get('masterSpec');
        //String specSupplier = Apexpages.currentPage().getParameters().get('specSupplier');
        /*
        String firstName = Apexpages.currentPage().getParameters().get('firstname');
        String lastName = Apexpages.currentPage().getParameters().get('lastname');
        String accountName = Apexpages.currentPage().getParameters().get('accountName');
        String technology = Apexpages.currentPage().getParameters().get('technology');
        */
        soql = 'Select Id, Supplier__c, Status__c, Spec_Name__c, Master_Specification__c, RecordTypeId, Name From Specification__c where Name != null';
        if(!internalName.equals('')){
            soql += ' and Name LIKE \'%'+ String.escapeSingleQuotes(internalName) + '%\'';
        }
        
        if(!recordType.equals('')){
            soql += ' and RecordTypeId in (SELECT Id FROM RecordType WHERE Name LIKE \'%' + String.escapeSingleQuotes(recordType) + '%\')';
        }
        
        if(!specName.equals('')){
            soql += ' and Spec_Name__c LIKE \'%'+ String.escapeSingleQuotes(specName) + '%\'';
        }
        if(!specStatus.equals('')){
            soql += ' and Status__c In (\'' + String.escapeSingleQuotes(specStatus) + '\')';
        }
        /*
        if(!masterSpec.equals('')){
            soql += ' and Master_Specification__r.Name LIKE \'%' + String.escapeSingleQuotes(masterSpec) + '%\'';
        }
        /*
        if(!specSupplier.equals('')){
            soql += ' and Supplier__r.Name LIKE \'%' + String.escapeSingleQuotes(specSupplier) + '%\'';
        }
        */
/*
if (!lastName.equals(''))
soql += ' and lastname LIKE ''+String.escapeSingleQuotes(lastName)+'%'';
if (!accountName.equals(''))
soql += ' and account.name LIKE ''+String.escapeSingleQuotes(accountName)+'%'';  
if (!technology.equals(''))
soql += ' and interested_technologies__c includes (''+technology+'')';
*/ 
        // run the query again
        runQuery();
        
        return null;
    }
    
    public List<String> specStatusList {
        get{
            if(specStatusList == null) {
                specStatusList = new List<String>();
                Schema.DescribeFieldResult field = Specification__c.Status__c.getDescribe();
                for(Schema.PicklistEntry f : field.getPicklistValues()){
                    specStatusList.add(f.getLabel());
                }
            }
            return specStatusList;
        }
        set;
    }
        // use apex describe to build the picklist values
        /*
    public List<String> technologies {
    get {
    if (technologies == null) {
    
    technologies = new List<String>();
    Schema.DescribeFieldResult field = Contact.interested_technologies__c.getDescribe();
    
    for (Schema.PicklistEntry f : field.getPicklistValues())
    technologies.add(f.getLabel());
    
    }
    return technologies;          
    }
    set;
    }
    */    
}
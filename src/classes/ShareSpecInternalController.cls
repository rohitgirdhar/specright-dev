public with sharing class ShareSpecInternalController {
    public Specification__c specRecord {get; set;}
    public Task taskNew {get; set;}
    public ShareSpecInternalController(ApexPages.StandardController std){
        this.specRecord = (Specification__c)std.getRecord();
        specRecord = [Select id, name
                      From Specification__c
                      Where id =: specRecord.Id];
        taskNew = new Task();
        taskNew.Status = 'In Progress';
    }
    
    public PageReference shareTask(){
        Boolean isTaskCreateable = Schema.sObjectType.Task.isCreateable() &&
            Schema.sObjectType.Task.fields.OwnerId.isCreateable() &&
            Schema.sObjectType.Task.fields.Status.isCreateable() &&
            Schema.sObjectType.Task.fields.Subject.isCreateable() &&
            Schema.sObjectType.Task.fields.Priority.isCreateable() &&
            Schema.sObjectType.Task.fields.Description.isCreateable() &&
            Schema.sObjectType.Task.fields.Specification_URL__c.isCreateable();
        if(!isTaskCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
            return null;
        }
        Task ts = new Task();
        ts.OwnerId = taskNew.OwnerId;
        ts.Status = taskNew.Status;
        ts.Subject = 'Check '+ specRecord.Name + ' from ' + System.UserInfo.getName();
        ts.Priority = taskNew.Priority;
        ts.Description = taskNew.Description;
        ts.Specification_URL__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + specRecord.Id;
        if(Schema.sObjectType.Task.isCreateable()){
            insert ts;
        }
        PageReference pg = new PageReference('/' + specRecord.Id);
        pg.setRedirect(true);
        return pg;
        
    }
}
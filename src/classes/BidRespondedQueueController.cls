public with sharing class BidRespondedQueueController{    
    public  List<Bid__c> lstBidToUpdate{get;set;}      
    public pagereference cancelPage;
    //to check validation
    public boolean confirm{get;set;}
    public String accesstype {get;set;}
    public BidRespondedQueueController(ApexPages.StandardSetController controller) {
        controller.setPageSize(100);
        confirm=true;
        accesstype ='';
        lstBidToUpdate= new List<Bid__c>();
        Set<Id> setBidIds = new Set<Id>();
        
        cancelPage = controller.cancel();
        
       //Get all selected bid list  
       for(sobject sobj : controller.getSelected()){
           setBidIds.add(sobj.id);
       }     
      Map<Id,String> mapRT = new Map<Id, String>(); 
      List<RecordType> lstRT = [Select Id, Name from RecordType where SObjectType = :'specright__Bid__c'];
      if(lstRt.size() > 0) {
          for(RecordType rt :lstRt) {
            mapRT.put(rt.Id, rt.Name); 
          }
      }
      System.debug(mapRT);
       //Query selected bid list   
       lstBidToUpdate= [select Bid_Amount__c,ownerId,Bid_Delivery_Date__c,Bid_Due_Date__c,Supplier__c,Specification__c,Declined_Bid__c,
                       Access_End__c,Client_Award_Notes__c,Client_Bid_Notes__c,Bid_Status__c,Unit_Type__c,Supplier_Bid_Notes__c,
                       Quanity_per_Unit__c,Verified__c,RecordTypeId
                       from Bid__c where id in :setBidIds];        
       
       //Error if not even one bid is selected
       if (lstBidToUpdate.size()<1) {
            
            //msg= 'Please select atleast 1 bid'; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.BidSubmit_Select_bids));                
       }   
              
        //Validation for blank fields in records
        for(Bid__c bid : lstBidToUpdate){  
          //Check if any field is blank   
          /*if(mapRT.get(bid.RecordTypeId) == 'Bid') {
            accesstype = 'bid';
            if(!bid.Declined_Bid__c && (bid.Bid_Amount__c == null || bid.Unit_Type__c == null || 
                      bid.Quanity_per_Unit__c == null || bid.Supplier_Bid_Notes__c == null)){
              confirm=false;                   
            } 
          }
          if(mapRT.get(bid.RecordTypeId) == 'Push Audit') {
            accesstype = 'pu';
            if(bid.Verified__c == null || bid.Verified__c == 'None'){
              confirm=false;                   
            } 
          } */                                             
                                       
        }
        
    }
    
    //Update Bids as Responded
     public PageReference updateStatus() {                                                                      
            String accessRTId ='';
            accessRTId = [Select Id, Name from RecordType where SObjectType = :'specright__Bid__c' and Name = :'Push Audit'].Id;
            Map<id,EmailStaging__c> mapEmailStagToInsert = new Map<Id,EmailStaging__c>();
            //iterate through all selected bids            
            for(Bid__c objBid: lstBidToUpdate){
                
                //Update Bid Status to Submitted
                objBid.Bid_Status__c = 'Responded';  
                if(accessRTId != '' && objBid.RecordTypeId == accessRTId) {
                  //create email staging
                  EmailStaging__c objEmailStag = new EmailStaging__c(User__c=objBid.ownerId,type__c='Push Audit Response');   
                  System.debug('*****'+objBid.ownerId);
                  mapEmailStagToInsert.put(objBid.ownerId,objEmailStag);  
                }                              
            }                             
            
            if(lstBidToUpdate.size()>0){
            
                //Check if sufficent access available              
                Boolean isBidUpdateable= Schema.sObjectType.Bid__c.isUpdateable();                                 
               
                if(!isBidUpdateable){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.label.BidEdit_Insufficient_Access));
                    return null;
                }             
                
                //update bids
                update lstBidToUpdate;
                //insert email staging        
                if(mapEmailStagToInsert.size()>0){
                  insert mapEmailStagToInsert.values(); 
                }                
           }                        
            
            return cancelPage;
     }
}
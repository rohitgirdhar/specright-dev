public with sharing class BidGroupHandler {
    public static void updateRecordtype(List<BidGroup__c> lstBidGroup) {

        Set<Id> bidGrpId = new Set<Id>();
        for(BidGroup__c objGrp : lstBidGroup){
            bidGrpId.add(objGrp.Id);
        }

        List<Bid_Request__c> lstBidReq = [Select Id, Notes__c, RecordTypeId from Bid_Request__c where BidGroup__c in :bidGrpId ];

        Set<Id> bidReqId = new Set<Id>();
        for(Bid_Request__c b : lstBidReq){
            bidReqId.add(b.Id);
        }
        List<Bid__c> lstBid = [Select Id, Notes__c, RecordTypeId from Bid__c where Bid_Request__c in :bidReqId ];
        List<Bid_Request__c> lstBidReqToUpdate = new List<Bid_Request__c>();
        List<Bid__c> lstBidsToUpdate = new List<Bid__c>();
        
        for(BidGroup__c bidGrp : lstBidGroup){
            if(lstBidReq.size()>0){
                for(Bid_Request__c bidReq : lstBidReq){
                    
                    if(bidReq.RecordTypeId != bidGrp.RecordTypeId){
                        
                        Boolean isBidRequestCreateable = Schema.sObjectType.Bid_Request__c.isCreateable() && 
            				Schema.sObjectType.Bid_Request__c.fields.Id.isCreateable() &&
                            Schema.sObjectType.Bid_Request__c.fields.RecordTypeId.isCreateable();
                        if(!isBidRequestCreateable){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
                			return;
                        }
                        
                        Bid_Request__c b = new Bid_Request__c();
                        b.Id = bidReq.Id;
                        b.RecordTypeId = bidGrp.RecordTypeId;
                        //b.Notes__c = bidGrp.Notes__c;
                        lstBidReqToUpdate.add(b);
                    }
                }
            }
            if(lstBid.size()>0){
                for(Bid__c bidObj : lstBid){
                    if(bidObj.RecordTypeId != bidGrp.RecordTypeId){
                        
                        Boolean isBidCreateable = Schema.sObjectType.Bid__c.isCreateable() && 
            				Schema.sObjectType.Bid__c.fields.Id.isCreateable() &&
                            Schema.sObjectType.Bid__c.fields.RecordTypeId.isCreateable();
                        if(!isBidCreateable){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access')); //Insufficient access
                			return;
                        }
                        
                        Bid__c b = new Bid__c();
                        b.Id = bidObj.Id;
                        b.RecordTypeId = bidGrp.RecordTypeId;
                        //b.Notes__c = bidGrp.Notes__c;
                        lstBidsToUpdate.add(b);
                    }
                }
            }
        }

        if(lstBidReqToUpdate.size()>0){
            if (Schema.sObjectType.Bid_Request__c.isUpdateable()) {
                update lstBidReqToUpdate;
			}
            else{
                return;
            }
        }

        if(lstBidsToUpdate.size()>0){
            if (Schema.sObjectType.Bid__c.isUpdateable()) {
                update lstBidsToUpdate;
			}
            else{
                return;
            }
        }         
    }
}
@isTest
public with sharing class BidGroupInviteSupplierControllerTest {
    static testMethod void myUnitTest() {
        BidGroup__c bidgroup = new BidGroup__c();
        bidgroup.TransactionID__c='20141219-5-2-1418980511146';
        insert bidgroup;

        Specification__c spec = new Specification__c();
        spec.Name = 'TestSpecification';
        spec.Spec_Name__c = 'TestSpec';
        spec.HQspecId__c = 'HQ001';
        spec.Status__c = 'Hold';
        insert spec;

        Bid_Request__c bidReq = new Bid_Request__c();
        bidReq.BidGroup__c = bidgroup.Id;
        bidReq.Name = 'TestBidRequest';
        bidReq.Specification__c = spec.Id;
        insert bidReq;
        
        List<BidGroup__c> lis = [Select TransactionID__c from BidGroup__c limit 2];
        ApexPages.StandardSetController controller =  new ApexPages.StandardSetController(lis);
        controller.setPageSize(100);
        controller.setSelected(lis);
        
        Set<Id> setBGIds = new Set<Id>();
        for(BidGroup__c bg:lis){
            setBGIds.add(bg.id);
        }
        
        system.debug('<<>>'+controller.getSelected());
        BidGroupInviteSupplierController Bidgroups = new BidGroupInviteSupplierController(controller);
        //Bidgroups.lstBidReq= new List<Bid_Request__c>();
        //Bidgroups.lstBidGroup=lis;
        system.assertequals(lis,Bidgroups.lstBidGroup);
                
        Bid__c bid = new Bid__c();        
        bid.Bid_Status__c = 'Waiting for Response';
        bid.Client_Award_Notes__c = 'Test';
        bid.Client_Bid_Notes__c= 'test1';
        
        /**
         * RA, 08/18 - Record Type is needed in the new model
         */
        System.debug('Here: ' + Bid__c.getSObjectType());
        String bidObjectTypeUnmanged = Bid__c.getSObjectType()+'';
        bidObjectTypeUnmanged = bidObjectTypeUnmanged.remove('specright__');
        List<RecordType> lstRT = [Select id from RecordType where sObjectType=:(bidObjectTypeUnmanged) and name='Bid'];
        if(lstRT.size() > 0)
            bid.recordTypeId = lstRT[0].id;
        insert bid; 
        
        List<Bid__c > lstBid = [Select id, Specification__c, 
            Bid_Status__c, Bid_Request__c, Supplier__c, recordTypeId
            from Bid__c where Bid_request__r.BidGroup__c in :setBGIds];
        Bidgroups.lstBids=lstBid;
        Bidgroups.inviteSupp();
        
    }
}
/**
 * Controller class to execute on Client/Subscriber Org while package install/upgrade.
 * @author - Wisdomedge Inc.
 */

global without sharing class SpecRightPostInstallScript implements InstallHandler {
    
    global void onInstall(InstallContext ctx) {
        
        /**
         * RA, 08/18/2017
         * Deprecated from v1.33 onwards
         */
    }

    public static void updateOrgWideSetting() {
        /**
         * RA, 08/18/2017
         * Deprecated from v1.33 onwards
         */   
    }
}
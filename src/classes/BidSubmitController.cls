public with sharing class BidSubmitController{
    
  //private final ApexPages.StandardSetController cntr;
  //private final PageReference fromPage;
  public  List<Bid__c> lstBidToUpdate{get;set;}   
  public String msg;
  public pagereference cancelPage;
  public boolean confirm{get;set;}

  public BidSubmitController(ApexPages.StandardSetController controller) {
    Map<Id,String> mapRT = new Map<Id, String>();
    List<RecordType> lstRT = [Select Id, Name from RecordType where SObjectType = :'Bid__c'];
    if(lstRt.size() > 0) {
      for(RecordType rt :lstRt) {
        mapRT.put(rt.Id, rt.Name); 
      }
    }

    controller.setPageSize(100);
    confirm=true;
    lstBidToUpdate= new List<Bid__c>();
    Set<Id> setBidIds = new Set<Id>();

    //cntr = (ApexPages.StandardSetController)controller;
    cancelPage = controller.cancel();

    //Get all selected bid list  
    for(sobject sobj : controller.getSelected()){
      setBidIds.add(sobj.id);
    }        
    lstBidToUpdate= [select Bid_Amount__c,Bid_Delivery_Date__c,Bid_Due_Date__c,Supplier__c,Specification__c,Supplier_Bid_Notes__c,
                          Access_End__c,Client_Award_Notes__c,Client_Bid_Notes__c,Bid_Status__c,ownerId,Unit_Type__c,Declined_Bid__c,  
                          Quanity_per_Unit__c,RecordTypeId from Bid__c where id in :setBidIds];       

    //Error if not even one bid is selected
    if (lstBidToUpdate.size()<1) {
      //msg= 'Please select atleast 1 bid for Submitting'; 
      msg=System.Label.BidSubmit_Select_bids;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, msg));                
    } 

    //Validation for blank fields in records
    for(Bid__c bid : lstBidToUpdate){  
      //check if current status was Responded  
      if(bid.bid_status__c!='Responded'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.BidSubmit_Please_Queue_Response));
        lstBidToUpdate=new List<Bid__c>();
      break;
      }   
    }
    //Push audit records can not be submitted
    for(Bid__c bid : lstBidToUpdate) {
      if(mapRT.get(bid.RecordTypeId) == 'Push Audit'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.BidSubmit_Push_Audit_Submit));
        lstBidToUpdate=new List<Bid__c>();
        break;
      }
      //Check if any field is blank   
      /*else if(mapRT.get(bid.RecordTypeId) == 'Bid') {
        if(!bid.Declined_Bid__c && (bid.Bid_Amount__c == null || bid.Unit_Type__c == null || 
          bid.Quanity_per_Unit__c == null || bid.Supplier_Bid_Notes__c == null)){
          confirm=false;                   
        } 
      }*/ 
    }   

  }

  public PageReference updateStatus() {                                              
    Map<id,EmailStaging__c> mapEmailStagToInsert = new Map<Id,EmailStaging__c>();

    for(Bid__c objBid: lstBidToUpdate){

      //Update Bid Status to Submitted
      objBid.Bid_Status__c = 'Submitted';

      //create email staging
      EmailStaging__c objEmailStag = new EmailStaging__c(User__c=objBid.ownerId,type__c='Bid Award Request');   
      System.debug('*****'+objBid.ownerId);
      mapEmailStagToInsert.put(objBid.ownerId,objEmailStag); 
    }

    if(lstBidToUpdate.size()>0){
      //Check if sufficent access available                
      Boolean isBidUpdateable= Schema.sObjectType.Bid__c.isUpdateable();

      if(!isBidUpdateable){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access to Read Access.'));
        return null;
      }

      //update bids
      update lstBidToUpdate;

      //insert email staging        
      if(mapEmailStagToInsert.size()>0){
        insert mapEmailStagToInsert.values(); 
      }
    }
    //navigate to previous page
    return cancelPage;
  }
    
}
/**
 * Version History
 *
 * RA, 06/07/2017: Modified to accommodate PROD-77
 */
public with sharing class SpecAnalyzerController {
    
    // the soql without the order and limit
    public String soql;
    // the collection of contacts to display
    public List<Specification__c> specList;
    
    public String specFamilyName {get; set;}
    
    // the variables are converted to private to omit governor limits including Viewstate issue
    private Integer numberRetrieve;
    private String globalLength;
    private String globalWidth;
    private String globalDepth;
    public Map<String,List<Specification__c>> mapRecordTypeToListOfRecords{get;set;}
    public Map<String,List<String>> recordTypeAndColumns{get;set;}
    public Map<String,String> fieldApiNameToLabel{get;set;}
    private List<String> defaultSetOfColumns;
    public String recType{get; set;}
    public String SpecRecordType{get;set;}
    private List<Specification__c> lstSpec;
    public Map<String,Boolean> recordTypeHaveMoreRecords{get;set;}
    private specright__SpecrightSettings__c specSetting;
    
    public PageReference saveToSpecFamilyLink{get; set;}
    
    // the current sort direction. defaults to asc
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }
    
    // the current field to sort by. defaults to spec name
    public String sortField {
        get  { if (sortField == null) {sortField = 'RecordType.Name'; } return sortField;  }
        set;
    }
    
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return soql + ' order by ' + sortField + ' ' + sortDir; }
        set;
    }
    
    public String specInternalIdLabel {
        get{
            Schema.DescribeFieldResult specInternalId = Specification__c.Name.getDescribe();
            return specInternalId.getLabel();
        }
        set;
    }
    
    public String specRTLabel {
        get{
			Schema.DescribeFieldResult specRT = Specification__c.RecordTypeId.getDescribe();
            String result = specRT.getLabel();
            return result.remove('ID');            
        }
        set;
    }
    
    public String specSpecNameLabel{
        get{
            Schema.DescribeFieldResult specName = Specification__c.Spec_Name__c.getDescribe();
            return specName.getLabel();
        }
        set;
    }
    
    public String specLengthLabel {
        get{
            Schema.DescribeFieldResult length = Specification__c.Length__c.getDescribe();
            return length.getLabel();
        }
        set;
    }
    
    public String specDepthLabel {
        get{
            Schema.DescribeFieldResult depth = Specification__c.Depth__c.getDescribe();
            return depth.getLabel();
        }
        set;
    }
    
    public String specWidthLabel {
        get{
            Schema.DescribeFieldResult width = Specification__c.Width__c.getDescribe();
            return width.getLabel();
        }
        set;
    }
    
    public String specStyleLabel {
        get{
            Schema.DescribeFieldResult style = Specification__c.Style__c.getDescribe();
            return style.getLabel();
        }
        set;
    }
    
    public String specFluteLabel{
        get{
            Schema.DescribeFieldResult flute = Specification__c.Flute__c.getDescribe();
            return flute.getLabel();
        }
        set;
    }
    
    public String specSubstrateLabel{
        get{
            Schema.DescribeFieldResult substrate = Specification__c.Substrate1__c.getDescribe();
            return substrate.getLabel();
        }
        set;
    }
    
    public String specStatusLabel{
        get{
            Schema.DescribeFieldResult status = Specification__c.Status__c.getDescribe();
            return status.getLabel();
        }
        set;
    }
    
    public String specWeightGradeLabel{
        get{
            Schema.DescribeFieldResult weightGrade = Specification__c.Weight_Grade__c.getDescribe();
            return weightGrade.getLabel();
        }
        set;
    }
    
    public String specMaterialComboLabel{
        get{
            Schema.DescribeFieldResult materialCombo = Specification__c.Material_Combination__c.getDescribe();
            return materialCombo.getLabel();
        }
        set;
    }
    
    // init the controller and display some sample data when the page loads
    public SpecAnalyzerController() {
        /**
         * RA, 06/07/2017
         * modified to read the column spec from a custom meta data type
         */
        List<specright__Spec_Finder_Settings__mdt> columnSpecifications;
        Map<String,String> fieldlabelToApiName;

        specSetting = specright__SpecrightSettings__c.getInstance('Spec Finder Record Limit');
        if(specSetting != null) {
            numberRetrieve = Integer.valueOf(specSetting.Value__c);
        }
        else {
            numberRetrieve = 25;
        }
        
        recordTypeAndColumns = new Map<String,List<String>>();
        /**
         * first initialize the colomnspec to read for default set of columns
         * based on the developername = Master
         */
        columnSpecifications =  [Select MasterLabel,DeveloperName,specright__Value__c 
            from specright__Spec_Finder_Settings__mdt where DeveloperName = 'Master'];
        
        if(columnSpecifications.size() > 0) {
            defaultSetOfColumns = columnSpecifications[0].specright__Value__c.split(',');
        }
        else {
            defaultSetOfColumns = Label.Default_Item_Finder_Columns.split(',');
        }
        
        fieldlabelToApiName = new Map<String,String>();
        fieldApiNameToLabel = new Map<String,String>{'Name'=>'Name','specright__Spec_Name__c'=>'Spec Name','specright__Width__c'=>'Width',
                                                    'specright__Depth__c'=>'Depth','specright__Style__c'=>'Style','specright__Substrate1__c'=>'Substrate',
                                                    'specright__Status__c'=>'Status','specright__Length__c'=>'Length'
        };
        
        Map<String, Schema.SObjectField> fieldMap = 
            Schema.getGlobalDescribe().get('specright__Specification__c').getDescribe().fields.getMap();
            
        for(String s:fieldMap.keySet()){
            SObjectField fieldToken = fieldMap.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            if(selectedField.IsAccessible()){
                String apiName = selectedField.getName();
                String LabelName = selectedField.getLabel();
                fieldlabelToApiName.put(LabelName,apiName);
                fieldApiNameToLabel.put(apiName,LabelName);
            }
                
        }
        columnSpecifications =  [Select MasterLabel,DeveloperName,specright__Value__c 
            from specright__Spec_Finder_Settings__mdt where DeveloperName != 'Master'];
        
        if(columnSpecifications != null){
            for(specright__Spec_Finder_Settings__mdt setting : columnSpecifications){
                List<String> columns = new List<String>();
                for(String str : setting.specright__Value__c.Split(',')){
                    if(fieldApiNameToLabel.keyset().contains(str)){
                        columns.add(str);
                    }
                    
                }
                
                recordTypeAndColumns.put(setting.DeveloperName,columns);
                
            }
        }
        soql = 'Select Id, Supplier__c, Master_Specification__c, RecordTypeId, RecordType.Name, Spec_Name__c, Substrate1__c, Status__c, Name, Length__c, Depth__c, Width__c, Style__c, Volume__c, Flute__c, Weight_Grade__c, Material_Combination__c' +
            ' From Specification__c ';
        
        //Select Id, s.Supplier__c, s.Status__c, s.Spec_Name__c, s.Master_Specification__c, s.RecordTypeId, s.Name 
        //    From Specification__c s 
        
        runQuery();
    }
    
    // toggles the sorting of query from asc<-->desc
    public void toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        // run the query again
        if(recType != null && !soql.contains('Where')){
            lstSpec = mapRecordTypeToListOfRecords.get(recType);
            
            if(lstSpec != null){
                Set<Id> specIds = new Set<Id>();
                for(Specification__c sp : lstSpec){
                    specIds.add(sp.Id);
                }
                lstSpec = new List<Specification__c>();
                lstSpec = Database.query(soql  +' where Id In : specIds'+' order by ' + sortField + ' ' + sortDir);
                
                mapRecordTypeToListOfRecords.put(recType,lstSpec);
            }
            
        }
        else{
            runQuery();
        }
        
    }
    
    // runs the actual query
    public void runQuery() {
        
        mapRecordTypeToListOfRecords = new Map<String,List<Specification__c>>();
        recordTypeHaveMoreRecords = new Map<String,Boolean>();
        
        Set<String> recordTypes = new Set<String>();
        try {
            
            specList = Database.query(soql  +' order by ' + String.escapeSingleQuotes(sortField) + ' ' + String.escapeSingleQuotes(sortDir));
            if(specList.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.warning, Label.No_Specifiaction_Found));
            }
            else{
                for(Specification__c sp : specList){
                    List<Specification__c> lstSpec = mapRecordTypeToListOfRecords.get(sp.RecordType.Name);
                    if(lstSpec == null){
                        lstSpec = new List<Specification__c>();
                    }
                    if(numberRetrieve>lstSpec.size()){
                        lstSpec.add(sp);
                        mapRecordTypeToListOfRecords.put(sp.RecordType.Name,lstSpec);
                        recordTypeHaveMoreRecords.put(sp.RecordType.Name,false);
                    }   
                    else{
                        recordTypeHaveMoreRecords.put(sp.RecordType.Name,true);
                    }
                    recordTypes.add(sp.RecordType.Name);
                }
                for(String RTname : recordTypes){
                    if(!recordTypeAndColumns.keySet().contains(RTname)){
                        recordTypeAndColumns.put(RTname,defaultSetOfColumns);
                    }
                }
            }
            
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops! ' + e));
        }
        
    }

    /**
     * Handler for View more records function
     */
    public void viewMoreRecords(){
        if(SpecRecordType != null){
            List<Specification__c> lstAppSpec = new List<Specification__c>();
            List<Specification__c> lstSpec = mapRecordTypeToListOfRecords.get(SpecRecordType);
            lstAppSpec.addAll(lstSpec);

            /**
             * considering the where clause is being added dynamically after the initial page state
             * is loaded, check if the clause is already present
             */
            if(soql.containsIgnoreCase('where')){
                
                /**
                 * add limit dynamically based on number that has been retrieved
                 */
                if(!soql.containsIgnoreCase('limit '+ numberRetrieve)){
                    soql = soql + ' and Id Not In : lstSpec and recordType.Name =: SpecRecordType limit '+numberRetrieve;
                }
            }
            /**
             * if where clause is not present in the soql being executed.
             */
            else{
                soql = soql  +' where Id Not In : lstSpec and recordType.Name =: SpecRecordType limit '+numberRetrieve;
            }
            lstSpec = Database.query(soql);
            lstAppSpec.addAll(lstSpec);
            mapRecordTypeToListOfRecords.put(SpecRecordType,lstAppSpec);
        }
    }
    public Map<String, String> toleranceCalc(String value, String tolerance){
        Map<String, String> toleranceResult = new Map<String, String>();
        Decimal valueInt = Decimal.valueOf(value);
        Decimal toleranceInt = Decimal.valueOf(tolerance);
        Decimal up = valueInt + toleranceInt;
        Decimal low = valueInt - toleranceInt;
        toleranceResult.put('high', String.valueOf(up));
        toleranceResult.put('low', String.valueOf(low));
        return toleranceResult;
    }
    
    public Map<String, String> noTolerance(String value){
        Map<String, String> noToleranceResult = new Map<String, String>();
        noToleranceResult.put('high', value);
        noToleranceResult.put('low', value);
        return noToleranceResult;
    }
    
    public Decimal cubeCalc(String lengthC, String widthC, String depthC){
        return Decimal.valueOf(lengthC) * Decimal.valueOf(widthC) * Decimal.valueOf(depthC);
    }
    
    // runs the search with parameters passed via Javascript
    public PageReference runSearch() {
        //String internalName = Apexpages.currentPage().getParameters().get('internalName');
        String recordType = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('recordTypeName'));
        String specLength = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('specLength'));
        String specDepth = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('specDepth'));
        String specWidth = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('specWidth'));
        String specStyle = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('specStyle'));
        String lengthTolerance = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('lengthTolerance'));
        String depthTolerance = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('depthTolerance'));
        String widthTolerance = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('widthTolerance'));
        String specVolume = string.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('specVolume'));
        //String specName = Apexpages.currentPage().getParameters().get('specName');
        //String specStatus = Apexpages.currentPage().getParameters().get('specStatus');
        //String masterSpec = Apexpages.currentPage().getParameters().get('masterSpec');
        //String specSupplier = Apexpages.currentPage().getParameters().get('specSupplier');
        
        //Set GlobalLWD
        globalLength = specLength;
        globalWidth = specWidth;
        globalDepth = specDepth;
        
        soql = 'Select Id, Supplier__c, Status__c, RecordType.Name, Spec_Name__c, Substrate1__c, Master_Specification__c, RecordTypeId, Name, Length__c, Depth__c, Width__c, Style__c, Volume__c, Flute__c, Weight_Grade__c, Material_Combination__c ' + 
            	'From Specification__c Where Name != null';
        /*
        if(!internalName.equals('')){
            soql += ' and Name LIKE \'%'+ String.escapeSingleQuotes(internalName) + '%\'';
        }
        */
        
        if(!recordType.equals('')){
            soql += ' and RecordTypeId in (SELECT Id FROM RecordType WHERE Name LIKE \'%' + String.escapeSingleQuotes(recordType) + '%\')';
        }
        Boolean isVolume = Boolean.valueOf(specVolume);
        if(isVolume && !specLength.equals('') && !specDepth.equals('') && !specWidth.equals('')){
            Map<String, String> lengthToleranceValue;
            Map<String, String> widthToleranceValue;
            Map<String, String> depthToleranceValue;
            if(!lengthTolerance.equals('')){
                lengthToleranceValue = toleranceCalc(specLength, lengthTolerance);
            }
            else{
                lengthToleranceValue = noTolerance(specLength);
            }
            
            if(!widthTolerance.equals('')){
                widthToleranceValue = toleranceCalc(specWidth, widthTolerance);
            }
            else{
                widthToleranceValue = noTolerance(specWidth);
            }
            
            if(!depthTolerance.equals('')){
                depthToleranceValue = toleranceCalc(specDepth, depthTolerance);
            }
            else{
                depthToleranceValue = noTolerance(specDepth);
            }
            
            Decimal cubeLowValue = cubeCalc(lengthToleranceValue.get('low'), widthToleranceValue.get('low'), depthToleranceValue.get('low'));
            Decimal cubeHighValue = cubeCalc(lengthToleranceValue.get('high'), widthToleranceValue.get('high'), depthToleranceValue.get('high'));
            soql += ' and Volume__c >=' + cubeLowValue + ' and Volume__c <=' + cubeHighValue;
        }
        else{
            if(!specLength.equals('')){
                if(!lengthTolerance.equals('')){
                    Map<String, String> toleranceValue = toleranceCalc(specLength, lengthTolerance);
                    soql += ' and Length__c >=' + toleranceValue.get('low') + ' and Length__c <=' + toleranceValue.get('high');
                }
                else{
                    soql += ' and Length__c =' + specLength;
                }
            }
            
            if(!specDepth.equals('')){
                if(!depthTolerance.equals('')){
                    Map<String, String> toleranceValue = toleranceCalc(specDepth, depthTolerance);
                    soql += ' and Depth__c >=' + toleranceValue.get('low') + ' and Depth__c <=' + toleranceValue.get('high');
                }
                else{
                    soql += ' and Depth__c =' + specDepth;
                }
                
            }
            
            if(!specWidth.equals('')){
                if(!widthTolerance.equals('')){
                    Map<String, String> toleranceValue = toleranceCalc(specWidth, widthTolerance);
                    soql += ' and Width__c >=' + toleranceValue.get('low') + ' and Width__c <=' + toleranceValue.get('high');
                }
                else{
                    soql += ' and Width__c =' + specWidth;
                }
            }
        }
        
        
        
        if(!specStyle.equals('')){
            soql += ' and Style__c LIKE \'%'+ String.escapeSingleQuotes(specStyle) + '%\'';
        }
        /*
        if(!specName.equals('')){
            soql += ' and Spec_Name__c LIKE \'%'+ String.escapeSingleQuotes(specName) + '%\'';
        }
        if(!specStatus.equals('')){
            soql += ' and Status__c In (\'' + specStatus + '\')';
        }
		*/
        /*
        if(!masterSpec.equals('')){
            soql += ' and Master_Specification__r.Name LIKE \'%' + String.escapeSingleQuotes(masterSpec) + '%\'';
        }
        /*
        if(!specSupplier.equals('')){
            soql += ' and Supplier__r.Name LIKE \'%' + String.escapeSingleQuotes(specSupplier) + '%\'';
        }
        */
        
        // run the query again
        runQuery();
        
        return null;
    }
    
    public PageReference saveToSpecFamily(){
        if(specList.isEmpty()){
            return null;
        }
        
        Boolean isSpecificationCreateable = Schema.SObjectType.Spec_Family_Connection__c.isCreateable() &&
            							Schema.SObjectType.Spec_Family_Connection__c.fields.Spec_Family__c.isCreateable() &&
            							Schema.SObjectType.Spec_Family_Connection__c.fields.Specification__c.isCreateable();
        if(!isSpecificationCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
            return null;
        }
        
        Spec_Family__c newSpecFamily = new Spec_Family__c();
        if(specFamilyName == '' || specFamilyName == null){
            newSpecFamily.Name = String.valueOfGmt(System.Datetime.now()) + '-' + globalLength + 'x' + globalWidth + 'x' + globalDepth;
        }
        else{
            newSpecFamily.Name = specFamilyName;
        }
        //System.debug('Here: ' + Schema.SObjectType.specright__Spec_Family__c.getRecordTypeInfosByName().get('Spec Family').getRecordTypeId());
        newSpecFamily.RecordTypeId = Schema.SObjectType.Spec_Family__c.getRecordTypeInfosByName().get('Spec Family').getRecordTypeId();
        if(Schema.sObjectType.Spec_Family__c.isCreateable()){
            insert newSpecFamily;
        }
        System.debug('After SF Name:' + newSpecFamily.Name);
        
        List<Spec_Family_Connection__c> specFamilyConnectionList = new List<Spec_Family_Connection__c>();
        for(Specification__c spec : specList){
            Spec_Family_Connection__c specFC = new Spec_Family_Connection__c();
            specFC.Spec_Family__c = newSpecFamily.Id;
            specFC.Specification__c = spec.Id;
            specFamilyConnectionList.add(specFC);
        }
        if(Schema.sObjectType.Spec_Family_Connection__c.isCreateable()){
            insert specFamilyConnectionList;
        }
        PageReference pg = new PageReference('/' + newSpecFamily.Id);
        saveToSpecFamilyLink = pg;
        //pg.setRedirect(true);
        return null;
    }
    /*
    public List<String> specStatusList {
        get{
            if(specStatusList == null) {
                specStatusList = new List<String>();
                Schema.DescribeFieldResult field = Specification__c.Status__c.getDescribe();
                for(Schema.PicklistEntry f : field.getPicklistValues()){
                    specStatusList.add(f.getLabel());
                }
            }
            return specStatusList;
        }
        set;
    }
	*/
        // use apex describe to build the picklist values
        /*
    public List<String> technologies {
    get {
    if (technologies == null) {
    
    technologies = new List<String>();
    Schema.DescribeFieldResult field = Contact.interested_technologies__c.getDescribe();
    
    for (Schema.PicklistEntry f : field.getPicklistValues())
    technologies.add(f.getLabel());
    
    }
    return technologies;          
    }
    set;
    }
    */
}
@isTest
public class ShareSpecAndSpecFInternalTest {
    public static testMethod void ShareSpecTest() {
        Specification__c testSpec = new Specification__c(
            Name = 'testSpec'
        );
        insert testSpec;
        
        ShareSpecInternalController shareSpecController = new ShareSpecInternalController(new ApexPages.StandardController(testSpec));
        shareSpecController.taskNew.OwnerId = System.UserInfo.getUserId();
        shareSpecController.taskNew.Priority = 'Normal';
        PageReference backToSpec = shareSpecController.shareTask();
        
        System.assertEquals('/' + testSpec.Id, backToSpec.getUrl());
    }
    
    public static testMethod void ShareSpecFTest() {
        Spec_Family__c testSpec = new Spec_Family__c(
            Name = 'testSpec'
        );
        insert testSpec;
        
        ShareSpecFInternalController shareSpecController = new ShareSpecFInternalController(new ApexPages.StandardController(testSpec));
        shareSpecController.taskNew.OwnerId = System.UserInfo.getUserId();
        shareSpecController.taskNew.Priority = 'Normal';
        PageReference backToSpec = shareSpecController.shareTask();
        
        System.assertEquals('/' + testSpec.Id, backToSpec.getUrl());
    }
	
}
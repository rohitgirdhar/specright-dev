public with sharing class AdministrationCtlr {
	
	public PageReference splitSpec() {
		database.executeBatch(new SpecMigration(false));
		return null;
	}

	public PageReference splitSpecOverwrite() {
		database.executeBatch(new SpecMigration(true));
		return null;
	}
}
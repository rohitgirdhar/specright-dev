@isTest
public class AttachSpecFileDocumentTest {
    public static testMethod void AttachFileDocumentTriggerTest(){
        //Create new Spec
        Id CorrugatedRTId = Schema.SObjectType.Specification__c.getRecordTypeInfosByName().get('Corrugated').getRecordTypeId();
        Specification__c newSpec = new Specification__c(Name = 'abcde', RecordTypeId = CorrugatedRTId);
        insert newSpec;
        
        Id FlexibleRTId = Schema.SObjectType.Specification__c.getRecordTypeInfosByName().get('Flexible').getRecordTypeId();
        Specification__c newSpecFlex = new Specification__c(Name = 'test 2', RecordTypeId = FlexibleRTId);
        insert newSpecFlex;
        
        //Create new File
        Id DocumentRTId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Document').getRecordTypeId();
        Spec_File__c objSpecFile1 = new Spec_File__c(Standard_File_Type__c = 'Corrugated', RecordTypeId = DocumentRTId);
        insert objSpecFile1;
        
        objSpecFile1.Standard_File_Type__c = 'Corrugated;Flexible';
        update objSpecFile1;
		
        Spec_File__c newObjSpecFile = [Select id, Standard_File_Type__c From Spec_File__c Where id =: objSpecFile1.Id];
        System.assertEquals('Corrugated;Flexible', newObjSpecFile.Standard_File_Type__c);
    }
    
    public static testMethod void AttachSpecDocumentTriggerTest(){
        //Create another File
        Id DocumentRTId = Schema.SObjectType.Spec_File__c.getRecordTypeInfosByName().get('Document').getRecordTypeId();
        Spec_File__c objSpecFile12 = new Spec_File__c(Standard_File_Type__c = 'Corrugated;Flexible', RecordTypeId = DocumentRTId);
        insert objSpecFile12;
        
        Spec_File__c objSpecFile1 = new Spec_File__c(Standard_File_Type__c = 'Corrugated', RecordTypeId = DocumentRTId);
        insert objSpecFile1;
        //Create another Spec
        Id CorrugatedRTId = Schema.SObjectType.Specification__c.getRecordTypeInfosByName().get('Corrugated').getRecordTypeId();
        Specification__c newSpec2 = new Specification__c(Name = 'abcde2', RecordTypeId = CorrugatedRTId);
        insert newSpec2;
        
        newSpec2.Name = 'updateName';
        update newSpec2;
        
        List<Document__c> docDelete = [Select Id From Document__c];
        delete docDelete;
        
        newSpec2.Name = 'updateName2';
        update newSpec2;
        
        Specification__c updatedSpec2 = [Select id, Name From Specification__c Where id =: newSpec2.Id];
        System.assertEquals('updateName2', updatedSpec2.Name);
    }
}
@isTest
public class SpecFilePreviewTest {
    public static TestMethod void filePreviewTest(){
        // Implement test code
        Specification__c spec = new Specification__c();
        spec.Name = 'test spec';
        insert spec;
		
        List<Spec_File__c> addNewFiles = new List<Spec_File__c>();
        Spec_File__c objSpecFile =new Spec_File__c(Spec_File_Name__c='Test File', Specification__c=spec.Id);
        addNewFiles.add(objSpecFile);
        Spec_File__c objSpecFile2 =new Spec_File__c(Spec_File_Name__c='Test File2', Specification__c=spec.Id);
        addNewFiles.add(objSpecFile2);
        Spec_File__c objSpecFile3 =new Spec_File__c(Spec_File_Name__c='Test File3', Specification__c=spec.Id);
        addNewFiles.add(objSpecFile3);
        insert addNewFiles;
        
        Attachment attachmentPDF = new Attachment(Name='Test Attachment.pdf',body=blob.valueof('testbody1'),parentId=objSpecFile.Id);
        insert attachmentPDF;
        
        Attachment attachmentImage = new Attachment(Name='Test Attachment.jpg',body=blob.valueof('testbody2'),parentId=objSpecFile2.Id);
        insert attachmentImage;
        
        Attachment attachmentDoc = new Attachment(Name='Test Attachment.doc',body=blob.valueof('testbody3'),parentId=objSpecFile3.Id);
        insert attachmentDoc;
        
        ApexPages.currentPage().getParameters().put('specId', spec.id);
        SpecFilePreviewController ctlr = new SpecFilePreviewController(new ApexPages.StandardController(spec));
        System.assertEquals(attachmentImage.Id, ctlr.photos.get(0));
        System.assertEquals(attachmentPDF.Id, ctlr.pdfs.get(0));
        System.assertEquals(attachmentDoc.Id, ctlr.docs.get(0));
        System.assertEquals(spec.Id, ctlr.specRecordId);
        System.assertEquals('Test File2', ctlr.photoMap.get(attachmentImage.Id));
        System.assertEquals(objSpecFile3.Id, ctlr.fileId.get(attachmentDoc.Id));
        System.assertEquals('https://view.officeapps.live.com/op/embed.aspx?src=https://s3.amazonaws.com/SpecBucket/' + attachmentDoc.Name, ctlr.docLinks.get(attachmentDoc.Id));
    }
}
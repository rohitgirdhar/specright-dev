@isTest(seeallData=true)
private class weOEMSpecificationTest
{
    @isTest
    static void testdoPost()
    {
        Test.startTest();
        //Setup Rest Context Object.
        Map<String, RecordType> mapRT = new Map<String, RecordType>();
        for(RecordType rt: [Select id, name from RecordType where sObjectType='specright__Specification__c']) {
            mapRT.put(rt.name, rt);
        }
        specright__Specification__c sp = new specright__Specification__c();
        sp.recordtypeid = mapRT.get('Flexible').id;
        sp.name = 'Test';
        sp.specright__HQspecId__c = '123019';
        //sp.Spec_Case__c = objSpecification.id;
        insert sp;

        specright__Coatings__c coat = new specright__Coatings__c();
        coat.specright__Specification__c = sp.id;
        coat.HQ_Coatings_Id__c = '123123';
        insert coat;

        specright__Embellishments__c emb = new specright__Embellishments__c();
        emb.specright__Specification__c = sp.id;
        emb.HQ_Embellishments_ID__c = '3243432';
        insert emb;

        specright__Layer_Material__c layer = new specright__Layer_Material__c();
        layer.specright__Specification__c = sp.id;
        layer.HQ_Layer_Materials_Id__c = '324324';
        insert layer;

        specright__PMS_Instructions__c pms = new specright__PMS_Instructions__c();
        pms.specright__Specification__c = sp.id;
        pms.HQ_PMS_Instructions_ID__c = '23432';
        insert pms;

        specright__Spec_File__c file = new specright__Spec_File__c();
        insert file;
        Attachment att = new Attachment(parentid=file.id, name='Test', body=Blob.valueof('Test'));
        insert att;
        
        weOEMSpecification  ctlr = new weOEMSpecification();
        sp = [Select id, recordtype.name, HQspecId__c,
            (Select id, name, Coatings__c from Coatings__r),
            (Select id, name, Embellishments__c from Embellishments__r),
            (Select id, name, Layer_Material__c from Layer_Materials__r),
            (Select id, name, PMS_Instructions__c from PMS_Instructions__r)
            from Specification__c where id=:sp.id];
          weOEMSpecification.testData = new weOEMSpecification.RequestData(sp, 
                new List<Spec_File__c>{file},
                sp.coatings__r,
                sp.embellishments__r,
                sp.layer_materials__r,
                sp.pms_instructions__r
            );  
           //weOEMSpecification.doPost results = weOEMSpecification.RequestData();
        
            weOEMSpecification.RequestData results = new weOEMSpecification.RequestData();
            string results1 = weOEMSpecification.doPost();
            System.assert(results.spec == null);
            Test.stopTest();
    }
}
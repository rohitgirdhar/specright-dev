global with sharing class SpecEventSchedule implements Schedulable{
   global void execute(SchedulableContext sc) {
      // We now call the batch class to be scheduled
      SpecEventBatch batch = new SpecEventBatch(); 
       
      //Parameters of ExecuteBatch(context,BatchSize)
      database.executebatch(batch,10);
   }
}
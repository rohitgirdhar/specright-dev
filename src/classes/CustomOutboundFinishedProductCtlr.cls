/**
 * Controller class for VF Page to handle the New and Edit event of 
 * Outbound Finished Product custom object
 * @author - Wisdomedge Inc.
 */
public with sharing class CustomOutboundFinishedProductCtlr {

  // record type derive from Specification and the same will apply to the related object.
    public String selectedRecordType {get; set;}
    
    // Custom object instance
    public Outbound_Finished_Product__c outboundFinishedProductObj {get; set;}
    
    // Custom Object instance for showing Id which is non-writable field
    public Outbound_Finished_Product__c outboundFinishedProductObjForId {get; set;}
    
    // page message
    public String pageMessage {get; set;}
     
    /**
     * default page constructor
     * initialize the Outbound Finished Product Record
     * also initializes the specRecordType
     */
    public CustomOutboundFinishedProductCtlr(ApexPages.StandardController controller) {
        outboundFinishedProductObj = (Outbound_Finished_Product__c)controller.getRecord();
        initPage();
    }
    
    /**
     * custom page initialization method
     */
    public void initPage() {
        Boolean isOutboundFinishedProductCreateable = Schema.sObjectType.Outbound_Finished_Product__c.isCreateable() &&
            Schema.sObjectType.Outbound_Finished_Product__c.fields.Name.isCreateable() &&
            Schema.sObjectType.Outbound_Finished_Product__c.fields.RecordTypeId.isCreateable();
        if(!isOutboundFinishedProductCreateable){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient Create Access'));
            return;
        }
        if (outboundFinishedProductObj.id == null) {
            List<specright__Specification__c> lstspecs = 
                    [Select Id, RecordType.Name from specright__Specification__c 
                    where Id = :outboundFinishedProductObj.SpecificationL__c];
            if(lstspecs.size() < 1) return;
            String specRT = lstspecs[0].RecordType.Name;
            String sobjtype = Outbound_Finished_Product__c.getSObjectType()+'';
            RecordType lstRT = 
                    [Select Id from RecordType where SObjectType =:sobjtype and Name =:specRT limit 1];
            outboundFinishedProductObj.Name = 'Auto-generated';
            outboundFinishedProductObj.RecordTypeId = lstRT.Id;
            String rtid = lstRT.Id;
            selectedRecordType = specRT;
        }
        else {
            Outbound_Finished_Product__c lstoutbounds =
                 [Select Id, Name,Id__c from Outbound_Finished_Product__c where Id=:outboundFinishedProductObj.Id limit 1];
            if(lstoutbounds != null)
                outboundFinishedProductObj.Name = lstoutbounds.Name;
                outboundFinishedProductObjForId = lstoutbounds;
        }
        // initialize selectedRecordType
        List<RecordType> lstRT = [Select Name from RecordType where id=:outboundFinishedProductObj.recordTypeId];
        if(lstRT.size() > 0) {
            selectedRecordType = lstRT[0].Name;
        }
        
    }
    
    /**
     * saves the current record and redirects to the specification
     * detail page.
     */
    public PageReference customSave() {
        pageMessage = '';
        try{
            if (Schema.sObjectType.Outbound_Finished_Product__c.isUpdateable() &&
                Schema.sObjectType.Outbound_Finished_Product__c.isCreateable()) {
                upsert outboundFinishedProductObj;
                return new PageReference('/'+outboundFinishedProductObj.specificationL__c);
			}
            else{
                return null;
            }
        }
        catch(Exception e) {
            pageMessage = e.getMessage();
        }
        return null;
    }
    
    /**
     * custom Save and New method that will not only upsert the 
     * outbound finished product but will also prepare it for adding
     * another record under the same specification
     */
    public PageReference customSaveAndNew() {
        customSave();
        // re-initialize custom object for adding another record under same specification
        outboundFinishedProductObj = new Outbound_Finished_Product__c
            (specificationL__c=outboundFinishedProductObj.specificationL__c);
        initPage();
        return null;
    }
}
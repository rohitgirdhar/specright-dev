public class SpecViewController {

    /**
	* Return the object extension Id
	* */
	@AuraEnabled
    public static Object getExtensionObject(Id specId) {
        Map<String, Object> extensionData = null;

        //get the specification record and get the associated commodity thru the commodity_Object_name field
        Specification__c specification = [SELECT Id, Name, Commodity_Name__c 
                                          FROM 	 Specification__c
                                        WHERE 	 Id = :specId];
        //system.debug(specification);

        //Commodity name shall never be null, but still a safe-side check.
        if (String.isNotBlank(specification.Commodity_Name__c)) {
        	String objectName = specification.Commodity_Name__c;
            String objectLabel = Schema.getGlobalDescribe().get(objectName).getDescribe().getLabel();
            extensionData = new Map<String, Object>();
            extensionData.put('name', objectName);
            extensionData.put('label', objectLabel);

            //get the commodity record
            List<sObject> commodityRecordList = Database.query('SELECT Id, Name FROM ' + objectName + ' WHERE Specification__c = :specId');
            if (!commodityRecordList.isEmpty()) {
                extensionData.put('Id', commodityRecordList[0].Id);
            }
        }

        //system.debug(' ### extensionData ' + extensionData);
        return extensionData;
    }

 	/**
    * Return the list of related metadata information
    * */
    @AuraEnabled
    public static Object[] getReleatedListsMetadata(Id commodityId) {
        return RelatedListDataGridController.getReleatedListsMetadata(commodityId);
    }    
}
/**
 * Controller class for OEM Setup page that should faciliate
 * the handshake and store the access information of a subscriber org to 
 * HQ
 * @author - Wisdomedge Inc.
 */

public with sharing class OEMAuthController {

    public string authCode {get; set;}
    public string accessToken {get; set;}
    public string refreshToken {get; set;}
    public string orgId {get;set;}
    public Boolean done {get; set;}
    public string instance_url {get;set;}
    
    /**
     * default constructor that first gets the token for subscriber org after 
     * authorizing the connected app and sends over to HQ org.
     */
    public OEMAuthController() {
        done = false;
        authCode = ApexPages.currentPage().getParameters().get('code');
        getRefreshToken();
        orgId=UserInfo.getOrganizationId();        
    }

    /**
     * refresh Token for the oem org.
     */
    public PageReference getRefreshToken() {
        //code to request refresh token and access token.      
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setMethod('POST');
        string endpoint = 'https://login.salesforce.com/services/oauth2/token?code='+
            authCode+'&grant_type=authorization_code&client_id=3MVG9xOCXq4ID1uHIAcHMzfuIG5HaZYfC5Qmp1ZxIOF2nkXEsHfHSnhEu.8k1.2d1ZTS8zoibuahAnB6lV32G&client_secret=3675183991603361&redirect_uri=https://login.salesforce.com/apex/specright__oemauth';
        req.setEndpoint(endpoint);

        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
        String refTok = res.getBody();

        integer n = refTok.indexOf('"refresh_token":"');
        n = n+17;
        integer m = refTok.indexOf('","id_token"');
        //system.debug('%%%%%%%%%%%'+refTok.subString(n,m));
        refreshToken = refTok.subString(n,m);
        //system.debug('%%%%%%%%%%%'+tWrap.RefreshToken );
        integer a = refTok.indexOf('"access_token":"');
        a = a+16;
        integer t= refTok.indexOf('"}');
        accessToken = refTok.subString(a,t);
        integer x = refTok.indexOf('"instance_url":"');
        x = x+16;
        integer z = refTok.indexOf('","token_type"');
        instance_url=refTok.subString(x,z);

        return null;
    }

    /**
     * once the connected app is authorized the function can be used to 
     * push the basic information from Subscriber information to HQ for completing
     * the handshake. The supplied information is subsequently used by HQ org
     * to push specifications to subscriber orgs.
     */
    public PageReference pushInfoToHQ() 
    {
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setMethod('POST');
        
        String endpoint = Label.HQ_URL;
        /**
         * check if the HQ URL is defined in the custom settings
         * and has been overridden
         */
        ViewSupplierLink__c link = ViewSupplierLink__c.getOrgDefaults();
        if(link.Org2Org_URL__c != null && link.Org2Org_URL__c != '') {
            endpoint = link.Org2Org_URL__c;
        }

        req.setEndpoint(endpoint + '/oemauthorize');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body='[['+accessToken + ']];[['+orgId + ']];[['+RefreshToken + ']];[['+instance_url+']]';
        req.setBody(EncodingUtil.urlencode(body, 'UTF-8'));
        System.debug('>>>REQ: ' + req + ', body: ' + body);
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());

        // SR-311
        AccessShareBatch.scheduleDeleteShareBatch();

        // SR-179
        SpecRightPostInstallScript.updateOrgWideSetting();
        done = true;
        return null;
    }

}
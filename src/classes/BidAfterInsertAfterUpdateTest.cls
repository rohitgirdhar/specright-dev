@isTest
public with sharing class BidAfterInsertAfterUpdateTest {
    public static testMethod void validateBidAfterInsertAfterUpdate() {
        /*List < RecordType > racctypes = [Select Name, Id From RecordType where sObjectType = 'Account'];
        Map < String, String > AccRecordTypes = new Map < String, String > {};
        for (RecordType rt: racctypes)
        {   
            AccRecordTypes.put(rt.Name, rt.Id);
        }
        //Commented to delete account record type
        */
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        //objAccount.RecordTypeId = AccRecordTypes.get('Supplier');
        insert objAccount;
        system.assertEquals('Test Account', objAccount.Name);
        List < RecordType > rtypes = [Select Name, Id From RecordType where sObjectType = 'specright__Specification__c'];
        Map < String, String > SpecRecordTypes = new Map < String, String > {};
        for (RecordType rt: rtypes)
        SpecRecordTypes.put(rt.Name, rt.Id);
        Specification__c objSpecification = new Specification__c();
        objSpecification.Name = 'Test Specification';
        objSpecification.RecordTypeId = SpecRecordTypes.get('Corrugated');
        insert objSpecification;

        Bid_Request__c objBidRequest = new Bid_Request__c();
        objBidRequest.Bid_Status__c = 'In Process';
        objBidRequest.Specification__c = objSpecification.Id;
        insert objBidRequest;

        Contact objContact = new Contact();
        objContact.LastName = 'Test LastName';
        objContact.AccountId = objAccount.Id;
        insert objContact;
        //Create Portal USer
        Set < String > customerUserTypes = new Set < String > {
            'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'
        };
        Profile p = [select Id, name from Profile where UserType in : customerUserTypes limit 1];

        User newUser = new User(
            profileId = p.id,
            username = 'william.faulkner111@yahoo.com',
            email = 'wf@ff.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias = 'nuser',
            lastname = 'lastname',
            contactId = objContact.id
        );
        insert newUser;

        Spec_File__c objSpecFiles = new Spec_File__c();
        objSpecFiles.Specification__c = objSpecification.Id;
        objSpecFiles.File_Type__c = 'Level 1';
        insert objSpecFiles;

        Bid__c objBid = new Bid__c();
        objBid.Bid_Request__c = objBidRequest.Id;
        objBid.Bid_Status__c = 'In Process';
        objBid.Specification__c = objSpecification.Id;
        objBid.Supplier__c= objAccount.Id;
        insert objBid;

        objBid.Bid_Status__c = 'Awarded';
        update objBid;



    }
}
@isTest
public with sharing class SpecFileAfterUpdateTest{

    public static testMethod void validateSpecFileAfterUpdate(){
    
        Specification__c objSpecification = new Specification__c();
        objSpecification.name='Test Specification';
        insert objSpecification;
        
        Spec_File__c objSpecFile =new Spec_File__c();
        objSpecFile.Spec_File_Name__c='Test File';
        objSpecFile.Specification__c=objSpecification.id;
        objSpecFile.File_Type__c='Level 1';
        insert objSpecFile;

        System.assertEquals(objSpecFile.Spec_File_Name__c,'Test File');
        Attachment attachment = new Attachment(Name='Test Attachment',body=blob.valueof('testbody'),parentId=objSpecFile.Id);
        insert attachment;
             
        objSpecFile.Attachment_id__c=attachment.id;
        update objSpecFile;
        
        Attachment attachment2 = new Attachment(Name='Test Attachment2',body=blob.valueof('testbody2'),parentId=objSpecFile.Id);
        upsert attachment2;
        
        objSpecFile.Attachment_id__c=attachment2.id;
        update objSpecFile;  

    }
    
    public static testMethod void SpecFileAfterUpdateEdit() {
        Specification__c objSpecification = new Specification__c();
        objSpecification.name='Test Specification';
        insert objSpecification;
        
        Spec_File__c objSpecFile =new Spec_File__c();
        objSpecFile.Spec_File_Name__c='Test File';
        objSpecFile.Specification__c=objSpecification.id;
        objSpecFile.File_Type__c='Level 1';
        insert objSpecFile;

        objSpecFile.Spec_File_Name__c = 'New Test File Name';
        update objSpecFile;
        objSpecFile.File_Type__c = 'Level 2';
        update objSpecFile;
        
        System.assertEquals('New Test File Name', objSpecFile.Spec_File_Name__c);
        System.assertEquals('Level 2', objSpecFile.File_Type__c);
    }
}
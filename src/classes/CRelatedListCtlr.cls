public with sharing class CRelatedListCtlr {

    public string sObjType {
        get; 
        set{
            sObjType = value;
            initComponent(); 
        }
    }
    
    public String parentObjId {
        get; 
        set {
            parentObjId = value;
            initComponent(); 

        }
    }
    
    public String objLabel {
        get; 
        set {
            objLabel = value;
            initComponent(); 
        }
    }
    
    //public String flsName {get; set;}

    public List<String> cols {
        get; 
        set;
    }
    
    public List<sObject> lstRecords {get; set;}

    Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
    /*
    //public void setThisCtlr(CRelatedListCtlr ctlr) {
    //thisCtlr = ctlr;
    }
    
    public CRelatedListCtlr thisCtlr{get; set{*/
    public String flsName {
        get; 
        set {
            flsName = value;
            initComponent(); 
        }
    }

    public void initComponent() {
        CRelatedListCtlr thisCtlr = new CRelatedListCtlr();
        System.debug('>>>this : ' + this);
        if(sObjType == null || flsName == null || parentObjId == null ) return;
        // get fields from related fieldset
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(sObjType);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        cols = new List<String>();
    
        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));

        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(flsName);

        //List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
        //return fieldSetObj.getFields(); 
        String fields = '';
        for (Schema.FieldSetMember fsmember: fieldSetObj.getFields()) {
            if(fields == '') fields = fsmember.getFieldPath();
            else fields = fields + ',' + fsmember.getFieldPath();
            cols.add(fsmember.getFieldPath());
        }
        lstRecords = database.query('Select ' + fields + ' from ' + sObjType +
            ' where specright__Specification__c=\''+
            parentObjId + '\'');
         
       
    }
}
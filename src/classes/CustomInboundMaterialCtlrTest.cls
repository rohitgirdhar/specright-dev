@isTest
private class CustomInboundMaterialCtlrTest {
    
    static Specification__c getFlexibleSpec() {
        RecordType rt =[select id ,Name from RecordType where Name = 'Flexible'  and 
                        sObjectType ='specright__Specification__c' limit 1];
        Specification__c sp = new Specification__c();
        sp.Name = 'Test Specification';
        sp.RecordTypeId = rt.id;
        insert sp;
        
        return sp;
    }
    
    static Inbound_Material__c getFlexibleInboundMaterial(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Flexible'  and 
                             sObjectType ='specright__Inbound_Material__c' limit 1];
        
        Inbound_Material__c IBM = new Inbound_Material__c();
        IBM.Name = 'Auto-generated';
        IBM.Location__c ='Test 234';
        IBM.Specification__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;
        upsert IBM;
        return IBM;
    }
    
    static Inbound_Material__c getCorrugatedInboundMaterial(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Corrugated'  and 
                             sObjectType ='specright__Inbound_Material__c' limit 1];
        
        Inbound_Material__c IBM = new Inbound_Material__c();
        IBM.Name = 'Auto-generated';
        IBM.Location__c ='Test 234';
        IBM.Specification__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;

        ApexPages.StandardController AController = new ApexPages.StandardController(IBM);
        Test.setCurrentPageReference(new PageReference('Page.CustomInboundMaterial')); 
        CustomInboundMaterialCtlr controller = new CustomInboundMaterialCtlr(AController);
        System.assertEquals(controller.inboundMaterialObj.Name , IBM.Name);
        
        return IBM;
    }
    
    static testMethod void  testCustomSavePositive() {
        Test.startTest();
        Specification__c flexibleSpec = getFlexibleSpec();
        // Handle test for Inbounds 
        Inbound_Material__c im = getFlexibleInboundMaterial(flexibleSpec);
        Test.stopTest();
        
        ApexPages.StandardController AController = new ApexPages.StandardController(im);
        Test.setCurrentPageReference(new PageReference('Page.CustomInboundMaterial')); 
        // test save for new record
        CustomInboundMaterialCtlr controller = new CustomInboundMaterialCtlr(AController);
        controller.CustomSave();
        controller.inboundMaterialPrefix = 'Prefix';
        System.assert(controller.inboundMaterialObj.id != null);
        
        // test edit scenario
        System.currentPageReference().getParameters().put('id', im.id);
        controller.customSaveAndNew();
    }
}
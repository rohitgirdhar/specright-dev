@isTest
public class CustomInOutboundDirCtlrTest {
    static Specification__c getFlexibleSpec() {
        RecordType rt =[select id ,Name from RecordType where Name = 'Flexible'  and 
                        sObjectType ='specright__Specification__c' limit 1];
        Specification__c sp = new Specification__c();
        sp.Name = 'Test Specification';
        sp.RecordTypeId = rt.id;
        insert sp;
        
        return sp;
    }
    
    static Inbound_Material__c getFlexibleInboundMaterial(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Flexible'  and 
                             sObjectType ='specright__Inbound_Material__c' limit 1];
        
        Inbound_Material__c IBM = new Inbound_Material__c();
        IBM.Name = 'Auto-generated';
        IBM.Location__c ='Test 234';
        IBM.Specification__c = sp.id;
        IBM.RecordTypeId = rtIBM.id;
        upsert IBM;
        return IBM;
    }
    
    static Outbound_Finished_Product__c getFlexibleOutboundMaterial(Specification__c sp) {
        RecordType rtIBM =[select id ,Name from RecordType where Name = 'Flexible'  and 
                             sObjectType ='specright__Outbound_Finished_Product__c' limit 1];
        
        Outbound_Finished_Product__c OBM = new Outbound_Finished_Product__c();
        OBM.Name = 'Auto-generated';
        OBM.Location__c ='Test 234';
        OBM.SpecificationL__c = sp.id;
        OBM.RecordTypeId = rtIBM.id;
        upsert OBM;
        return OBM;
    }
    
    static testMethod void SelectOptionTest() {
        Specification__c flexibleSpec = getFlexibleSpec();
        Inbound_Material__c im = getFlexibleInboundMaterial(flexibleSpec);
        
        ApexPages.StandardController AController = new ApexPages.StandardController(im);
        Test.setCurrentPageReference(new PageReference('Page.CustomInboundMaterial')); 
        CustomInOutboundDirCtlr controller = new CustomInOutboundDirCtlr(AController);
        
        List<SelectOption> inboundOptions =  controller.getOptionsrecordTypeInbound();
        System.assertNotEquals(0, inboundOptions.size());
        List<SelectOption> outboundOptions = controller.getOptionsrecordTypeOutbound();
        System.assertNotEquals(0, outboundOptions.size());
    }
    
    static testMethod void pageDirTest() {
        Test.startTest();
        Specification__c flexibleSpec = getFlexibleSpec();
        Inbound_Material__c im = getFlexibleInboundMaterial(flexibleSpec);
        Test.stopTest();
        
        SObjectType childSobType = Inbound_Material__c.SObjectType;
        SObjectType parentSobType = Specification__c.SObjectType;
        Schema.DescribeFieldResult dfr = Schema.SObjectType.Inbound_Material__c.fields.Specification__c;
        
        ApexPages.StandardController AController = new ApexPages.StandardController(im);
        Test.setCurrentPageReference(new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e'));
        System.currentPageReference().getParameters().put('00No000000BmwZJ', flexibleSpec.Name);
        System.currentPageReference().getParameters().put('00No000000BmwZJ_lkid', flexibleSpec.Id);
        CustomInOutboundDirCtlr controller = new CustomInOutboundDirCtlr(AController);
        controller.DirectToPage();
        controller.ToEditPage();
        controller.BackToSpec();
        System.assert(true);
    }
    
    static testMethod void pageDirOutboundTest() {
        Test.startTest();
        Specification__c flexibleSpec = getFlexibleSpec();
        Outbound_Finished_Product__c im = getFlexibleOutboundMaterial(flexibleSpec);
        Test.stopTest();
        
        SObjectType childSobType = Outbound_Finished_Product__c.SObjectType;
        SObjectType parentSobType = Specification__c.SObjectType;
        Schema.DescribeFieldResult dfr = Schema.SObjectType.Inbound_Material__c.fields.Specification__c;
        
        ApexPages.StandardController AController = new ApexPages.StandardController(im);
        Test.setCurrentPageReference(new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e'));
        System.currentPageReference().getParameters().put('CF00No000000BmwZO', flexibleSpec.Name);
        System.currentPageReference().getParameters().put('CF00No000000BmwZO_lkid', flexibleSpec.Id);
        CustomInOutboundDirCtlr controller = new CustomInOutboundDirCtlr(AController);
        controller.DirectToPageOutbound();
        controller.ToEditPageOutbound();
        System.assert(true);
    }
}
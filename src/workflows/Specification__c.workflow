<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>RT_to_Flexible</fullName>
        <description>From Corrugated to Flexible</description>
        <field>RecordTypeId</field>
        <lookupValue>Flexible</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RT to Flexible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Spec_RT_Corrugated</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Corrugated</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Spec RT Corrugated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>

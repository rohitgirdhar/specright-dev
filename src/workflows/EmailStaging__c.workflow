<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailStaging_Supplier_Invitation_Mail</fullName>
        <description>EmailStaging Supplier Invitation Mail</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>specright_email_templates/BidInviteSupplierTemplate</template>
    </alerts>
    <alerts>
        <fullName>EmailToBidOwner</fullName>
        <description>EmailToBidOwner</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>specright_email_templates/BidAwardTemplate</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Push_Audit_Access_owner</fullName>
        <description>Email to Push Audit Access owner</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>specright_email_templates/Push_Audit_Response</template>
    </alerts>
    <rules>
        <fullName>EmailStaging Send Email to Bid Owner</fullName>
        <actions>
            <name>EmailToBidOwner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailStaging__c.Type__c</field>
            <operation>equals</operation>
            <value>Bid Award Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailStaging Send Email to Supplier</fullName>
        <actions>
            <name>EmailStaging_Supplier_Invitation_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailStaging__c.Type__c</field>
            <operation>equals</operation>
            <value>Supplier Invitation</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailStaging Send Email to Supplier for Push Audits</fullName>
        <actions>
            <name>Email_to_Push_Audit_Access_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailStaging__c.Type__c</field>
            <operation>equals</operation>
            <value>Push Audit Response</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

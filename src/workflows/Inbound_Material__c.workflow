<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Inbound_Material_Name</fullName>
        <field>Name</field>
        <formula>&apos;IM - &apos; + Specification__r.Name + &apos; - &apos; + Location__c</formula>
        <name>Update Inbound Material Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Inbound Material Name</fullName>
        <actions>
            <name>Update_Inbound_Material_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

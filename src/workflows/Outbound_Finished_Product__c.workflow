<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Outbound_Finished_Product_Name</fullName>
        <field>Name</field>
        <formula>&apos;OFP - &apos; + SpecificationL__r.Name + &apos; - &apos; +  Location__c</formula>
        <name>Update Outbound Finished Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Outbound Finished Product Name</fullName>
        <actions>
            <name>Update_Outbound_Finished_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Outbound_Finished_Product__c.ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Finished_Product__c.Location__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

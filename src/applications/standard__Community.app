<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-Idea</tab>
    <tab>standard-Answers</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Questions</tab>
    <tab>Award_Bid</tab>
    <tab>BidGroup__c</tab>
    <tab>EmailStaging__c</tab>
    <tab>Spec_Family__c</tab>
    <tab>Like_Item_Finder</tab>
</CustomApplication>

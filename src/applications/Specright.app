<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Specright is an intelligent approach to packaging specification management.</description>
    <label>Specright</label>
    <logo>Specright/specright_logo_cloud.png</logo>
    <tab>Specification__c</tab>
    <tab>Spec_Family__c</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>BidGroup__c</tab>
    <tab>Like_Item_Finder</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
